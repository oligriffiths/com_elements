<?php
/**
 * User: Oli Griffiths
 * Date: 28/06/2012
 * Time: 14:36
 */

class ComElementsModelLayouts extends ComDefaultModelDefault
{
	public function __construct(KConfig $config)
	{
		parent::__construct($config);
		$this->getState()
			->insert('package',     'string')
			->insert('type',        'string',null,  true, array(), true)
			->insert('target',      'string',null,  true, array(), true)
			->insert('structure',   'string',null,  true, array(), true)
			->insert('name',        'string',null,  true, array(), true)
			->insert('path',        'string',null,  true, array(), true)
			->insert('template',    'string',null,  true, array(), true)
			->insert('admin',       'int',  0,      true, array(), true)
			->insert('submission',  'int',  0,      true, array(), true);
	}


	/**
	 * Method to get a item object which represents a table row
	 *
	 * If the model state is unique a row is fetched from the database based on the state.
	 * If not, an empty row is be returned instead.
	 *
	 * @return KDatabaseRow
	 */
	public function getItem()
	{
		if(!$this->_item)
		{
			parent::getItem();

			//If this is a new row, set the ID from the state
			if($this->_item && $this->_item->isNew())
			{
				$data = $this->getState()->getData(true);
				$this->_item->setData($data, false);
			}
		}

		return $this->_item;
	}


	/**
	 * @return KDatabaseTableDefault
	 */
	public function getTable()
	{
		if(!$this->package) return null;

		if($this->_table !== false)
		{
			if(!($this->_table instanceof KDatabaseTableAbstract))
			{
				//Make sure we have a table identifier
				if(!($this->_table instanceof KServiceIdentifier)) {
					$this->setTable($this->_table);
				}

				try {
					$options = array('name' => $this->package.'_layouts');
					$this->_table = $this->getService($this->_table, $options);
				} catch (KDatabaseTableException $e) {
					$this->_table = false;
				}
			}
		}

		return $this->_table;
	}
}