<?php
/**
 * Created By: Oli Griffiths
 * Date: 30/05/2012
 * Time: 14:47
 */
defined('KOOWA') or die('Protected resource');

class ComElementsModelStructures extends ComDefaultModelDefault implements KServiceInstantiatable
{
	protected $_items = array();
	protected $_tables = array();

	/**
	 * Object constructor
	 *
	 * @param   object  An optional KConfig object with configuration options.
	 */
	public function __construct(KConfig $config)
	{		
		parent::__construct($config);
		$this->getState()
				->insert('id','string',null, true)
				->insert('path','string', null, true)
				->insert('type','string')
				->insert('package','string');
	}


	/**
	 * Force creation of a singleton
	 *
	 * @param 	object 	An optional KConfig object with configuration options
	 * @param 	object	A KServiceInterface object
	 * @return KDatabaseTableDefault
	 */
	public static function getInstance(KConfigInterface $config, KServiceInterface $container)
	{
		// Check if an instance with this identifier already exists or not
		if (!$container->has($config->service_identifier))
		{
			//Create the singleton
			$classname = $config->service_identifier->classname;
			$instance  = new $classname($config);
			$container->set($config->service_identifier, $instance);
		}

		return $container->get($config->service_identifier);
	}



	/**
	 * Method to get a item object which represents a table row
	 *
	 * If the model state is unique a row is fetched from the database based on the state.
	 * If not, an empty row is be returned instead.
	 *
	 * @return KDatabaseRow
	 */
	public function getItem()
	{
		if(!$this->_item)
		{
			parent::getItem();

			//If this is a new row, set the ID from the state
			if($this->_item && $this->_item->isNew())
			{
				$this->_item->id = $this->id;
			}
		}

		return $this->_item;
	}


	/**
	 * @return KDatabaseTableDefault
	 */
	public function getTable()
	{
		if(!$this->package) return null;

		if(!isset($this->_tables[$this->package]))
		{
			//Make sure we have a table identifier
			if(!($this->_table instanceof KServiceIdentifier)) {
				$this->setTable($this->_table);
			}

			try {
				$options = array('name' => $this->package.'_structures');
				$this->_tables[$this->package] = $this->getService($this->_table, $options);
			} catch (KDatabaseTableException $e) {
				$this->_tables[$this->package] = false;
			}
		}

		return $this->_tables[$this->package];
	}


	protected function _buildQueryWhere(KDatabaseQuery $query)
	{
		parent::_buildQueryWhere($query);

		if($this->type)
		{
			$id = $this->getTable()->mapColumns(array('id' => 0));
			$id_col = array_pop(array_keys($id));

			$query->where('tbl.'.$id_col,'LIKE',$this->type.'%');
			$query->order('tbl.'.$id_col,'ASC');
		}
	}
}