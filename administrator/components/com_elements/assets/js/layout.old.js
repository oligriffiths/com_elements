
var ElementsLayout = new Class({

	Implements: [Options, Events],

	initialize: function(element, options){

		this.setOptions(options)
		this.element = $(element)
		this.layout = this.element.getElement('.layout')
		this.add_block = this.element.getElement('.add_block')
		this.lockBtn = this.element.getElement('.lock')
		this.editing = true;

		this.add_block.addEvent('click', function(e){
			new Event(e).stop()
			this.addBlock(this.layout, false)

		}.bind(this))
		this.addBlock(this.layout)

		this.lockBtn.addEvent('click', function(e){
			new Event(e).stop()
			if(this.editing) this.lock()
			else this.edit()
		}.bind(this))
	},

	createSortable: function(parent)
	{
		parent.sortables = new Sortables(parent, {
			snap: 10,
			handle: '.row_actions .move',

			onStart: function(){
				this.list.addClass('dragging')
			},
			onComplete: function(){
				this.list.removeClass('dragging')
			},
			clone: function(event, element, list){
				return element.clone(true).addClass('clone').setStyles({
					opacity: 0.7,
					margin: '0px',
					position: 'absolute',
					visibility: 'hidden',
					'width': element.getStyle('width')
				}).inject(this.list).setPosition(element.getPosition(element.getOffsetParent()))
			}
		})
	},

	addDropdown: function(options)
	{
		var button = new Element('div', {'class':'btn-group',html: '<button class="btn dropdown-toggle"><span class="caret"></span></button>'})
		var dropdown = new Element('ul',{'class':'dropdown-menu'}).inject(button)
		button.getElement('button').addEvent('click', function(e){
			new Event(e).stop()
			if(button.hasClass('open')) button.removeClass('open')
			else button.addClass('open')
		})

		if(options)
		{
			for(var key in options)
			{
				var value = options[key]
				if(typeof(value) != 'string') continue;
				var icon = ''
				if(key.match(/^add_/)) icon = '<span class="icon-plus"></span>'
				if(key.match(/^remove_/)) icon = '<span class="icon-minus-sign"></span>'

				new Element('li',{html: '<a class="'+key+'">'+icon+' '+value+'</a>'}).inject(dropdown)
			}
		}

		dropdown.getElements('a').addEvent('click', function(e){
			new Event(e).stop()
			button.removeClass('open')
		})

		return button;
	},

	addBlock: function(parent, empty)
	{
		//if(parent.getElements('.col').length == 10) return;
		if(empty) parent.empty()

		var block = new Element('ul', {'class':'block'}).inject(parent)
		this.createSortable(block)

		var actions = new Element('li', {'class': 'actions block_actions'}).inject(block)

		var menu = []
		menu['add_row'] = 'Add Row'
		menu['remove_block'] = 'Remove Block'
		this.addDropdown(menu).inject(actions)


		this.parseBlock(block)
		this.addRow(block)
	},

	parseBlock: function(block)
	{
		block.getElement('.add_row').addEvent('click', this.addRow.bind(this, block))
		block.getElement('.remove_block').addEvent('click', this.removeBlock.bind(this, block))
	},


	addRow: function(parent){

		var row = new Element('li',{'class':'row cols_1'}).inject(parent,'bottom')
		if(!row.getParent().sortables) this.createSortable(row.getParent())

		var actions = new Element('div',{'class':'actions row_actions'}).inject(row)
		new Element('div',{'class': 'btn btn-mini move', html: '<span class="icon-move"></span>'}).inject(actions)

		var menu = []
		menu['add_col'] = 'Add Column'
		menu['remove_row'] = 'Remove Row'
		this.addDropdown(menu).inject(actions)

		this.parseRow(row)
		this.addColumn(row)
	},

	parseRow: function(row)
	{

		row.getElement('.add_col').addEvent('click', this.addColumn.bind(this, row))
		row.getElement('.remove_row').addEvent('click', this.removeRow.bind(this, row))

		row.getParent().sortables.attach(row)
	},

	addColumn: function(row)
	{
		var col = new Element('div', {'class':'col'}).inject(row)
		var actions = new Element('div',{'class':'actions column_actions'}).inject(col)

		var menu = []
		menu['add_block'] = 'Add Block'
		menu['remove_col'] = 'Remove Column'
		this.addDropdown(menu).inject(actions)

		var content = new Element('div',{'class':'content'}).inject(col)
		new Element('input',{'type': 'text','name': '','placeholder': 'enter position name'}).inject(content)

		this.parseColumn(col)
		this.countColumns(row)
	},

	parseColumn: function(col)
	{
		col.getElement('.add_block').addEvent('click', this.addBlock.bind(this, [col.getElement('.content'), true]))
		col.getElement('.remove_col').addEvent('click', this.removeCol.bind(this, col))
	},

	countColumns: function(row)
	{
		var cols = row.getChildren('.col')
		cols.each(function(col, index){
			col.set('class','col col_'+(index+1))
		})

		row.set('class','row cols_'+cols.length)
	},

	removeRow: function(row)
	{
		if(row.getParent().getElements('.row').length <= 1){
			this.removeBlock(row.getParent())
		}else{
			row.remove()
		}
	},

	removeCol: function(col)
	{
		var row = col.getParent()
		if(col.getParent().getElements('.col').length <= 1);
		col.remove()
		this.countColumns(row)
	},

	removeBlock: function(block)
	{
		block.remove()
	},

	lock: function(){
		this.lockBtn.addClass('active')
		this.layout.addClass('lock')
		this.editing = false
	},

	edit: function(){
		this.lockBtn.removeClass('active')
		this.layout.removeClass('lock')
		this.editing = true
	}
})