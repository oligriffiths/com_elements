window.addEvent('domready', function(){

    var dummy = $('element_dummy').dispose()

    var sortable = new Sortables($$('.structure .elements'),
        {
            handle: '.move',
            clone: function(event, element)
            {
                var pos = element.getPosition(element.getOffsetParent());
                var clone = element.clone(true);
                clone.addClass('clone')
                clone.getElement('.jpane-slider').empty()
                return clone.setStyles({
                    opacity: 0.9,
                    background: '#ddd',
                    margin: '0px',
                    position: 'absolute',
                    left: pos.x+'px !important',
                    visibility: 'hidden',
                    'width': element.getStyle('width'),
                    'height': element.getStyle('height')
                }).inject(this.list).setPosition(element.getPosition(element.getOffsetParent()))
            }
        })

    var accordion = new Accordion($$('.elements h3.jpane-toggler'), $$('.elements div.jpane-slider'),
        {
            onActive: function(toggler, i) { toggler.addClass('jpane-toggler-down'); toggler.removeClass('jpane-toggler'); },
            onBackground: function(toggler, i) { toggler.addClass('jpane-toggler'); toggler.removeClass('jpane-toggler-down'); },
            duration: 300,
            alwaysHide: true,
            display: -1
        }
    );


    var index = 0
    $('element_types').getElements('button').each(function(element){
        var type = element.get('id').replace('element_type_','')
        var package = element.get('data-package')
        var structure = element.get('data-structure')

        element.addEvent('click', function(e){
            new Event(e).stop();
            index++;
            var new_element = dummy.clone(true).removeProperty('id').setStyle('display','').inject($('elements'))
            var toggler = new_element.getElement('h3.jpane-toggler');
            var content = new_element.getElement('.jpane-slider');

            toggler.getElement('label').set('html', 'New Element | '+type)
            content.removeProperty('style')

            new Request({
                url: '/index.php?option=com_elements&view=element&format=raw&layout=config&type='+type+'&package='+package+'&structure='+structure,
                method: 'get',
                onSuccess: function(html)
                {
                    new Element('div', {html: html}).inject(new_element.getElement('.jpane-slider'))

                    new_element.getElement('span.loading').destroy()
                    new_element.getElement('select.element_type').set('value', type)
                    new_element.getElements('input,textarea,select').each(function(el){
                        el.set('name', el.get('name').replace(/elements\[[a-zA-Z0-9]*\]/, 'elements['+index+']')).removeProperty('id')
                    })
                    parseElement(new_element)
                    new_element.getElement('h3.jpane-toggler').fireEvent('click')

                },
                onFailure: function(error)
                {
                    if(error.status == 401){
                        alert('Unable to retrieve element, your session has expired, please refresh and login')
                        document.location.reload()
                    }else{
                        alert('Unable to retrieve element')
                    }
                }

            }).send()

            accordion.togglers.push(toggler)
            accordion.addSection(toggler, content)
            sortable.attach(new_element)
        })
    })

    $$('.elements .element').each(parseElement)

    function parseElement(element)
    {
        var remove = element.getElement('.remove')
        if(remove){
            remove.addEvent('click', function(e){
                new Event(e).stop()
                if(confirm('Are you sure you wish to remove this element?')) element.destroy()
            })
        }

        //Ensure the custom group field only contains alpha numeric, spaces become underscores
        var custom_group = element.getElement('.custom_group')
        if(custom_group){
            custom_group.addEvent('keyup', function(){
                this.set('value',this.get('value').replace(/[^a-zA-Z0-9]+/g, '_'))
            })

            //Add change event to the group selector to show/hide the custom group field
            var group = element.getElement('.element_group')
            if(group)
            {
                group.addEvent('change', function(){
                    if(this.get('value') == '*custom*')
                        custom_group.getParent('.control-group').setStyle('display','')
                    else
                        custom_group.getParent('.control-group').setStyle('display','none')
                }).fireEvent('change')
            }
        }
    }
})