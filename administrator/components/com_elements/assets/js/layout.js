
window.addEvent('domready', function(){

    var accordion = new Fx.Accordion($$('#positions h3.jpane-toggler'), $$('#positions div.jpane-slider'),
    {
        onActive: function(toggler, i) { toggler.addClass('jpane-toggler-down'); toggler.removeClass('jpane-toggler'); },
        onBackground: function(toggler, i) { toggler.addClass('jpane-toggler'); toggler.removeClass('jpane-toggler-down'); },
        duration: 300,
        alwaysHide: true,
        display: -1
    });

    var Sortables_fixed = new Class({
        Extends: Sortables,
        copy_el: null,
        options: {
            clone: true,
            source: null,
            opacity: 0.5,
            onStart: function(element, clone){

                this.source = element.getParent()
                element.addClass('hideConfig')

                element.getElements('.hasTip').each(function(el){
                    el.set('data-tip-title', el.retrieve('tip:title'))
                    el.set('data-tip-text', el.retrieve('tip:text'))
                })

                if(element.getParent() == this.options.source){
                    this.copy_el = element.clone(true, true)
                    this.copy_el.getElements('input[type=radio]').each(function(input, i){
                        input.set('name', 'clone_' + input.get('name'));
                        if (input.get('checked')) element.getElements('input[type=radio]')[i].set('checked', true);
                    });
                    this.copy_el.setStyle('display','none')
                    this.copy_el.inject(element, 'before')

                    //Enable tooltips
                    new Tips(this.copy_el.getElements('.hasTip'), {
                        title: function(element){
                            return element.get('data-tip-title')
                        },
                        text: function(element){
                            return element.get('data-tip-text')
                        }
                    })
                }
			},
            onComplete: function(element)
            {
                element.removeClass('hideConfig')
                this.source = null

                //If the elements parent is the source, and there is a copy, remove the copy else remove the element
                if(element.getParent() == this.options.source)
                {
                    if(this.copy_el) this.copy_el.destroy()
                    else element.destroy()
                }
                else
                {
                    //If the elements parent is the source do nothing
                    if(this.source != element.getParent())
                    {
                        //Add new element to accordion
                        var toggler = element.getElement('h3.jpane-toggler')
                        if(toggler && !toggler.retrieve('accordion:display'))
                        {
                            accordion.addSection(toggler, element.getElement('div.jpane-slider'))
                            var index = accordion.togglers.indexOf(toggler)
                            if(index != -1) accordion.display(index, false)
                        }

                        if(this.copy_el)
                        {
                            //Rename inputs
                            this.copy_el.getElements('input[type=radio]').each(function(input, i){
                                input.set('name', input.get('name').replace(/^clone_/,''));
                                if (input.get('checked')) element.getElements('input[type=radio]')[i].set('checked', true);
                            });

                            //Add to sortables
                            this.addItems(this.copy_el)
                            this.copy_el.setStyle('opacity',1)

                            this.copy_el = null
                        }
                    }
                }
            }
        },
        insert: function(dragging, element){

            //Only sort if the source is not the source list
            if (element.getParent() != this.options.source){

                this.parent(dragging, element)

                if(this.copy_el)
				{
					if(element.getParent() != this.options.source) this.copy_el.setStyle('display','block')
					else this.copy_el.setStyle('display','none')
				}
            }
        },
        getClone: function(event, element)
        {
            var clone = this.parent(event, element)
            clone.addClass('clone')
            return clone;
        }
    })


    var sortable = new Sortables_fixed($$('.layout_assign .elements'),
    {
        handle: 'h3.title',
        precalculate: true,
        handle: '.move',
        source: $('elements_wrapper')
    })

    sortable.addEvent('onComplete', addRemoveEvent)
    sortable.addEvent('onComplete', function(element){

        if(this.source != element.getParent())
        {
            //Get the position
            var position = element.getParent().getParent().get('data-position')
            if(position)
            {
                var index =  element.getParent().getChildren().length-1;

                //Rename elements & labels
                element.getElements('input,textarea,select').each(function(el){

                    var id = el.get('id')
                    el.set('name', el.get('name').replace(/^elements\[[a-zA-Z0-9_\-]*\]\[[0-9]*\]/, 'elements['+position+']['+index+']'))
                    el.set('id', id.replace(/^elements\[[a-zA-Z0-9_\-]*\]\[[0-9]*\]/, 'elements['+position+']['+index+']'))

                    var label = $(id+'-lbl')
                    if(label)
                    {
                        label.set('id', el.get('id')+'-lbl')
                        label.set('for', el.get('id'))
                    }
                }.bind(this))
            }
        }
    })

    $$('.layout_assign .elements .element').each(addRemoveEvent)

    function addRemoveEvent(element)
    {
        var remove = element.getElement('.header .remove');
        if(remove && !remove.hasRemoveEvent)
        {
            remove.addEvent('click', function(){
                if(confirm('Are you sure you wish to remove this element?'))
                {
                    element.destroy()
                }
            })
            remove.hasRemoveEvent = true;
        }
    }
})