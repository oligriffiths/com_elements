<?php
/**
 * Created By: Oli Griffiths
 * Date: 19/09/2012
 * Time: 12:10
 */
defined('KOOWA') or die('Protected resource');

class ComElementsDatabaseRowStructure extends KDatabaseRowDefault
{
	protected $_item;
	protected $_elements;
	protected $_target_table = false;

    public function __construct(KConfig $config = null)
    {
        parent::__construct($config);

        //Set the package
        $package = $config->data->package ?: preg_replace('#_structures$#','', $this->getTable()->getName());
        $this->setData(array('package' => $package), false);

        //Load the structure data
        $this->loadStructureFromTable();

        //Initialize the elements
        $this->_elements = $this->getService('com://site/elements.view.elements.set', array('structure' => $this,'package' => $this->package));
    }


    /**
	 * Object constructor
	 *
	 * @param   object  An optional KConfig object with configuration options.
	 */
	protected function _initialize(KConfig $config)
	{
        parent::_initialize($config);

        if(!$config->data) $config->data = array();

		$config->append(array(
			'data' => array(
                'elements' => array(),
                'package' => $config->package
            )
        ));
	}


	public function __clone()
	{
		$this->_elements = clone $this->_elements;
		$this->_elements->setStructure($this);
		return $this;
	}


	/**
	 * Overridden setData to load the DB structure once ID is set
	 * @param $data
	 * @param bool $modified
	 * @return ComElementsDatabaseRowStructure
	 */
	public function setData($data, $modified = true)
	{
		if(isset($data['elements']) && is_string($data['elements']))
		{
			$data['elements'] = json_decode($data['elements'], true);
		}

		parent::setData($data, $modified);

		if(isset($data['id']))
		{
			$this->loadStructureFromTable();
		}

		return $this;
	}


	/**
	 * Overridden set to load the DB structure once ID is set
	 * @param string $column
	 * @param mixed $value
	 */
	public function __set($column, $value)
	{
		if($column == 'elements' && is_string($value))
		{
			$value = new KConfig(json_decode($value, true));
		}

		parent::__set($column, $value);

		if($column == 'id')
		{
			$this->loadStructureFromTable();
		}
	}


	/**
	 * Returns a formatted ID
	 * @return string
	 */
	public function getId()
	{
		return $this->package.'.'.$this->id;
	}


	/**
	 * @return bool|object
	 */
	public function getTargetTable()
	{
		if($this->_target_table === false)
		{
			if(!$this->package) return false;

			if(!($this->_target_table instanceof KDatabaseTableAbstract))
			{
				//Make sure we have a table identifier
				if(!($this->_target_table instanceof KServiceIdentifier)) {
					$id = explode('.',$this->id);
					$this->setTargetTable(KInflector::tableize($id[0]));
				}

				try {
					$this->_target_table = $this->getService($this->_target_table);
				} catch (KDatabaseTableException $e) {
					$this->_target_table = false;
				}
			}
		}

		return $this->_target_table;
	}


	/**
	 * Method to set a table object attached to the rowset
	 *
	 * @param	mixed	An object that implements KObjectServiceable, KServiceIdentifier object
	 * 					or valid identifier string
	 * @throws	KDatabaseRowException	If the identifier is not a table identifier
	 * @return	KDatabaseRowsetAbstract
	 */
	public function setTargetTable($table)
	{
		if(!($table instanceof KDatabaseTableAbstract))
		{
			if(is_string($table) && strpos($table, '.') === false )
			{
				if(!$this->package) return $this;

				//Create target table identifier
				$identifier                 = clone $this->getIdentifier();
				$identifier->application    = 'admin';
				$identifier->package        = $this->package;
				$identifier->path           = array('database','table');
				$identifier->name           = $table;
			}
			else  $identifier = $this->getIdentifier($table);

			if($identifier->path[1] != 'table') {
				throw new KDatabaseRowsetException('Identifier: '.$identifier.' is not a table identifier');
			}

			$table = $identifier;
		}

		$this->_target_table = $table;

		return $this;
	}


	/**
	 * @return ComElementsDatabaseRowStructure
	 */
	public function loadStructureFromTable($force = false)
	{
		if(!$this->package) return $this;

		if($this->elements && !$force) return $this;

		if($table = $this->getTargetTable())
		{
			//Loop through columns and attempt to retrieve from the config
			$columns = $table->getColumns();

			$data = array();
			foreach($columns AS $column => $schema)
			{
				if($column == 'elements' || $column == 'params' || ($column == 'type' && $this->type) || $schema->primary) continue;

				$config         = new KConfig(isset($this->elements[$column]) ? $this->elements[$column] : array());
				$config->fixed  = true;
				$config->isRepeatable  = false;
				$config->required = $schema->required;

				if($column == 'password' || $column == 'password2')
				{
					$config->type = 'password';
				}
				elseif($column == 'type' && $this->type)
				{
					$config->type = 'hidden';
					$config->data = array('value' => $this->type);
				}
				else if(!$config->type)
				{
					switch($schema->type){
						case 'bit':
							$config->type = 'boolean';
							break;

						case 'int':
						case 'tinyint':
						case 'varchar':
						case 'char':
						case 'float':
							if($schema->length > 100) $config->type = 'textarea';
							else $config->type = 'text';
							break;

						case 'datetime':
						case 'timestamp':
						case 'date':
							$config->type = 'date';
							break;

						case 'tinyblob':
						case 'mediumblob':
						case 'blob':
						case 'longblob':
						case 'medium':
						case 'text':
						case 'mediumtext':
						case 'longtext':
							$config->type = 'editor';
							$config->isRepeatable = true;
							break;

						default:
							$config->type = 'default';
							break;
					}
				}

				$data[$column] = $config->toArray();
			}

			//Merge into structure
			$this->elements = array_merge($this->elements, $data);
		}

		return $this;
	}


	/**
	 * @return bool
	 */
	public function save()
	{
		if(!is_string($this->elements))
		{
			$encoder = $this->getService('com://admin/elements.helper.json');
			$this->_data['elements'] = $encoder->json_encode(KConfig::unbox($this->elements), JSON_PRETTY_PRINT + JSON_NUMERIC_CHECK);
		}

		return parent::save();
	}


	public function getItem()
	{
		return $this->_item;
	}


	public function setItem(KDatabaseRowAbstract $item)
	{
		$this->_item = $item;
		$this->_elements->setItem($item);
		return $this;
	}


	public function getTypes()
	{
		$id = explode('.',$this->id);
		$state = array('type' => $id[0]);
		if($this->path) $state[] = $this->path;

		return $this->getService('com://admin/elements.model.structures')->id(null)->set($state)->getList();
	}


	public function getType()
	{
		if(!$this->hasType()) return false;

		$id = explode('.',$this->id);
		return array_pop($id);
	}


	/**
	 * @return mixed
	 */
	public function getElementTypes()
	{
		return $this->getService('com://site/elements.model.elements')->set('package', $this->package)->getList();
	}


	/**
	 * @param array $options
	 * @return object
	 */
	public function getElements($options = array())
	{
		$options = new KConfig($options);
		$options->append(array(
			'excludeSkipped' => true,
			'excludeHidden' => true,
			'elements' => null,
			'item' => null,
			'mode' => 'edit'
		));

		$elements = $this->elements;
		$element_set = null;

		//If we're given a subset of elements, clone the element set and empty it
		$restrict = $options->elements ? (array) $options->elements->toArray() : array();
		if(count($restrict)){

			$elements_subset = array();
			foreach($restrict AS $element){

				$element_id = isset($element['id']) ? $element['id'] : null;

				if(isset($elements[$element_id])){
					$base_element = $elements[$element_id];

					if($type = (isset($base_element['type']) ? $base_element['type'] : null)){

						//Ensure the base element has config properties
						unset($base_element['id']);
						unset($base_element['type']);
						$base_element = array(
							'id' => $element_id,
							'type' => $type,
							'fixed' => isset($element['fixed']) ? $element['fixed'] : false,
							'isRepeatable' => isset($element['isRepeatable']) ? $element['isRepeatable'] : false,
							'config' => array('edit' => $base_element, 'render' => array()));

						//Remove the element id and type, these are already stored in the base_element
						unset($element['id']);
						unset($element['type']);

						//Get the config and merge into the base element
						$config = isset($element['config']) && isset($element['config'][$options->mode]) ? $element['config'][$options->mode] : $element;
						$base_element['config'][$options->mode] = array_merge($base_element['config'][$options->mode], $config);

						$elements_subset[] = $base_element;
					}
				}
			}

			$elements = $elements_subset;
			$element_set = clone $this->_elements;
			$element_set->erase();
		}

		//Loop through elements and add to the global set and custom set if set
		foreach($elements AS $id => $element)
		{
            if(!isset($element['type'])) continue;
			$type = $element['type'];
			$id = isset($element['id']) ? $element['id'] : (!is_numeric($id) ? $id : null);

			//Remove type and id
			unset($element['type']);
			unset($element['id']);

            if($type == '-skip-' && $options->excludeSkipped) continue;
            if($type == 'hidden' && $options->excludeHidden) continue;

			$element = array(
				'id' => $id,
				'type' => $type,
				'fixed' => isset($element['fixed']) ? $element['fixed'] : false,
				'isRepeatable' => isset($element['isRepeatable']) ? $element['isRepeatable'] : false,
				'config' => array(
					'edit' => isset($element['config']) && isset($element['config']['edit']) ? $element['config']['edit'] : $element,
					'render' => isset($element['config']) && isset($element['config']['render']) ? $element['config']['render'] : array()
				)
			);

			if(!$this->_elements->$id){
				$element['item'] = $this->_item;
				$element = $this->_elements->setElement($element);
			}else{
				$config = isset($element['config']) ? $element['config'] : array();
				$element = $this->_elements->$id;
				if(isset($config['edit'])) $element->setConfig($config['edit'], 'edit');
				if(isset($config['render'])) $element->setConfig($config['render'], 'render');
			}

			//Add to element set if it exists
			if($element_set) $element_set->insert($element);
		}

        return $element_set ?: $this->_elements;
	}


    public function getElementIds()
    {
        return $this->elements ? array_keys($this->elements) : array();
    }


	/**
	 * @param $id
	 * @return mixed
	 */
	public function getElement($id)
	{
		return $this->getElements()->$id;
	}


	/**
	 * @param string $type
	 * @param bool $admin
	 * @return mixed
	 */
	public function getLayouts($state = array())
	{
		$state = new KConfig($state);
		$state->append(array(
			'type' => 'com',
			'target' => null,
			'admin' => false,
			'submission' => false,
			'package' => $this->package,
			'structure' => $this->id,
			'path' => $this->path
		));

		$layouts = $this->getService('com://admin/elements.model.layouts')->set($state->toArray())->getList();

		if($state->type == 'com')
		{
			$hasFull = $hasListing = $hasTeaser = false;
			foreach($layouts AS $layout)
			{
				if($layout->name == 'full') $hasFull = true;
				if($layout->name == 'listing') $hasListing = true;
			}

			if($hasFull || $hasListing){
				$data = $state->toArray();

				//Set the full layout as we have a fallback for this in com_elements
				if(!$hasFull && ((!$state->admin && !$state->submission) || ($state->admin && $state->submission)))
				{
					$data['name'] = 'full';
					$layout = $layouts->getTable()->getRow(array('data' => $data));
					$layouts->insert($layout);
				}

				//Set the listing layout as we have a fallback for this in com_elements
				if(!$hasListing && !$state->admin && !$state->submission)
				{
					$data['name'] = 'listing';
					$layout = $layouts->getTable()->getRow(array('data' => $data));
					$layouts->insert($layout);
				}
			}
		}

		return $layouts;
	}


	public function getLayout($name, $state = array())
	{
		static $layouts = array();

		$state = new KConfig($state);
		$state->append(array(
			'name' => $name,
			'type' => 'com',
			'target' => null,
			'admin' => false,
			'submission' => false,
			'package' => $this->package,
			'structure' => $this->id,
			'path' => $this->path
		));

		//Cache the layout
		$hash = md5(serialize($state));
		if(!isset($layouts[$hash])){
			$layouts[$hash] = $this->getService('com://admin/elements.model.layouts')->set($state->toArray())->getItem();
		}

		$layout = clone $layouts[$hash];
		$layout->setStructure($this);

		return $layout;
	}


	/**
	 * @return array
	 */
	public function getGroups()
	{
		$columns = $this->getTable()->getColumns();
		return isset($columns['elements']) ? array('elements') : array();
	}


	/**
	 * Checks if the target table has a type column
	 * @return bool
	 */
	public function hasType()
	{
		if($table = $this->getTargetTable())
		{
			$columns = $table->getColumns();
			return isset($columns['type']);
		}

		return false;
	}
}