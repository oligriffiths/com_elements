<?php
/**
 * User: Oli Griffiths
 * Date: 17/07/2012
 * Time: 18:17
 *
 * @example_usage
 * @todo: Complete this
 *
 */
class ComElementsDatabaseRowLayout extends KDatabaseRowDefault
{
	protected $_identifier;

	protected $_package;


	/**
	 * Stores the layout structure
	 *
	 * @var $_structure
	 * @type ComElementsDatabaseRowStructure
	 */
	protected $_structure;

	/**
	 * Stores a template layout object
	 *
	 * @var $_template
	 * @type ComElementsTemplateLayout
	 */
	protected $_template;

	/**
	 * Stores the file base path
	 *
	 * @var $_base_path
	 * @type String
	 */
	protected $_base_path;

	/**
	 * Stores the layout positions
	 *
	 * @var $_positions
	 * @type KConfig
	 */
	protected $_positions;

	/**
	 * Store the raw position information
	 * @var array
	 */
	protected $_positions_raw = array();

	/**
	 * @var bool
	 */
	protected $_positions_loaded = false;


	/**
	 * Setup layout options
	 *
	 * @param  KConfig An optional KConfig object with configuration options.
	 * @return void
	 */
	public function __construct(KConfig $config)
	{
		$config->append(array(
			'data' => array(
				'admin' => false,
				'submission' => false,
				'elements' => array(),
				'positions' => array(),
				'cache' => false,
				'cache_expiry' => 0
			)
		));

		parent::__construct($config);

		//Setup positions holder
		$this->_positions = new KConfig();
		
		$this->_package = preg_replace('#_layouts$#','',$this->getTable()->getName());
	}


	/**
	 * Reset the position info on clone
	 */
	public function __clone()
	{
		$this->_positions = clone $this->_positions;

		//Re-assign the layout to each loaded position so it references the correct layout
		foreach($this->_positions->toArray() AS $position => $params){
			if(isset($this->_positions->$position)){
				$this->_positions->$position->setLayout($this);
			}
		}
	}


	/**
	 * Overridden setData de-jsonise the layout info
	 * @param $data
	 * @param bool $modified
	 * @return ComElementsDatabaseRowStructure
	 */
	public function setData($data, $modified = true)
	{
		if(isset($data['elements']) && is_string($data['elements']) && trim($data['elements']))
		{
			$elements = json_decode($data['elements'], true);
			$error = json_last_error();

			if($error != JSON_ERROR_NONE)
			{
				//Set the data first
				parent::setData($data, $modified);

				$message = '';
				switch($error){
					case JSON_ERROR_DEPTH: $message = 'No error has occurred'; break;
					case JSON_ERROR_STATE_MISMATCH: $message = 'The maximum stack depth has been exceeded'; break;
					case JSON_ERROR_CTRL_CHAR: $message = 'Invalid or malformed JSON'; break;
					case JSON_ERROR_SYNTAX: $message = 'Control character error, possibly incorrectly encoded'; break;
					case JSON_ERROR_UTF8: $message = 'Syntax error'; break;
				}

				$id = $this->getPathIdentifier();
				throw new Exception('Layout '.$id.' contains a parse error: '.$message);
			}

			$data['elements'] = new KConfig($elements);
		}

		parent::setData($data, $modified);

		return $this;
	}


	/**
	 * Overridden set de-jsonise the layout info
	 * @param string $column
	 * @param mixed $value
	 */
	public function __set($column, $value)
	{
		if($column == 'elements' && is_string($value) && trim($value))
		{
			$elements = json_decode($value, true);
			if(!$elements)
			{
				throw new Exception('Layout '.$this->id.' elements contains a parse error');
			}
			$value = new KConfig($elements);
		}

		parent::__set($column, $value);
	}

	/**
	 * Sets the layout options
	 *
	 * @param $options
	 * @return ComElementsDatabaseRowLayout $this
	 */
	public function setCache($cache = null, $expiry = null)
	{
		if(!is_null($cache)) $this->cache = $cache;
		if(!is_null($expiry)) $this->cache_expiry = $expiry;

		return $this;
	}


	/**
	 * Load the position data from the positions file
	 * @return ComElementsDatabaseRowLayout
	 * @throws Exception
	 */
	public function loadPositions()
	{
		if($this->_positions_loaded) return $this;

		$path = $this->getPath('position');
		if(!file_exists($path))
		{
			throw new Exception('No position information found for '.$this->id.' : '. $path);
		}

		$file = file_get_contents($path);
		if(!trim($file))
		{
			throw new Exception('Position file empty: '.$path.'');
		}

		$this->_positions_raw = json_decode($file, true);
		if($this->_positions_raw == false)
		{
			throw new Exception('Unable to parse position file: '.$path.'');
		}
		$this->setData(array('positions' => new KConfig($this->_positions_raw)), false);

		$this->_positions_loaded = true;

		return $this;
	}


	/**
	 * Gets the path based on the path identifier
	 * @param $type
	 * @return array|string
	 */
	public function getPath($type)
	{
		$identifier = $this->getPathIdentifier();

		$path = '';
		switch($type)
		{
			case 'position':
				$path = dirname($identifier->filepath).'/'.$identifier->name.'.json';
				break;

			case 'layout':
				$path = $identifier->filepath;
				break;
		}

		return $path;
	}


	/**
	 * Constructs the path identifier:
	 *      First looking in the media folder
	 *      Then in the components view folder
	 *      Then fallback to com_elements
	 * @return KServiceIdentifier
	 */
	public function getPathIdentifier()
	{
		if(!isset($this->_identifier))
		{
			//Refactor the id to add the identfier type
			$path = array();

			//Add path if set
			if($this->path) $path = explode('.', $this->path);

			//Add admin/site folder
			$path['app'] = $this->admin ? 'admin' : 'site';

			//Add template if set
			if($this->template)
			{
				$path[] = 'templates';
				$path[] = $this->template;
			}

			//Add structre ID
			$path = array_merge($path, explode('.', $this->structure));

			//Add submissions/renderers
			$path[] = $this->submission ? 'submissions' : 'renderers';

			//Set the type if not set
			if(!$this->target)
			{
				$type = 'com';
				$package = $this->target ? $this->target : $this->_package;
				$this->setData(array('type' => $type, 'package' => $package), false);
			}

			//Check for a media overrider for the identifier
			$media_identifier = clone $this->getIdentifier();
			$media_identifier->type = 'media';
			$media_identifier->application = 'site';
			$media_identifier->package = $this->type.'_'.$this->package;
			$media_identifier->path = $path;
			$media_identifier->name = $this->name;

			//Check the folder exists
			if(is_dir(dirname($media_identifier->filepath)))
			{
				$this->_identifier = $media_identifier;
				return $this->_identifier;
			}

			//Remove the admin/site part of the path
			unset($path['app']);

			//Fall back to the packages view folder
			array_unshift($path, 'view');
			$component_identifier = clone $this->getIdentifier();
			$component_identifier->type = $this->type;
			$component_identifier->application = $this->admin ? 'admin' : 'site';
			$component_identifier->package = $this->target ? $this->target : $this->_package;
			$component_identifier->path = $path;
			$component_identifier->name = $this->name;

			//Check if the dir exists
			if(is_dir(dirname($component_identifier->filepath)))
			{
				$this->_identifier = $component_identifier;
				return $this->_identifier;
			}

			//Fall back to com_elements
			$default_identifier = clone $this->getIdentifier();
			$default_identifier->type = $this->type;
			$default_identifier->application = $this->admin ? 'admin' : 'site';
			$default_identifier->package = 'elements';
			$default_identifier->path = array('view','item', $this->submission ? 'submissions' : 'renderers');
			$default_identifier->name = $this->name;

			//Check if the dir exists
			if(file_exists($default_identifier->filepath))
			{
				$this->_identifier = $default_identifier;
				return $this->_identifier;
			}

			return $component_identifier;
		}

		return $this->_identifier;
	}


	/**
	 * Constructs the href for the layout, used in the backend
	 * @return string
	 */
	public function getHref()
	{
		$parts = array();
		$parts['package']   = $this->_package;
		$parts['type']      = $this->type;
		$parts['name']      = $this->name;
		$parts['structure'] = $this->structure;
		if($this->admin)        $parts['admin']     = $this->admin;
		if($this->submission)   $parts['submission']= $this->submission;
		if($this->path)         $parts['path']      = $this->path;
		if($this->target)       $parts['target']    = $this->target;

		return http_build_query($parts);
	}


	/**
	 * Jsonises the elements data on save
	 * @return bool
	 */
	public function save()
	{
		if($this->elements && !is_string($this->elements))
		{
			$encoder = $this->getService('com://admin/elements.helper.json');
			$this->_data['elements'] = $encoder->json_encode(KConfig::unbox($this->elements), JSON_PRETTY_PRINT + JSON_NUMERIC_CHECK);
		}

		return parent::save();
	}


	/**
	 * Returns an position by name
	 * @param $position
	 * @return null
	 */
	public function getPosition($name)
	{
		$this->loadPositions();

		if(!isset($this->positions->$name)) return false;

		if(!isset($this->_positions->$name))
		{
			$this->_positions->$name = $this->getService('com://site/elements.template.position', array(
				'name' => $name,
				'layout' => $this,
				'title' => ucfirst($name),
				'elements' => $this->elements && isset($this->elements[$name]) ? $this->elements[$name] : array(),
				'item' => $this->getItem()
			));
		}

		return $this->_positions->$name;
	}


	/**
	 * Gets the positions assigned to the layout
	 * @return mixed
	 */
	public function getPositions()
	{
		$this->loadPositions();

		$positions = array();
		foreach(array_keys( KConfig::unbox($this->positions) ) AS $name)
		{
			$positions[$name] = $this->getPosition($name);
		}

		return $positions;
	}


	/**
	 * Gets the structure for the layout
	 * @return mixed
	 */
	public function getStructure()
	{
		if(!$this->_structure)
		{
			//If structure wasnt found from the item, get it from the name
			if(!$this->_structure)
			{
				$state = array('package' => $this->_package, 'id' => $this->structure);
				$this->_structure = $this->getService('com://admin/elements.model.structures')->set($state)->getItem();
			}
		}
		return $this->_structure;
	}


	/**
	 * Sets the layout structure
	 * @param ComElementsDatabaseRowStructure $structure
	 * @return ComElementsDatabaseRowLayout
	 */
	public function setStructure(ComElementsDatabaseRowStructure $structure)
	{
		$this->_structure = $structure;
		return $this;
	}


	/**
	 * Return the set item
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->_structure->getItem();
	}


	/**
	 * Sets the item in the layout
	 * @param $item
	 * @return ComElementsDatabaseRowLayout
	 */
	public function setItem($item)
	{
		$this->_structure->setItem($item);
		return $this;
	}


	/**
	 * Returns the template for the layout
	 * @return mixed
	 */
	public function getTemplate()
	{
		if(!$this->_template)
		{
			$identifier = clone $this->getIdentifier();
			$identifier->application = 'site';
			$identifier->package = $this->_package;
			$identifier->path = 'template';
			$identifier->name = 'layout';

			//Allows for package override
			if(!file_exists($identifier->filepath)) $identifier->package = 'elements';

			//Get the template
			$this->_template = $this->getService($identifier, array('layout' => $this, 'cache' => $this->cache, 'cache_expiry' => $this->cache_expiry));
		}

		return $this->_template;
	}


	/**
	 * Renders the layout
	 * @return mixed
	 */
	public function render()
	{
		//Assign
		$data = array();
		$data['item'] = $this->getItem();
		$identifier = $this->getPathIdentifier();

		return $this->getTemplate()->loadIdentifier($identifier, $data)->render();
	}
}