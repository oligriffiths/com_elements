<?php
/**
 * Created By: Oli Griffiths
 * Date: 25/09/2012
 * Time: 16:00
 */
defined('KOOWA') or die('Protected resource');

class ComElementsDatabaseTableStructures extends ComElementsDatabaseTableDefault
{
	public static function getInstance(KConfigInterface $config, KServiceInterface $container)
	{
		$identifier = $config->service_identifier.'.'.$config->name;

		// Check if an instance with this identifier already exists or not
		if (!$container->has($identifier))
		{
			//Create the singleton
			$classname = $config->service_identifier->classname;
			$instance  = new $classname($config);
			$container->set($identifier, $instance);
		}

		return $container->get($identifier);
	}

	/**
	 * Set elements column not to be filtered
	 * @param KConfig $config
	 */
	public function _initialize(KConfig $config)
	{
		parent::_initialize($config);
		$config->append(array(
			'behaviors' => array('com://site/codebox.database.behavior.cacheable'),
			'filters' => array('elements' => 'raw')
		));
	}
}