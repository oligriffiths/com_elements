<?php
/**
 * User: Oli Griffiths
 * Date: 13/09/2012
 * Time: 17:23
 */

class ComElementsDatabaseTableJson extends KDatabaseTableAbstract
{
	public function getSchema()
	{
		return parent::getSchema();
	}

	public function select($query = null, $mode = KDatabase::FETCH_ROWSET)
	{
		return parent::select($query, $mode);
	}

	public function count($query = null)
	{
		return parent::count($query);
	}

	public function insert(KDatabaseRowInterface $row)
	{
		return parent::insert($row);
	}

	public function update(KDatabaseRowInterface $row)
	{
		return parent::update($row);
	}

	public function delete(KDatabaseRowInterface $row)
	{
		return parent::delete($row);
	}

}