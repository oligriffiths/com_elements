<?php
/**
 * Created By: Oli Griffiths
 * Date: 01/06/2012
 * Time: 10:16
 */
defined('KOOWA') or die('Protected resource');

class ComElementsDatabaseTableDefault extends KDatabaseTableDefault
{
	public function _initialize(KConfig $config)
	{
		parent::_initialize($config);

		$this->_name        = $config->name;
		$this->_base        = $config->base;
		$this->_database    = $config->database;

		/**
		 * Attempt to set the identity column if we find only 1 primary column
		 */
		if(!$this->_identity_column)
		{
			foreach ($this->getColumns(true) as $column)
			{
				//Find auto increment columns or none-composite primary columns
				if($column->primary && empty($column->related)) {
					$this->_identity_column = $column->name;
				}
			}
		}
	}


	/**
	 * Fix to cater for non-autoincremented primary columns being overwritten with db insert id
	 * @param KDatabaseRowInterface $row
	 * @return bool|int
	 */
	public function insert(KDatabaseRowInterface $row)
	{
		//Get the identity column value
		$id_col = array_search($this->getIdentityColumn(), $this->_column_map);
		$id = null;
		if($id_col) $id = $row->get($id_col);

		//Run the insert
		$affected = parent::insert($row);

		//If insert worked, and we had an id set, attempt to re-set
		if($affected && $id){
			$column = $this->getColumn($id_col);
			if(!$column->autoinc) $row->setData(array($id_col => $id), false);
		}

		return $affected;
	}


}