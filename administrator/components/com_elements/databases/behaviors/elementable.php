<?php
/**
 * User: Oli Griffiths
 * Date: 23/07/2012
 * Time: 22:07
 */
class ComElementsDatabaseBehaviorElementable extends KDatabaseBehaviorAbstract
{
	/**
	 * Stores the layout structure
	 *
	 * @var $_structures
	 * @type array of ComElementsDatabaseRowStructure
	 */
	protected $_structures = array();

	/**
	 * Stores hashes of element objects
	 *
	 * @var $_data
	 * @type array of object SPL hashes
	 */
	protected $_data = array();


	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'priority'   => KCommand::PRIORITY_LOW
		));

		parent::_initialize($config);

		//Initialize com_elements
		$this->getService('com://site/elements.initialize');
	}


	/*******************
	 * Database Behaviors
	 *******************/
	protected function _beforeTableInsert(KCommandContext $context)
	{
		return $this->_beforeTableUpdate($context);
	}


	/**
	 * Get the element data and merge into item
	 * @param KCommandContext $context
	 */
	protected function _beforeTableUpdate(KCommandContext $context)
	{
		$this->setMixer($context->data);
		$data = $this->getElementData();
		$context->data->setData($data);
	}


	/**
	 * Run after table insert, delegates the on after table update method
	 *
	 * @param KCommandContext $context
	 * @return bool true/false
	 */
	protected function _afterTableInsert(KCommandContext $context)
	{
		return $this->_afterTableUpdate($context);
	}


	/**
	 * Run after table update to save any elements that rely on the item being created first, eg image el
	 * and update search index
	 *
	 * @param KCommandContext $context
	 * @return bool true/false
	 */
	protected function _afterTableUpdate(KCommandContext $context)
	{
        //Get and reset element data
        $id = $this->getMixerId();
        unset($this->_data[$id]);
        $context->data->decodeData();
        $data = $context->data->getData();
		$element_data = $this->getElementData($data);
		$context->data->setData($element_data);

		if(count($context->data->getModified()))
		{
			//Disable the command chain, its already been run
			$context->data->getTable()->getCommandChain()->disable();
			$context->data->save();
			$context->data->getTable()->getCommandChain()->enable();
            $context->data->setData($data, false);
		}

		//Update the search index
		return $this->updateSearchIndex($context);
	}


    /**
     * Clear any static data from previous selects
     *
     * @param KCommandContext $context
     */
    protected function _beforeTableSelect(KCommandContext $context)
    {
        $this->_structures = array();
        $this->_data = array();
    }


	/**
	 * Mix elementable methods into the row
	 *
	 * @param KCommandContext $context
	 * @return void
	 */
	protected function _afterTableSelect(KCommandContext $context)
	{
		if($context->data instanceof KDatabaseRowAbstract)
		{
			$context->data->isElementable();
            $context->data->decodeData();
		}

		if($context->data instanceof KDatabaseRowsetDefault)
		{
			foreach($context->data AS $row){
                $row->isElementable();
                $row->decodeData();
            }
		}
	}


	/**
	 * Run delete on all elements
	 * @param KCommandContext $context
	 */
	protected function _afterTableDelete(KCommandContext $context)
	{
		$elements = $context->data->getElements();
		foreach($elements AS $element)
		{
			$element->delete();
		}
	}


	/**
	 * Runs before table update
	 *
	 * @param KCommandContext $context
	 * @return void
	 */
	protected function getElementData()
	{
		// Check elementable methods are available in the row
		$item = $this->getMixer();

		$data = $item->getData();
		$table = $item->getTable();
		$columns = $table->getColumns();
		$associations = $item->isAssociatable() ? $item->getAssociations() : array();
		$hasElementsCol = isset($columns['elements']) && in_array($columns['elements']->type, array('text','mediumtext','longtext','blob','mediumblob','longblob'));

		//Get the element data and merge into relevant fields
		$elements = $this->getElements();
		$elementsData = array();

		$encoder = $this->getService('com://admin/elements.helper.json');

		$valid = true;
		foreach($elements AS $id => $element)
		{
            //Check if a group is defined
            $group = $element->getConfig('group');
            $group = $group ? $group : ($hasElementsCol ? 'elements' : NULL);

			//If element is required, it must have an actual value, not 0
			if($element->isRequired() && !$element->hasValue(null, null, true)){
				$valid = false;
				throw new KException(sprintf('The field %s is required', $id));

			}else if($element->hasValue()){

				// Check the element validates
				$valid = $element->validate();

				if($valid === true)
				{
					$result = $element->save();
					if($result === true){
						$value = $element->getSubmissionValue();

						// If there are no values skip element
						if(is_null($value)) continue;

						//Check if the element id is an existing column or its an associatable property
						if(isset($columns[$id]) || isset($associations[$id]))
						{
							//JSON encode repeatable or multifield elements
							if($element->isRepeatable() || $element->isMultifield()) $value = $encoder->json_encode($value, JSON_PRETTY_PRINT + JSON_NUMERIC_CHECK);

							//Store data value
							$data[$id] = $value;
						}

						// If there is a group and the coulmn is not a standard column
						elseif($group && !isset($columns[$id]))
						{
							//Add the element to the group
							if(!isset($elementsData[$group]))
							{
								$elementsData[$group] = array();
							}

							// Set the element data value for this element
							$elementsData[$group][$id] = $value;

							//Also set the value in the data
							$data[$id] = $value;
						}
					}else if($result){
						throw new KException($result);
					}
				}else if($valid){
					throw new KException($valid);
				}

			}else{

				if(isset($columns[$id]) || isset($associations[$id]))
				{
					unset($data[$id]);
				}else{
                    if($group){
                        if(!isset($elementsData[$group]))
                        {
                            $elementsData[$group] = array();
                        }

                        unset($elementsData[$group][$id]);
                    }else{
                        unset($elementsData[$id]);
                    }
                }
			}
		}

		//If item is invalid, store toe session
		if(!$valid && $item->isValidatable()){
			$item->storeToSession();
			return false;
		}

		//Loop the element groups and encode
		foreach(array_keys($elementsData) AS $group)
		{
			//If the group is a column, encode the data
			if(isset($columns[$group]) && isset($data[$group]))
			{
				$data[$group] = $encoder->json_encode($elementsData[$group], JSON_PRETTY_PRINT + JSON_NUMERIC_CHECK);
			}
		}

		//Set the new data
		return $data;
	}


	/**
	 * Checks for and updates a search index table if present
	 * @return bool
	 */
	public function updateSearchIndex(KCommandContext $context)
	{
		if(!$context->data instanceof KDatabaseRowAbstract) return;

		//Set the mixer
		$this->setMixer($context->data);

		// Get the mixer
		$mixer = $context->data;

		// Check mixer is an instance of KDatabaseRowAbstract
		try
		{
            $identifier         = clone $mixer->getTable()->getIdentifier();
			$identifier->name   = KInflector::singularize($mixer->getIdentifier()->name).'_search_indexes';
			$table              = $this->getService($identifier);
			$mixer_pk           = $mixer->getTable()->getIdentityColumn();

			// Build query to look for existing search index
			$query = $table->getDatabase()->getQuery();
			$query->select('*')
				  ->from($table->getName())
				  ->where($mixer_pk, '=', $mixer->id);

			// Load existing index rowset if exists
			$existing = $table->select($query, KDatabase::FETCH_ROWSET);

			//Re-index the existing elements array into element_id => index
			$existing_indexes = array();

			foreach($existing AS $index)
			{
				if(!isset($existing_indexes[$index->element_id])) $existing_indexes[$index->element_id] = array();
				if(!isset($existing_indexes[$index->element_id][$index->index])) $existing_indexes[$index->element_id][$index->index] = array();
				$existing_indexes[$index->element_id][$index->index][$index->name] = $index;
			}

			//Get the index data
			$elements = $this->getElements();
			$search_data = array();

			foreach($elements AS $element)
			{
				$search_data[$element->id] = $element->getSearchData();
			}

			//Loop through the search data and insert/update the index table
			$used = array();
			foreach($search_data AS $element_id => $data)
			{
				foreach($data AS $value)
				{
					$value = is_array($value) ? new KConfig($value) : $value;
					if(is_null($value->value) || $value->value === '' || !is_scalar($value->value)) continue;

					//Check for an existing row
					if( isset($existing_indexes[$element_id]) &&
						isset($existing_indexes[$element_id][$value->index]) &&
						isset($existing_indexes[$element_id][$value->index][$value->name])) $row = $existing_indexes[$element_id][$value->index][$value->name];
					else $row = $table->getRow();

					//Set row data and save
					$row->$mixer_pk = $mixer->id;
					$row->element_id = $element_id;
					$row->index = $value->index;
					$row->name = $value->name;
					$row->value = $value->value;

					try{
						$row->save();
						$used[$element_id] = true;
					}

					// Catch exceptions
					catch(Exception $e){}
				}
			}

			//Remove unused data
			foreach($existing AS $row)
			{
				if(!isset($used[$row->element_id])) $row->delete();
			}

		}

		// Catch exceptions
		catch(Exception $e){
			//var_dump($e); exit;
		}

		return true;
	}


	/*******************
	 * MIXIN's
	 *******************/

	/**
	 * De-jsonize the element data and set in the mixer
	 *
	 * @param $data
	 * @return ComElementsDatabaseBehaviorElementable
	 */
	public function decodeData()
	{
        $row = $this->getMixer();
		$id = $this->getMixerId();

        $data = $row->getData();

        if(!isset($this->_data[$id]))
        {
            //JSON decode data
            if($structure = $this->getStructure())
            {
                $groups = $structure->getGroups();
                $elements = $structure->getElementIds();

                foreach($data AS $key => $value)
                {
                    $value = is_string($value) ? trim($value) : $value;

                    //Ensure the data is json
                    if(is_string($value) && preg_match('#^(\s|\n)*{#', $value) && preg_match('#(\s|\n)*}$#', $value)){

                        //Check if this column is a group column
                        if(in_array($key, $groups))
                        {
                            if($json = json_decode($value, true))
                            {
                                if(json_last_error() == JSON_ERROR_NONE){
                                    $data = array_merge($data, $json);
                                }
                            }

                        //Else check this is an element column & decode
                        }else if(in_array($key, $elements)){
                            if($json = json_decode($value, true)){
                                if(json_last_error() == JSON_ERROR_NONE){
                                    $data[$key] = $json;
                                }
                            }
                        }
                    }
                }

                //Store the data
                $this->_data[$id] = $data;
            }else{
                $this->_data[$id] = false;
            }
        }

        //Set the row data
        if($this->_data[$id] && $this->_data[$id] != $row->getData()) $row->setData($this->_data[$id], false);

		return $this;
	}


	/**
	 * Gets a unique identifier for the mixer
	 * @return mixed
	 */
	protected function getMixerId(){
		$mixer = $this->getMixer();

        $id = (string) $mixer->getIdentifier();
        foreach(array_keys($mixer->getTable()->getUniqueColumns()) AS $column_id){
            $id .= '.'.$mixer->get($column_id);
        }

		return $id;
	}


	/**
	 * Returns the structure attached to the item
	 * @return ComElementsDatabaseRowStructure
	 */
	public function getStructure()
	{
		static $structures;

		$mixer = $this->getMixer();
		$hash = spl_object_hash($mixer);

		//Check if we already have the structure for the item
		if(!isset($structures[$hash])){

			$identifier = $mixer->getIdentifier();
			$id = (string) $identifier;

			if(!isset($this->_structures[$id]))
			{
				$identifier = $this->getMixer()->getIdentifier();

	            $state = array(
		            'package' => $identifier->package,    //Set the componant/package
		            'id' => array(),                        //Set the ID, an array to be imploded
		            'path' => array()                       //Set an optional path
	            );

	            //Attempt to get an ID from the item
	            if(method_exists($mixer, 'getStructurePath'))
	            {
		            $path = $this->getMixer()->getStructurePath();
		            if($path) $state['path'] = (array) $path;
	            }

				//Add the item identifier name to the ID (eg, the singular table name)
				$state['id'][] = $identifier->name;

				//Set type if type is a column
				$column = $mixer->getTable()->getColumn('type');
				if($column)
				{
					if($mixer->isNew() && !$mixer->type) $mixer->type = KRequest::get('request.type','word');
					if($mixer->type) $state['id'][] = $mixer->type;
				}

				//Implode id an path
				$state['id'] = implode('.',$state['id']);
				$state['path'] = implode('.',$state['path']);

				//Get the structure
				$this->_structures[$id] = $this->getService('com://admin/elements.model.structures')->set($state)->getItem();
			}

			$structures[$hash] = isset($this->_structures[$id]) ? clone $this->_structures[$id] : null;

			if($structures[$hash]) $structures[$hash]->setItem($mixer);
		}

		return $structures[$hash];
	}


    /**
     * Gets the structure types
     * @return array
     */
    public function getTypes()
    {
        if($structure = $this->getStructure())
        {
            return $structure->getTypes();
        }

        return array();
    }

	/**
	 * Renders the layout attached to the item
	 *
	 * Allows template rendering direct from item ($item->render('layout')
	 * Calls Chain: Layout->render()
	 * 				Template->render()
	 * 				Template->renderPosition()
	 * 				Layout->renderPosition()
	 * 				Position->render()
	 * 				Structure->getElements()
	 * 				Element->render()
	 *
	 * @param $layout_id
	 * @param array $options - see ComElementsDatabaseRowLayout
	 *
	 * @example_usage
	 * 				package/views/item/tmpl/default.php
	 * 					<?= $item->render('full') ?>
	 * 				package/views/item/renderers/full.php
	 * 					<?= $this->checkPosition('position1') ? $this->renderPosition('position') : null ?>
	 * 					<?= $this->checkPosition('position2') ? $this->renderPosition('position2') : null ?>
	 * 				package/views/item/renderers/full.json
						See com_elements/database/row/layout.php descriptor
	 * @return mixed
	 */
	public function render($layout_id, $options = array())
	{
		$options = new KConfig($options);
		$options->append(array(
			'cache' => null,
			'type' => 'com',
			'admin' => JFactory::getApplication()->isAdmin()
		))->append(array(
			'submission' => is_null($options->submission) ? $options->admin : $options->submission
		));

		//Validate the structure
		$structure = $this->getStructure();
		if(!$structure || $structure->isNew())
		{
			throw new Exception('');
			//throw new KBehaviorException('No structure defined for '.$this->getMixer()->getIdentifier());
		}

		//Attempt to get an ID from the item
		if(method_exists($this->getMixer(), 'getLayoutTemplate'))
		{
			$options->template = $this->getMixer()->getLayoutTemplate($options->submission, $options->admin);
		}

		//Get the layout from the structure
		$layout = $structure->getLayout($layout_id, $options);

		//Ensure this layout exists
		if(!$layout || $layout->isNew())
		{
            throw new Exception('');
			//throw new KBehaviorException('No layout "'.$layout_id.'" of type "'.$options->type.'" defined for '.$this->getMixer()->getIdentifier());
		}

		//Set the layout options & bind the item into the layout
		$layout->setCache($options->cache)->load();

		return $layout->render();
	}


	/**
	 * Gets an element from the structure
	 * @param $id
	 * @return object | null
	 */
	public function getElement($id)
    {
        $elements = $this->getElements(false);
        return $elements ? $elements->$id : null;
    }


	/**
	 * Gets all the elements from the structure
	 * @param array $options
	 * @return array|object
	 */
	public function getElements($options = array())
    {
        if($structure = $this->getStructure())
        {
            return $structure->getElements($options);
        }
        return array();
    }

/*
	public function setElementData($id, $data )
	{
		if($element = $this->getElement($id)){
			$element->setValues($data);
		}

		return $this;
	} */
}