<?php
/**
 * User: Oli Griffiths
 * Date: 13/09/2012
 * Time: 16:56
 */

class ComELementsHelperPath extends KObject
{
	protected $_paths;

	/**
	 * Force creation of a singleton
	 *
	 * @param 	object 	An optional KConfig object with configuration options
	 * @param 	object	A KServiceInterface object
	 * @return KDatabaseTableDefault
	 */
	public static function getInstance(KConfigInterface $config, KServiceInterface $container)
	{
		// Check if an instance with this identifier already exists or not
		if (!$container->has($config->service_identifier))
		{
			//Create the singleton
			$classname = $config->service_identifier->classname;
			$instance  = new $classname($config);
			$container->set($config->service_identifier, $instance);
		}

		return $container->get($config->service_identifier);
	}

	public function register($key, $path)
	{
		if(!isset($this->_paths[$key])) $this->_paths[$key] = array();
		array_push($this->_paths[$key], $path);
		return $this;
	}

	public function set($key, $paths)
	{
		$this->_paths[$key] = (array) $paths;
		return $this;
	}

	public function get($key)
	{
		return isset($this->_paths[$key]) ? $this->_paths[$key] : null;
	}

	public function last($key)
	{
		$paths = isset($this->_paths[$key]) ? $this->_paths[$key] : null;
		if(!$paths) return null;

		return array_pop($paths);
	}

	public function first($key)
	{
		$paths = isset($this->_paths[$key]) ? $this->_paths[$key] : null;
		if(!$paths) return null;

		return array_shift($paths);
	}
}