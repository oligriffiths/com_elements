<?php
/**
 * Created By: Oli Griffiths
 * Date: 03/09/2012
 * Time: 15:01
 */
defined('KOOWA') or die('Protected resource');

class ComElementsHelperParameter extends KObjectDecorator
{
	protected $_xml = array();

	public function __construct(KConfig $config = null)
	{
		parent::__construct($config);
		$config->append(array(
			'params' => '',
			'path' => ''
		));

		jimport('joomla.html.parameter');
		$this->_object = new JParameter($config->params, $config->path);
		$this->_object->addElementPath(dirname(__FILE__).'/parameters');
	}


	/**
	 * Sets the XML object from custom xml files
	 *
	 * @access	public
	 * @param	object	An XML object
	 * @since	1.5
	 */
	function setXML( &$xml )
	{
		if (is_object( $xml ))
		{
			if ($group = $xml->attributes( 'group' )) {
				$this->_xml[$group] = $xml;
			} else {
				$this->_xml['_default'] = $xml;
			}

			$this->_object->setXML($xml);
		}
	}


	/**
	 * Render all parameters
	 *
	 * @access	public
	 * @param	string	The name of the control, or the default text area if a setup file is not found
	 * @return	array	Aarray of all parameters, each as array Any array of the label, the form element and the tooltip
	 * @since	1.5
	 */
	function getParams($name = 'params', $group = '_default')
	{
		if (!isset($this->_xml[$group])) {
			return false;
		}
		$results = array();
		foreach ($this->_xml[$group]->children() as $param)  {
			$results[] = $this->getParam($param, $name);
		}
		return $results;
	}


	/**
	 * Render a parameter type
	 *
	 * @param	object	A param tag node
	 * @param	string	The control name
	 * @return	array	Any array of the label, the form element and the tooltip
	 * @since	1.5
	 */
	function getParam(&$node, $control_name = 'params', $group = '_default')
	{
		//get the type of the parameter
		$type = $node->attributes('type');

		//remove any occurance of a mos_ prefix
		$type = str_replace('mos_', '', $type);

		$element =& $this->loadElement($type);

		// error happened
		if ($element === false)
		{
			$result = array();
			$result[0] = $node->attributes('name');
			$result[1] = JText::_('Element not defined for type').' = '.$type;
			$result[5] = $result[0];
			$result[6] = $type;
			return $result;
		}

		//get value
		$value = $this->_object->get($node->attributes('name'), $node->attributes('default'), $group);

		$result = $element->render($node, $value, $control_name);
		$result[6] = $type;
		return $result;
	}


	/**
	 * Render
	 *
	 * @access	public
	 * @param	string	The name of the control, or the default text area if a setup file is not found
	 * @return	string	HTML
	 * @since	1.5
	 */
	function render($name = 'params', $group = '_default')
	{
		$params = $this->getParams($name, $group)?:array();
		$html = array ();

		if (isset($this->_xml[$group]) && $description = $this->_xml[$group]->attributes('description')) {
			// add the params description to the display
			$desc	= JText::_($description);
			$html[]	= $desc;
		}

		$html[] = '<div class="form-horizontal jparameters">';


        foreach ($params as $param)
        {
            $html[] = '<div class="control-group param_'.($param[6]).'">';
            $html[] = '<div class="control-label">'.$param[0].'</div>';
            $html[] = '<div class="controls">';
            $html[] = $param[1];
            $html[] = $param[2] ? '<span class="help-block">'.$param[2].'</span>' : '';
            $html[] = '</div>';
            $html[] = '</div>';
        }
		$html[] = '</div>';

		if (count($params) < 1) {
			return false;
		}

		return implode("\n", $html);
	}
}
