<?php
/**
 * User: Oli Griffiths
 * Date: 29/07/2012
 * Time: 22:28
 */

if(!defined('JSON_PRETTY_PRINT_MISSING'))
{
	if(!defined('JSON_PRETTY_PRINT'))
	{
		define('JSON_PRETTY_PRINT', 128);
		define('JSON_PRETTY_PRINT_MISSING', 1);
	}else{
		define('JSON_PRETTY_PRINT_MISSING', 0);
	}
}

class ComElementsHelperJson extends KObject
{

	public static function json_encode($data, $options = 0)
	{
		if(!JSON_PRETTY_PRINT_MISSING) return json_encode($data, $options);

		$json = json_encode($data, $options);

		if($options & JSON_PRETTY_PRINT){

			$tokens = preg_split('|([\{\}\]\[,])|', $json, -1, PREG_SPLIT_DELIM_CAPTURE);
			$result = '';
			$indent = 0;

			$lineBreak = "\n";
			$ind = "    ";

			$inLiteral = false;
			foreach ($tokens as $token) {
				if ($token == '') {
					continue;
				}

				$prefix = str_repeat($ind, $indent);
				if (!$inLiteral && ($token == '{' || $token == '[')) {
					$indent++;
					if (($result != '') && ($result[(strlen($result) - 1)] == $lineBreak)) {
						$result .= $prefix;
					}
					$result .= $token . $lineBreak;
				} elseif (!$inLiteral && ($token == '}' || $token == ']')) {
					$indent--;
					$prefix = str_repeat($ind, $indent);
					$result .= $lineBreak . $prefix . $token;
				} elseif (!$inLiteral && $token == ',') {
					$result .= $token . $lineBreak;
				} else {
					$result .= ( $inLiteral ? '' : $prefix ) . $token;

					// Count # of unescaped double-quotes in token, subtract # of
					// escaped double-quotes and if the result is odd then we are
					// inside a string literal
					if ((substr_count($token, "\"") - substr_count($token, "\\\"")) % 2 != 0) {
						$inLiteral = !$inLiteral;
					}
				}
			}
			return $result;
		}

		return $json;
	}
}