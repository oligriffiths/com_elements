<?php
defined( 'KOOWA' ) or die( 'Restricted Access' );

//Initialize com_elements
KService::get('com://site/elements.initialize');

// Dispatch using a default view if not set
echo KService::get( 'com://admin/elements.dispatcher' )->dispatch();