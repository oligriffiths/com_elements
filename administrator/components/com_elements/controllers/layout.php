<?php

class ComElementsControllerLayout extends ComDefaultControllerDefault
{
	/**
	 * Constructor
	 *
	 * @param 	object 	An optional KConfig object with configuration options.
	 */
	public function __construct( KConfig $config )
	{
		parent::__construct( $config );

		$this->registerCallback( array( 'before.add','before.edit' ), array( $this, 'restructure' ) );
	}

    /**
     * Allow retrieval of non existant records
     * @param KCommandContext $context
     * @return KDatabaseRow|object
     */
    protected function _actionRead(KCommandContext $context)
    {
        return $this->getModel()->getItem();
    }


	/** Remove the _position alias
	 * @param KCommandContext $context
	 */
	public function restructure( KCommandContext $context )
	{
		if( $context->data->elements ){
			unset( $context->data->elements->_position );

			$positions = $context->data->elements->toArray();
			$new_positions = array();

			//Re-index the positions array
			foreach($positions AS $name => $position)
			{
				$new_position = array();
				foreach($position AS $element)
				{
					$id = key($element);
					$element = array_merge(array('id' => $id), current($element));

					//Reset the data
					$new_position[] = $element;
				}
				$new_positions[$name] = $new_position;
			}
			$context->data->elements = $new_positions;
		}
	}
}