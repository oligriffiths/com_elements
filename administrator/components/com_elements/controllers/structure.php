<?php 

class ComElementsControllerStructure extends ComDefaultControllerDefault
{
	/**
	 * Constructor
	 *
	 * @param 	object 	An optional KConfig object with configuration options.
	 */
	public function __construct( KConfig $config )
	{
		parent::__construct( $config );

		$this->registerCallback( array( 'before.add', 'before.edit' ), array( $this, 'formatElements' ) );
	}


    /**
     * Allow retrieval of non existant records
     * @param KCommandContext $context
     * @return KDatabaseRow|object
     */
    protected function _actionRead(KCommandContext $context)
    {
        return $this->getModel()->getItem();
    }


	protected function _actionGet(KCommandContext $context)
	{
		//Check if we are reading or browsing
		$action = KInflector::isSingular($this->getView()->getName()) ? 'read' : 'browse';

		//Execute the action
		$this->execute($action, $context);

		parent::_actionGet($context);

		return (string) $this->getView()->display();
	}


	/**
	 * Reformats elements to set custom groups as a the group
	 * @param $context
	 * @return mixed
	 */
	public function formatElements( $context )
	{
        if( !$context->data->elements ) return;

		//Re-index elements
		$elements = $context->data->elements;
		if( $elements ){
			$new_elements = array();
			foreach( $elements AS $id => $element )
			{
				//if( !isset( $element->id ) ) $element->id = $id;
				$new_elements[ $element->id ?: $id ] = $element;
				unset($element->id);

				//Set group name is group is custom
				if( $element->group == '*custom*' )
				{
					$element->group = $element->group_custom;
					$element->group_custom = true;
				}
                else
                {
					unset( $element->group_custom );
				}
			}
			$context->data->elements = $new_elements;
		}
	}
}