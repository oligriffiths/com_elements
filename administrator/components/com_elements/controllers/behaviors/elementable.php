<?php
/**
 * Created By: Oli Griffiths
 * Date: 14/01/2013
 * Time: 11:19
 */
defined('KOOWA') or die('Protected resource');

class ComElementsControllerBehaviorElementable extends KControllerBehaviorAbstract
{
	protected $_redirect;
	protected $_errors = array();

	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'priority'   => KCommand::PRIORITY_HIGH,
		));

		parent::_initialize($config);
	}


	/**
	 * Get an object handle
	 *
	 * Force the object to be enqueue in the command chain.
	 *
	 * @return string A string that is unique, or NULL
	 * @see execute()
	 */
	public function getHandle()
	{
		return KMixinAbstract::getHandle();
	}

	/**
	 * Executes the validate action before any "save" events and raises errors on redirect
	 * @param $name
	 * @param KCommandContext $context
	 * @return bool
	 */
	public function execute($name, KCommandContext $context)
	{
		if(in_array($name, array('before.add','before.edit'))){
			return $this->validateelements($context);
		}

		if(in_array($name, array('after.add','after.edit','after.apply','after.save'))){
			$this->setRedirect();
		}

		if(in_array($name, array('after.validateelements')))
		{
			$this->raiseErrors($context);
		}
		return parent::execute($name, $context);
	}



	protected function _actionValidateelements(KCommandContext $context)
	{
		// Check elementable methods are available in the row
		$item = $this->getModel()->getItem();

		if( $item )
		{
			if($item->isElementable())
			{
				$valid = true;
				$item->setData(KConfig::unbox($context->data));

				//Get the element data and merge into relevant fields
				$elements = $item->getElements();

				// Init errors array
				$errors = array();

				foreach($elements AS $id => $element)
				{
					//If element is required, it must have an actual value, not 0
					if($element->isRequired() && !$element->hasValue(null, null, true)){
						$valid = false;
						$errors[$id] = sprintf(JText::_('The field %s is required'), $id);

					}else if($element->hasValue()){
						try{
							// Check the element validates
							$result = $element->validate();

							if($result !== true){
								$errors[$id] = $result;
							}
						}catch(KException $e)
						{
							// Element failed validation, return false and set error
							$errors[$id] = $e->getMessage();
						}
					}
				}

				//If item is invalid, store to session
				if(!$valid){
					//Store errors
					$this->_errors = $errors;

					if($item->isValidatable()){
						$item->storeToSession();
					}

					$referrer = KRequest::referrer();
					if($referrer){
						$this->_redirect = (string) $referrer;
					}
					return false;
				}
			}
		}

		return true;
	}


	protected function setRedirect()
	{
		if($this->_redirect){
			$this->getMixer()->setRedirect($this->_redirect);
		}
	}


	/**
	 * Raises any errors that the row contains
	 * @param KCommandContext $context
	 */
	protected function raiseErrors(KCommandContext $context)
	{
		$model = $context->caller->getModel();
		$item = $model->getItem();

		if (count($this->_errors)) {

			$text = '';
			$isHtml = KRequest::format() == 'html';
			$identifier = $item->getIdentifier();

			foreach($this->_errors AS $key => $error)
			{
				if(!is_array($error)) $error = array($error);

				foreach($error AS $e){
					if($isHtml){
						$msg = (string) $e;
						if(class_exists('KMessage')){
							KMessage::setMessage($msg, 'error', $identifier, $key);
						}else if(class_exists('JApplication')){
							JFactory::getApplication()->enqueueMessage($msg,'error');
						}
					}else{
						$text .= 'Validation error: ('.$key.') : '.$e.' -- ';
					}
				}
			}
			if(!$isHtml) $this->setResponse($context, KHttpResponse::BAD_REQUEST, $text);
		}
	}
}