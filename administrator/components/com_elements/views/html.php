<?php
/**
 * Created By: Oli Griffiths
 * Date: 01/10/2012
 * Time: 14:23
 */
defined('KOOWA') or die('Protected resource');

class ComElementsViewHtml extends ComDefaultViewHtml
{
	public function __construct(KConfig $config)
	{
		parent::__construct($config);

		//Add alias filter for media:// namespace
		$this->getTemplate()->getFilter('alias')->append(
			array('assets://' => (string) KRequest::base().'/components/com_'.($this->getIdentifier()->package).'/assets/'), KTemplateFilter::MODE_READ | KTemplateFilter::MODE_WRITE
		);
	}
}