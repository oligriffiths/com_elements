
<?= @helper('behavior.mootools') ?>
<style src="assets://css/elements.css" />
<style src="assets://css/structures.css" />
<script src="media://lib_koowa/js/koowa.js" />

<?php if($structures): ?>

<form id="structures-form" action="" method="get" class="-koowa-grid bootstrap">
	<?//= @template('default_filter'); ?>
	<table class="adminlist">
		<thead>
			<tr>
				<th>Type</th>
				<th>Render Layouts</th>
                <th>Submission Layouts</th>
                <th>Module Layouts</th>
				<th>Plugin Layouts</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($structures AS $structure): ?>
			<tr>
				<td>
					<strong>
						<a href="<?= @route('view=structure&package='.$structure->package.'&id='.$structure->id) ?>"><?= ucfirst($structure->id) ?></a>
					</strong>
					(<a href="<?= @route('view=structure&package='.$structure->package.'&id='.$structure->id) ?>"><?= strtolower(@text('edit')) ?></a>)
                </td>

                <td>
	                <?= @template('_layouts', array('structure' => $structure, 'config' => array('admin' => false))); ?>
	                <?= @template('_layouts', array('structure' => $structure, 'config' => array('admin' => true))); ?>
				</td>
                <td>
	                <?= @template('_layouts', array('structure' => $structure, 'config' => array('submission' => true))); ?>
					<?= @template('_layouts', array('structure' => $structure, 'config' => array('admin' => true, 'submission' => true))); ?>
                </td>
				<td>
					<?= @template('_layouts', array('structure' => $structure, 'config' => array('type' => 'mod'))); ?>
					<?= @template('_layouts', array('structure' => $structure, 'config' => array('type' => 'mod', 'submission' => true))); ?>
				</td>
				<td>
					<?= @template('_layouts', array('structure' => $structure, 'config' => array('type' => 'plg'))); ?>
					<?= @template('_layouts', array('structure' => $structure, 'config' => array('type' => 'plg', 'submission' => true))); ?>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="5">
				</td>
			</tr>
		</tfoot>
	</table>
</form>

<?php else: ?>
	<div class="well hero-unit" style="margin-bottom: 0">
        <h1>Oops! - No structures found</h1>
		<br />
        <p>You appear to have come to this page without defining a package. Please ensure &package=(component name) in the state/url.</p>
	</div>

<?php endif ?>