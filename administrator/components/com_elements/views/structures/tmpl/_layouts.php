<?php
/**
 * Created By: Oli Griffiths
 * Date: 04/10/2012
 * Time: 11:23
 */
defined('KOOWA') or die('Protected resource');

if(!isset($config)) $config = array();

$layouts = $structure->getLayouts($config);
if($layouts->count()):
	?>
	<div>
	    <?= @text(isset($config['admin']) && $config['admin'] ? 'Admin' : 'Site') ?>:
	    <ul class="unstyled inline">
			<?php
			foreach($layouts AS $layout){
				?><li>
	                <a href="<?= @route('view=layout&'.$layout->getHref()) ?>"><?= ucfirst($layout->name) ?></a>
	            </li><?php
			}?>
	    </ul>
	</div>
<?php endif ?>