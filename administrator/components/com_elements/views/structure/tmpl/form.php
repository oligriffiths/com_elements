<?= @helper('behavior.mootools') ?>
<?= @helper('behavior.tooltip') ?>
<?= @helper('behavior.keepalive') ?>

<?= @template('com://admin/default.view.grid.toolbar'); ?>
<style src="assets://css/elements.css" />
<style src="assets://css/structure.css" />
<script src="assets://js/structure.js" />
<script src="media://lib_koowa/js/koowa.js" />

<form action="<?= @route('&') ?>" method="post" class="-koowa-form bootstrap com_elements structure">

	<div class="form-content">
		<?php if($structure->hasType()): ?>
		<div class="panel title clearfix">
			<label for="type"><?= @text('Type') ?></label>
			<input type="text" name="type" value="<?= $structure->getType() ?>" id="type" class="inputbox required" placeholder="Type" />
		</div>
		<?php endif ?>

		<fieldset class="panel">
			<legend>Edit Fields</legend>
			<div class="elements pane-sliders" id="elements">
				<?
				foreach($structure->getElements(false) AS $element)
				{
					if($element->getConfig('primary')) continue;
					?>
					<div class="element panel">
                        <div class="header">
							<h3 class="jpane-toggler title" id="element_<?= $element->id ?>">
								<span>
									<label><?= $element->getlabel() ?></label>
								</span>
							</h3>

							<?php if($element->isRemovable()): ?>
                                <span class="btn btn-danger remove"><i class="icon-white icon-remove"></i></span>
							<?php endif ?>
                            <span class="btn move"><i class="icon-move"></i></span>
                        </div>

						<div class="jpane-slider content">
							<?= $element->config() ?>
						</div>
                    </div>
					<?php
				}
				?>

				<div class="element panel" id="element_dummy" style="display: none">
					<div class="header">
						<h3 class="jpane-toggler title">
							<span><label></label></span>
							<span class="loading"></span>
						</h3>
                        <span class="btn btn-danger remove"><i class="icon-white icon-remove"></i></span>
                        <span class="btn move"><i class="icon-move"></i></span>
					</div>
					<div class="jpane-slider content"></div>
				</div>
		</fieldset>
    </div>

    <div class="sidebar">
	    <div class="scrollable">
	        <fieldset class="panel element_types">
	            <legend>Add Fields</legend>
                <ul id="element_types" class="unstyled">
					<?php
					$element_types = $structure->getElementTypes();
					foreach($element_types AS $element)
					{
						?>
                        <li>
                            <button class="btn hasTip" id="element_type_<?= $element->type ?>" title="<?= $element->getName() ?>" rel="<?= $element->description ?>" data-package="<?= $structure->package ?>" data-structure="<?= $structure->id ?>">
                                <span class="icon-plus"></span>
								<?= ucfirst($element->getName()) ?>
                            </button>
                        </li>
						<?php
					}
					?>
                </ul>
            </fieldset>
        </div>
    </div>
</form>