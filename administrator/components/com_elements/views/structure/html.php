<?php
/**
 * Created By: Oli Griffiths
 * Date: 08/10/2012
 * Time: 13:42
 */
defined('KOOWA') or die('Protected resource');

class ComElementsViewStructureHtml extends ComElementsViewHtml
{
	public function display()
	{
		$this->getModel()->getItem()->loadStructureFromTable(true);

		return parent::display();
	}

}