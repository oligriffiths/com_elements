<?php
/**
 * User: Mark Head <mark@organic-development.com>
 * Date: 15/08/2012
 * Time: 16:21
 */

echo @helper('bootstrap.tab');

?>
<div class="form-body">
	<?php if( $this->checkPosition( 'title' ) ) : ?>
	<header>
		<div class="title">
			<?= $this->renderPosition( 'title', 'raw' ); ?>
		</div>
	</header>
	<?php endif; ?>

	<section class="item form-horizontal form-content">
	    <div class="tabbable tabs-left">
		    <?php if(
		        $this->checkPosition('details', false) &&
		        ($this->checkPosition('extra', false) || $this->checkPosition('meta', false))
	        ) : ?>
	        <ul class="nav nav-tabs">
				<li class="active"><a href="#details" data-toggle="tab"><?= @text('Details') ?></a></li>
				<?php if($this->checkPosition('extra', false)): ?>
		            <li><a href="#extra" data-toggle="tab"><?= @text('Extra') ?></a></li>
			    <?php endif ?>
		        <?php if($this->checkPosition('meta', false)): ?>
	            <li><a href="#meta" data-toggle="tab"><?= @text('Meta Info') ?></a></li>
		        <?php endif ?>
	        </ul>
		    <?php endif ?>

	        <div class="detail tab-content">
				<?php if( $this->checkPosition( 'details' ) ) : ?>
			        <div id="details" class="tab-pane active">
		                <fieldset>
		                    <legend><?= @text('Details') ?></legend>
							<?= $this->renderPosition( 'details', 'form'); ?>
			            </fieldset>
			        </div>
				<?php else: ?>
					<div class="alert alert-danger"><?= @text('There are no elements assigned to the details position') ?></div>
				<?php endif ?>
			    <?php if( $this->checkPosition( 'extra' ) ) : ?>
			        <div id="extra" class="tab-pane">
			            <fieldset>
			                <legend><?= @text('Extra Details') ?></legend>
						    <?= $this->renderPosition( 'extra', 'form'); ?>
			            </fieldset>
			        </div>
			    <?php endif; ?>
		        <?php if( $this->checkPosition( 'meta' ) ) : ?>
			        <div id="meta" class="tab-pane">
			            <fieldset>
			                <legend><?= @text('Meta Information') ?></legend>
					        <?= $this->renderPosition( 'meta', 'form'); ?>
			            </fieldset>
			        </div>
		        <?php endif; ?>
	        </div>
	    </div>
	</section>
</div>

<?php if( $this->checkPosition( 'parameters' ) || $this->checkPosition( 'sidebar' ) ) : ?>
<div class="sidebar">
	<div class="scrollable">
	<?php if($this->checkPosition( 'parameters' )) : ?>
	<fieldset class="form-horizontal">
		<legend><?= @text('Parameters') ?></legend>
		<?= $this->renderPosition( 'parameters','form' ); ?>
	</fieldset>
	<?php endif ?>

	<?php if($this->checkPosition( 'parameters' )) : ?>
		<?= $this->renderPosition( 'sidebar','fieldset' ); ?>
	<?php endif ?>
    </div>
</div>
<?php endif; ?>
