<?php
/**
 * User: Oli Griffiths
 * Date: 17/07/2012
 * Time: 18:16
 */


//Get the layout type
$structure = $layout->getStructure();

?>
<?= @template('com://admin/default.view.grid.toolbar'); ?>

<?= @helper('behavior.mootools') ?>
<?= @helper('behavior.tooltip') ?>
<?= @helper('behavior.keepalive') ?>
<? @helper('com://site/bootstrap.template.helper.bootstrap.jquery'); ?>
<? @helper('com://site/bootstrap.template.helper.bootstrap.style',array('button')); ?>

<style src="assets://css/elements.css" />
<style src="assets://css/layout.css" />
<script src="assets://js/layout.js" />
<script src="media://lib_koowa/js/koowa.js" />

<form action="" method="post" class="-koowa-form bootstrap com_elements layout_assign">

    <div class="form-content">
		<div id="positions">
			<?php
			foreach($layout->getPositions() AS $name => $position) :
				?>
				<fieldset class="panel position" data-position="<?= $name ?>">
					<legend><?= $position->get('title', ucfirst($name)) ?></legend>
					<div class="elements pane-sliders">
						<?php
						$i = 0;
						foreach($position->getElements() AS $id => $element):
						?>
							<div class="element panel element_<?= $element->id ?>">
								<div class="header">
									<h3 class="jpane-toggler title">
										<span>
											<?= $element->getlabel() ?>
										</span>
									</h3>
                                    <div class="btn btn-danger remove">
                                        <i class="icon-remove icon-white"></i>
                                    </div>
                                    <div class="btn move">
	                                    <i class="icon-move"></i>
                                    </div>
								</div>
								<div class="jpane-slider content">
									<div class="config">
										<?= $element->renderConfig(array($name, $i)) ?>
									</div>
								</div>
							</div>
						<?php
						$i++;
						endforeach;
						?>
					</div>
				</fieldset>
				<?php
			endforeach;
			?>
		</div>
	</div>

	<div class="sidebar">
		<div class="scrollable" id="elements">
            <fieldset class="panel">
                <legend><?= @text('Elements') ?></legend>
                <div id="elements_wrapper" class="inner elements pane-sliders">
					<?php
					foreach($structure->getElements() AS $id => $element)
					{
						?>
                        <div class="element panel element_<?= $element->id ?>">
                            <div class="header">
                                <h3 class="jpane-toggler title">
										<span>
											<?= $element->getlabel() ?>
										</span>
                                </h3>
                                <div class="btn btn-danger remove">
                                    <i class="icon-remove icon-white"></i>
                                </div>
                                <div class="btn move">
                                    <i class="icon-move"></i>
                                </div>
                            </div>
                            <div class="jpane-slider config">
                                <div class="config">
									<?= $element->renderConfig(array('_position',0)) ?>
                                </div>
                            </div>
                        </div>
						<?php
					}
					?>
                </div>
            </fieldset>
		</div>
	</div>
</form>