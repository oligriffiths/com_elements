<?php
/**
 * User: Oli Griffiths
 * Date: 28/06/2012
 * Time: 14:36
 */

?>
<link href="media://bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
<link href="media://com_elements/css/layout.css" type="text/css" rel="stylesheet" />
<script src="media://com_elements/js/layout.js" type="text/javascript" />

<script>
	window.addEvent('domready', function(){
		new ElementsLayout('layout_wrap')
	})
</script>
<form action="" method="post" class="-koowa-form bootstrap com_elements layout">

	<div class="grid_12">

		<div id="layout_wrap">
				<button class="btn lock">
					<span class="icon-lock"></span>
				</button>


				<button class="btn btn-primary add_block">
					<span class="icon-plus icon-white"></span>
					Add Block
				</button>

			<div class="layout">

			</div>
		</div>
	</div>
</form>
