<?php
/**
 * User: Oli Griffiths
 * Date: 23/06/2012
 * Time: 15:45
 */

class ComElementsControllerElement extends ComDefaultControllerResource
{

    protected function _actionPost(KCommandContext $context)
    {
        //KRequest::set('request.format','json');
        //$this->getRequest()->format = 'json';

        $element = $this->getModel()->getItem();
        $callback = $context->data->callback ? $context->data->callback : $this->getRequest()->callback;

        if(!$callback)
        {
            throw new KControllerException('No callback parameter supplied');
        }

        if ($callback && method_exists($element, $callback))
        {
            //Remove the after dispatch callback as this clears the context result
            $dispatcher = KService::get( 'com://site/elements.dispatcher' );
            $dispatcher->unregisterCallback('after.dispatch', array($this, 'render'));

            $context->result = json_encode($element->$callback($context));
            return $context->result;
        } else {
            throw new KControllerException('Element '.$element->getType().' does not implement the method '.$callback);
        }

    }

	protected function _actionCallback(KCommandContext $context)
	{
		KRequest::set('request.format','json');
		$this->getRequest()->format = 'json';

		$element = $this->getModel()->getItem();
		$callback = $context->data->callback ? $context->data->callback : $this->getRequest()->callback;

		if(!$callback)
		{
			throw new KControllerException('No callback parameter supplied');
		}

		if($callback && method_exists($element, $callback))
		{
			//Remove the after dispatch callback as this clears the context result
			$dispatcher = KService::get( 'com://site/elements.dispatcher' );
			$dispatcher->unregisterCallback('after.dispatch', array($this, 'render'));

			$context->result = json_encode($element->$callback($context));
			return $context->result;
		}else{
			throw new KControllerException('Element '.$element->getType().' does not implement the method '.$callback);
		}
	}
}