<?php
/**
 * Com ......
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_***
 * @package     ******
 * @uses        Com_
 */

class ComElementsControllerLayout extends ComDefaultControllerDefault
{
    protected $_item;

    public function __construct(KConfig $config)
    {
        parent::__construct($config);

        $this->_item = $config->item;
    }

    public function getView()
    {
        if(!$this->_view instanceof KViewAbstract)
        {
            //Make sure we have a view identifier
            if(!($this->_view instanceof KServiceIdentifier)) {
                $this->setView($this->_view);
            }

            //Create the view
            $config = array(
                'model'     => $this->getModel(),
                'media_url' => KRequest::root().'/media',
                'base_url'	=> KRequest::url()->get(KHttpUrl::BASE),
                'data'      => array('item' => $this->_item)
            );

            if($this->isExecutable()) {
                $config['auto_assign'] = !$this->getBehavior('executable')->isReadOnly();
            }

            $this->_view = $this->getService($this->_view, $config);

            //Set the layout
            if(isset($this->_request->layout)) {
                $this->_view->setLayout($this->_request->layout);
            }

            //Make sure the view exists
            if(!file_exists(dirname($this->_view->getIdentifier()->filepath))) {
                throw new KControllerException('View : '.$this->_view->getName().' not found', KHttpResponse::NOT_FOUND);
            }
        }

        return $this->_view;
    }
}