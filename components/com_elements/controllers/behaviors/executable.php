<?php
/**
 * Created By: Oli Griffiths
 * Date: 09/10/2012
 * Time: 11:26
 */
defined('KOOWA') or die('Protected resource');

class ComElementsControllerBehaviorExecutable extends ComDefaultControllerBehaviorExecutable
{
	public function execute($name, KCommandContext $context)
	{
		$parts = explode('.', $name);

		if($parts[0] == 'before' && !in_array($parts[1], array('add','edit','delete')))
		{
			return true;
		}

		return parent::execute($name, $context);
	}

}