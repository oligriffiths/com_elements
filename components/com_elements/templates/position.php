<?php
/**
 * Com ......
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_***
 * @package     ******
 * @uses        Com_
 */

class ComElementsTemplatePosition extends ComDefaultTemplateAbstract
{
	protected $_name;
	protected $_layout;
	protected $_elements_raw = array();
	protected $_elements;
    protected $_has_elements;

	public function __construct(KConfig $config)
	{
		parent::__construct($config);
		$config->append(array(
				'name' => null,
				'elements' => array(),
				'layout' => null,
				'item' => null
		));

		$this->_name = $config->name;
		$this->_elements_raw = $config->elements->toArray();
		$this->setLayout($config->layout);
	}


	public function setLayout($layout)
	{
		$this->_layout = $layout;
		return $this;
	}


	public function hasElements()
	{
        if(!isset($this->_has_elements)){
            $elements = $this->getElements();
            if($elements instanceof ComElementsViewElementsSet)
            {
                $this->_has_elements = $elements->count() > 0;
            }else{
                $this->_has_elements = count($this->_elements_raw) > 0;
            }
        }

        return $this->_has_elements;
	}


	public function check($checkValue = true)
	{
		if(!$this->hasElements()) return false;

		if(!$checkValue) return true;

		foreach($this->getElements() AS $element)
		{
			if($element->hasValue()) return true;
		}

		return false;
	}


	public function getElements()
	{
		if(!$this->_elements)
		{
			$mode = $this->_layout->submission ? 'edit' : 'render';

			//Get all the element ids
			$elements = array();
			foreach($this->_elements_raw AS $element){
				if(!isset($element['id'])){
					continue;
				}

				if($mode == 'edit') $elements[$element['id']] = $element;
				else $elements[] = $element;
			}

            //If no elements found, simulate a random name so we can get back just the set from the structure
            if(empty($elements)) $elements = array(rand());


			//Get the element set from the structure
			$this->_elements = $this->_layout->getStructure()->getElements(array('elements' => $elements, 'mode' => $mode));
		}

		return $this->_elements;
	}


	public function render($chrome = 'block', $submission = false)
	{
		//If no elements, return false
		if(!$this->hasElements()) return false;

		//Loop the elements and render them
		$elements = array();
		foreach($this->getElements() AS $element)
		{
			$el = new stdClass;
			$el->content = '';
			try{
				//If in admin mode or submission view, run edit
				if($this->_layout->submission || $submission)
				{
					$el->content = $element->edit();
				}else{
					if($element->hasValue()){
						$el->content = $element->render();
					}else{
						continue;
					}
				}
			}catch(KException $e)
			{
				$el->content = $e->getMessage();
			}

			$el->element    = $element;
			$el->id         = $element->getControlId(null, false, false);
			$el->label      = ($label = $element->getConfig('altlabel', null, 'render')) ? $label : $element->getTitle();
			$el->showlabel  = $element->getConfig('showlabel',true,'render');
			$el->description= $element->getConfig('description');
			$el->tooltip    = $element->getConfig('tooltip');
			$el->type       = $element->getType();
			$el->class      = $element->getConfig('class','','render');
			$el->class      .= $element->isRepeatable() ? ' repeatable' : '';
			$elements[] = $el;
		}

		//Render the element style
		$return = array();
		$max = count($elements)-1;
		foreach($elements AS $i => $element)
		{
			$element->first = $i == 0;
			$element->last = $i == $max;
			if($chrome)
			{
				$options = array();

				//Allow chrome to be an array with options
				if(is_array($chrome))
				{
					$options = $chrome;
					$chrome = isset($chrome['chrome']) ? $chrome['chrome'] : 'block';
				}

				//Try to load the chrome from the calling package
                $identifier = clone $this->_layout->getItem()->getIdentifier();
                $identifier->path = array('template','chromes');
                $identifier->name = $chrome;

                //If file is not found, revert to com_elements
                if(!file_exists($identifier->filepath))
                {
                    $identifier->application = 'site';
                    $identifier->package = 'elements';
                }

				$element = array_merge($options, (array) $element);

                $return[] = (string) $this->getChrome()->loadIdentifier($identifier, $element);
			}else{
				$return[] = (string) $element->content;
			}
		}

		return implode('', $return);
	}


	public function getChrome()
	{
		static $chrome;
		if(!$chrome){
            $chrome = $this->getService('com://site/elements.template.chrome');
            $chrome->addFilter(array('shorttag', 'alias'));
        }

		return $chrome;
	}
}