<?php
/**
 * Com ......
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_***
 * @package     ******
 * @uses        Com_
 */
class ComElementsTemplateHelperListbox extends KTemplateHelperListbox
{
	/**
	 * Generic listbox function
	 *
	 * @param 	array 	An optional array with configuration options
	 * @return	string	Html
	 */
	public function listbox($config = array())
	{
		$config = new KConfig($config);
		return parent::_listbox($config);
	}
}