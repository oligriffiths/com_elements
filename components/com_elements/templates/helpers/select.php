<?php
/**
 * User: Oli Griffiths
 * Date: 15/06/2012
 * Time: 19:45
 */

class ComElementsTemplateHelperSelect extends KTemplateHelperSelect
{
	public function elementTypes($config = array()){
		static $options;

		$config = new KConfig($config);

		if(!isset($options)){
			$types = $config->type->getElementTypes();
			$options = array($this->option(array('value' => '-skip-', 'text' => Jtext::_('- Skip -'))));
			foreach($types AS $type) $options[] = $this->option(array('value' => $type->type, 'text' => $type->name));
		}
		$config->options = $options;
		$config->append(array('attribs' => array('class' => 'element_types')));

		return $this->optionlist($config);
	}


	public function booleanlist($config = array())
	{
		$config = new KConfig($config);
		$config->append(array(
			'name'   	=> '',
			'attribs'	=> array(),
			'true'		=> 'yes',
			'false'		=> 'no',
			'selected'	=> null,
			'translate'	=> true
		));

		$name    = $config->name;
		$attribs = KHelperArray::toString($config->attribs);

		$html  = array();

		$extra = !$config->selected ? 'checked="checked"' : '';
		$text  = $config->translate ? JText::_( $config->false ) : $config->false;

		$html[] = '<label for="'.$name.'0" class="radio booleanlist inline">';
		$html[] = '<input type="radio" name="'.$name.'" id="'.$name.'0" value="0" '.$extra.' '.$attribs.' />'.$text;
		$html[] = '</label>';

		$extra = $config->selected ? 'checked="checked"' : '';
		$text  = $config->translate ? JText::_( $config->true ) : $config->true;

		$html[] = '<label for="'.$name.'1" class="radio booleanlist inline">';
		$html[] = '<input type="radio" name="'.$name.'" id="'.$name.'1" value="1" '.$extra.' '.$attribs.' />'.$text;
		$html[] = '</label>';

		return implode(PHP_EOL, $html);
	}
}