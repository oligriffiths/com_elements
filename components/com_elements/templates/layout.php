<?php
/**
 * User: Oli Griffiths
 * Date: 29/07/2012
 * Time: 12:37
 */

class ComElementsTemplateLayout extends ComDefaultTemplateAbstract
{
	protected $_layout;
	protected $_item;
	protected $_admin = false;
    protected $_submission = false;
	protected $_cache_enabled = false;
	protected $_cache_expiry = false;

	public function __construct(KConfig $config)
	{
		parent::__construct($config);

		$config->append(array(
			'cache' => false, //Cache on/off
			'cache_expiry' => false, //Cache on/off
			'layout' => NULL //The layout itself
		));
		$this->_layout          = $config->layout;
		$this->_cache_enabled   = $config->cache;
		$this->_cache_expiry    = $config->cache_expiry;

		//Validate the layout
		if(!$this->_layout || !$this->_layout instanceof ComElementsDatabaseRowLayout)
		{
			throw new KTemplateException(ComElementsTemplateLayout.'::'.__FUNCTION__.' requires layout parameter with instance of ComElementsDatabaseRowLayout');
		}

		//Validate the item
		if(!$this->getItem() || !$this->getItem()->isElementable())
		{
			throw new KTemplateException(ComElementsTemplateLayout.'::'.__FUNCTION__.' requires layout with an Item implements the Elementable behavior');
		}

		//Add filters
		$this->addFilter(array('shorttag', 'alias', 'template','module'));
	}

	protected function _initialize(KConfig $config)
	{
		$config->view = 'default';
		parent::_initialize($config);
	}


	/**
	 * Gets the item from the layout
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->_layout->getItem();
	}


	/**
	 * Searches for the file
	 *
	 * @param	string	The file path to look for.
	 * @return	mixed	The full path and file name for the target file, or FALSE
	 * 					if the file is not found
	 */
	public function findFile($file)
	{
		$result = false;
		$path   = dirname($file);

		// is the path based on a stream?
		if (strpos($path, '://') === false)
		{
			// not a stream, so do a realpath() to avoid directory
			// traversal attempts on the local file system.
			$path = realpath($path); // needed for substr() later
			$file = realpath($file);
		}

		// The substr() check added to make sure that the realpath()
		// results in a directory registered so that non-registered directores
		// are not accessible via directory traversal attempts.
		if (file_exists($file) && substr($file, 0, strlen($path)) == $path) {
			$result = $file;
		}

		// could not find the file in the set of paths
		return $result;
	}


	/**
	 * Loads and parses file path and caches the response if cache enabled
	 * @param $path
	 * @param array $data
	 * @param bool $process
	 * @return KTemplateAbstract|mixed
	 */
	public function loadFile($path, $data = array(), $process = true)
	{
		if(!$this->_cache_enabled) return parent::loadFile($path, $data, $process);

		//Construct cache id
		$item_identifier = $this->getItem()->getIdentifier();
		$cache_identifier = (string) $item_identifier.'.'.$this->getItem()->id.'.'.$path;
		$cache_expires = $this->_cache_expiry;

		if(isset($this->_cache) && $cache_identifier)
		{
			$identifier = md5($cache_identifier);
			if ($template = $this->_cache->get($identifier, 'com_elements'))
			{
				$this->_contents = $template;
				return $template;
			}
		}

		$template = parent::loadFile($path, $data, $process);

		if(isset($this->_cache) && $cache_identifier)
		{
			$identifier = md5($cache_identifier);

			//Store the object in the cache
			if($cache_expires){
				$cache = clone $this->_cache;
				$cache->_handler = NULL;
				$cache->setLifeTime($cache_expires);
			}else{
				$cache = $this->_cache;
			}

			$cache->store((string) $template, $identifier, 'com_elements');
		}

		return $template;
	}


	/**
	 * Checks a position has elements with values
	 * @param $name
	 * @return bool
	 */
	public function checkPosition($name, $checkValue = null)
	{
		$checkValue = $checkValue !== null ? $checkValue : !$this->_layout->submission;
		$position = $this->_layout->getPosition($name);
		if(!$position) return false;
		return $position->check($checkValue);
	}


	/**
	 * Renders the elements in a position
	 * @param $name
	 * @param string $chrome - Sets the element chrome. This are located in com_elements/templates/chromes
	 * @return string
	 */
	public function renderPosition($name, $chrome = 'block', $submission = false)
	{
		//Get the position
		$position = $this->_layout->getPosition($name);

		//Ensure position exists
		if(!$position) return '<div>The position "'.$name.'" does not exist</div>';

		//Loop the elements and render them
		return $position->render($chrome, $submission);
	}
}