<?php
/**
 * User: Lee Parsons <lee@organic-development.com>
 * Date: 08/10/2012
 * Time: 09:26
 */

class ComElementsTemplateChrome extends KTemplateAbstract
{
    /**
     * Searches for the file
     *
     * This function first tries to find a template override, if no override exists
     * it will try to find the default template
     *
     * @param	string	The file path to look for.
     * @return	mixed	The full path and file name for the target file, or FALSE
     * 					if the file is not found
     */
    public function findFile($path)
    {
        $template  = JFactory::getApplication()->getTemplate();
        $override  = JPATH_THEMES.'/'.$template.'/html';
        $override .= str_replace(array(JPATH_BASE.'/modules', JPATH_BASE.'/components', '/views'), '', $path);

        //Try to load the template override
        $result = parent::findFile($override);

        if($result === false)
        {
            $result = parent::findFile($path);
        }

        return $result;
    }
}