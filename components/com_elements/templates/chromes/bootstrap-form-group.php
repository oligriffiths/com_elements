<?php
/**
 * User: Oli Griffiths
 * Date: 23/07/2012
 * Time: 14:55
 */

?>
<div class="control-group element element_<?= $type ?> <?= $id ?> <?= $class ?>">
    <?php if($showlabel): ?>
    <legend class="element-title"><?= $label ?></legend>
    <?php endif	?>
    <div class="content">
        <?= $content ?>
    </div>
</div>