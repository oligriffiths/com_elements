<?php
/**
 * User: Oli Griffiths
 * Date: 23/07/2012
 * Time: 14:55
 */

?>
<div class="element element_<?= $type ?> <?= $id ?> <?= $class ?>">
	<?php if($showlabel): ?>
		<h3 class="element-title"><?= $label ?></h3>
	<?php endif	?>
	<div class="content">
		<?= $content ?>
	</div>
</div>