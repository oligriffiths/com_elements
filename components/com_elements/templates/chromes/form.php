<?php
/**
 * Created By: Oli Griffiths
 * Date: 17/08/2012
 * Time: 13:54
 */
defined('KOOWA') or die('Protected resource');

?>
<div class="control-group element element_<?= $type ?> <?= $id ?> <?= $class ?>">
	<?php
	if($showlabel)
	{
		$for = $element->isMultifield() ? '' : 'for="'.$element->getControlId().'"';
		?>
        <label class="control-label" <?= $for ?>>
	        <span <?= $tooltip ? 'rel="tooltip" title="'.$tooltip.'"' : '' ?>><?= $label ?></span>
        </label>
		<?php
	}
	?>
    <div class="controls">
	    <div id="<?= $id ? $id : '' ?>">
			<?= $content ?>
        </div>

		<?php if($description): ?>
	    <div class="help-block">
			<?= $description ?>
		</div>
		<?php endif ?>
    </div>
</div>