<?php
/**
 * Created By: Oli Griffiths
 * Date: 16/10/2012
 * Time: 10:32
 */
defined('KOOWA') or die('Protected resource');
?>
<fieldset class="element element_<?= $type ?> <?= $id ?> <?= $class ?>">
	<?php if($showlabel): ?>
    <legend class="element-title"><?= $label ?></legend>
	<?php endif	?>
    <div class="content">
		<?= $content ?>
    </div>
</fieldset>