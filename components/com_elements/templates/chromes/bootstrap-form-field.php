<?php
/**
 * User: Oli Griffiths
 * Date: 23/07/2012
 * Time: 14:55
 */

?>
<div class="control-group element element_<?= $type ?> <?= $id ?> <?= $class ?>">
    <?php if($showlabel): ?>
    <label class="control-label" for="<?= $id ?>"><?= $label ?></label>
    <?php endif	?>
    <div class="controls">
        <?= $content ?>
    </div>
</div>