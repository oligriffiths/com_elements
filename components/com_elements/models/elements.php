<?php
/**
 * Created By: Oli Griffiths
 * Date: 30/05/2012
 * Time: 14:47
 */
defined('KOOWA') or die('Protected resource');

class ComElementsModelElements extends ComDefaultModelDefault
{
	public function __construct(KConfig $config)
	{		
		parent::__construct($config);
		$this->getState()
			->insert('id', 'string')
			->insert('type', 'string')
			->insert('structure', 'string')
			->insert('package', 'string');
	}

	public function getItem()
	{
		if(!$this->_item){

			//If structure is set, attempt to get element from structure
			if($this->package && $this->structure && $this->id)
			{
				$state = $this->getState()->getData();
				$state['id'] = $this->structure;
				unset($state['type']);
				$structure = $this->getService('com://admin/elements.model.structures')->set($state)->getItem();
				if($structure)
				{
					$this->_item = $structure->getElement($this->id);
					return $this->_item;
				}
			}

			$options = array(
				'package' => $this->package
			);

			$type = $this->get('type','default');

			$identifier             = clone $this->getIdentifier();
			$identifier->package    = $this->package;
			$identifier->path       = array('view','elements', $type);
			$identifier->name       = 'element';

			//Check if element has an override / exists within target package
			if(is_dir(dirname($identifier->filepath)))
			{
				//Check if the element has an element class file
				if(!file_exists($identifier->filepath)) $identifier->package = 'elements';

			}else{
				$identifier->package = 'elements';
				$options['package'] = 'elements';
			}

			//Get an instance of the element
			$this->_item = $this->getService($identifier, $options);
		}

		return $this->_item;
	}

	public function getList()
	{
		static $types;

		if(!$types){

			$files = array();
			if($this->package) $files = array_merge($files, glob(JPATH_ROOT.'/components/com_'.$this->package.'/views/elements/*/config.xml'));
			$files = array_merge($files, glob(dirname(dirname(__FILE__)).'/views/elements/*/config.xml'));

			sort($files);

			$types = $this->getService('com://site/elements.view.elements.set', array('package' => $this->package));
			if($files)
			{
				foreach($files AS $file)
				{
					$path = explode('/',str_replace(JPATH_ROOT.'/components/','', $file));
					array_pop($path);
					$name = array_pop($path);

					$types->setElement($name);
				}
			}
		}

		return $types;
	}
}