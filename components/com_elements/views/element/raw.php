<?php
/**
 * User: Oli Griffiths
 * Date: 19/06/2012
 * Time: 21:40
 */

class ComElementsViewElementRaw extends ComDefaultViewHtml
{
	protected function _initialize(KConfig $config)
	{
		$config->auto_assign = true;
		parent::_initialize($config);
	}
}