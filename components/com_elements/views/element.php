<?php
/**
 * User: Oli Griffiths
 * Date: 22/06/2012
 * Time: 20:08
 */

class ComElementsViewElement extends ComDefaultViewHtml implements KObjectHandlable
{
	/**
	 * @var
	 */
	protected $_id;

	/**
	 * @var mixed
	 */
	protected $_type;

	/**
	 * This holds the package the
	 * @var
	 */
	protected $_package;

	/**
	 * @var
	 */
	protected $_config;

	/**
	 * @var mixed
	 */
	protected $_group;

	/**
	 * @var mixed
	 */
	protected $_input_group;

	/**
	 * @var bool|mixed
	 */
	protected $_fixed;

	/**
	 * @var bool
	 */
	protected $_multifield;

	/**
	 * @var bool
	 */
	protected $_repeatable;

	/*
	 *
	 */
	protected $_index;

	/**
	 * @var array
	 */
	protected $_indexable = array();

	/**
	 * @var bool
	 */
	protected $_set;

	/**
	 * @var
	 */
	protected $_structure;

	/**
	 * @var
	 */
	protected $_item;

	/**
	 * Caches the output based on config params
	 * @var array
	 */
	protected $_outputs = array();


	/**
     * Initializes the config for the object
     *
     * Called from {@link __construct()} as a first step of object instantiation.
     *
     * @param 	object 	An optional KConfig object with configuration options
     * @return  void
     */
	protected function _initialize(KConfig $config)
	{
		parent::_initialize($config);
		$config->append(array(
			'id'        => 'id_'.rand(),
			'value'     => null,
			'fixed'     => false,
			'multifield'=> false,
			'indexable' => array(),
			'config'    => array('edit' => array(), 'render' => array()),
			'set'       => null,
			'item'      => $this->getService('koowa:object.array')
		));

		$this->reset();

		if(!$config->item instanceof KObjectArray){
			throw new KException('The item passed to '.__CLASS__.'::'.__FUNCTION__.' must be an instance of KObjectArray');
		}

		//Remove some template filters, not used
		$config->template_filters = array_diff($config->template_filters->toArray(), array('script','link','style','form','module','variable','template'));
		$config->template = 'element';

		$config->auto_assign= false;
		$this->_id          = $config->id;
		$this->_package     = $config->package;
		$this->_config      = $config->config;
		$this->_multifield  = $config->multifield;
		$this->_set         = $config->set;
		$this->_indexable   = KConfig::unbox($config->indexable);
		$this->_structure   = $config->structure;
		$this->_type        = $this->getType();
		$this->_group       = $config->group;
		$this->_input_group = $this->getConfig('group') ?: null;
		$this->_fixed       = (bool) $config->fixed;
		$this->_item        = $config->item;

		//Initialize the value property
		if($this->isRepeatable())
		{
			$value = $this->_item->{$this->_id};

			if($this->isMultifield()){
				if(!is_array($value)){
					$value = array(0 => array());
					$this->_item->{$this->_id} = $value;
				}
			}
			else if(!is_array($value)){
				$value = array(0 => $value);
				$this->_item->{$this->_id} = $value;
			}

			$this->_index = key($value);

		}else{
			$value = $this->_item->{$this->_id};
			if($this->isMultifield() && !is_array($value)) $this->_item->{$this->_id} = array();
		}

		if(is_string($this->_structure))
		{
			$this->_structure = $this->getService('com://admin/elements.model.structures')->id($this->_structure)->getItem();
		}

		if(!$this->_structure && $this->_item instanceof KDatabaseRowAbstract && $this->_item->isElementable())
		{
			$this->_structure = $this->_item->getStructure();
		}

		if($config->value) $this->setValues(KConfig::unbox($config->value));

		return $this;
	}


	/**
	 * Sets up the element by calling initialize again
	 * @param $config
	 * @return ComElementsViewElement
	 */
	public function setup($config)
	{
		$config = new KConfig($config);
		$this->_initialize($config);

		return $this;
	}


	/**
	 * Resets the elements data
	 * @return ComElementsViewElement
	 */
	public function reset()
	{
		$this->_data = null;
		$this->_item = null;
		$this->_output = null;
		$this->_multifield = null;
		$this->_repeatable = null;
		$this->_fixed = null;
		$this->_indexable = array();
		return $this;
	}


	/**
	 * @param string $key
	 * @return mixed|string
	 */
	public function __get($key)
	{
		switch($key)
		{
			case 'id': return $this->getId();
			break;

			case 'type': return $this->getType();
			break;

			case 'config': return $this->getConfig();
			break;

			case 'multifield': return $this->isMultifield();
			break;

			case 'repeatable': return $this->isRepeatable();
			break;

			case 'required': return $this->getConfig('required');
			break;

			case 'values': return $this->getValues();
			break;
		}

		$value = $this->getValue($key);
		if($value !== null) return $value;

		return parent::__get($key);
	}


	/**
	 * Unsets a value, fallsback to unsetting a property in $this
	 * @param $name
	 */
	function __unset($name)
	{
		if($this->getValue($name)) $this->unsetValue($name);
		else unset($this->$name);
	}


	/**
	 * @return string
	 */
	public function getHandle()
	{
		return $this->_id ? $this->_id : $this->type;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->getHandle();
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->getName();
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		$title = $this->getConfig('title');
		return $title ? $title : KInflector::humanize($this->getHandle());
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		$type = $this->getType();
		$type = $type ? $type : '- skip -';
		return '<label>'.$this->getTitle().' <sub>('.$type.')</sub></label>';
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->getConfig('description');
	}

	public function getPackage()
	{
		if($this->_item instanceof KDatabaseRowsetAbstract) return $this->_item->getIdentifier()->package;
		if($structure = $this->getStructure()) return $structure->package;
		return $this->_package;
	}

	/**
	 * @return mixed
	 */
	public function isAdmin()
	{
		global $admin;
		if(!isset($admin)) $admin = JFactory::getApplication()->isAdmin();

		return $admin;
	}

	/**
	 * @return bool
	 */
	public function isRepeatable()
	{
		if(is_null($this->_repeatable)) $this->_repeatable = !$this->isFixed() && $this->getConfig('repeatable', false);
		return (bool) $this->_repeatable;
	}

	/**
	 * @return bool
	 */
	public function isRemovable()
	{
		return (bool) !$this->isFixed();
	}

	/**
	 * @return bool
	 */
	public function isFixed(){
		return (bool) $this->_fixed;
	}

	/**
	 * @return mixed
	 */
	public function isMultifield()
	{
		return (bool) $this->_multifield;
	}

	/**
	 * @return bool
	 */
	public function isRequired()
	{
		return (bool) $this->getConfig('required');
	}


	/**
	 * @param string $value
	 * @param null $index
	 * @return bool
	 */
	public function hasValue($key = null, $index = null, $notBlank = false)
	{
		if($this->isRepeatable()){
			if($index === null){
				$values = $this->getValues();
				foreach($values AS $index => $value){
					$this->_index = $index;
					if($this->_hasValue($key, $index, $notBlank)) return true;
				}
			}else{
				return $this->_hasValue($key, $index, $notBlank);
			}
		}else{
			return $this->_hasValue($key, null, $notBlank);
		}

		return false;
	}


	protected function _hasValue($key, $index = null, $notBlank = false)
	{
		$value = $this->getValue($key, $index);
		return $notBlank ? ($value ? true : false) : $value || $value === '0' || $value === 0;
	}


	/**
	 * @param string $name
	 * @param null $index
	 * @return null
	 */
	public function getValue($key = null, $index = null)
	{
		$value = $this->getValues();

		if($this->isRepeatable())
		{
			$index = $index !== null ? $index : $this->getIndex();
			$value = isset($value[$index]) ? $value[$index] : null;
			if($value === null && $this->isMultifield()) $value = array();
		}

		if($value === null) return null;

		return $this->isMultifield() && $key ? (isset($value[$key]) ? $value[$key] : null) : $value;
	}

	/**
	 * @return mixed
	 */
	public function getValues()
	{
		return $this->_item->{$this->_id};
	}

	/**
	 * @param $value
	 * @param string $key
	 * @param null $index
	 * @return ComElementsViewElement
	 */
	public function setValue($value, $key = null, $index = null)
	{
		$values = $this->getValues();

		if($this->isRepeatable())
		{
			//Get the index
			$index = $index !== null ? $index : $this->getIndex();

			//Ensure the index is set
			if(!isset($values[$index])) $values[$index] = array();

			//Set the value
			if($this->isMultifield())
			{
				$values[$index][$key] = $value;
			}else{
				$values[$index] = is_scalar($value) ? $value : null;
			}
		}else{
			if($this->isMultifield() && $key !== null)
			{
				$values[$key] = $value;
			}else
			{
				$values = $value;
			}
		}

		$this->_item->{$this->_id} = $values;

		return $this;
	}


	/**
	 * @param $values
	 * @param null $index
	 * @return ComElementsViewElement
	 */
	public function setValues($values, $index = null)
	{
		$repeatable = $this->isRepeatable();
		$multifield = $this->isMultifield();

		if($repeatable){

			if($multifield){
				if(!is_array($values)) return $this;
				if(!is_numeric(key($values))) $values = array($index => $values);
			}else{
				if(!is_array($values)) $values = array($index => $values);
			}

			//Re-index array
			$values = array_values($values);
		}else{
			if($multifield && !is_array($values)) return $this;
			$values = array($values);
		}

		foreach($values AS $index => $value)
		{
			if($multifield)
			{
				foreach($value AS $key => $v)
				{
					$this->setValue($v, $key, $index);
				}
			}else{
				$this->setValue($value, null, $index);
			}
		}

		return $this;
	}


	/**
	 * Unsets a specific value
	 * @param $key
	 * @param null $index
	 * @return ComElementsViewElement
	 */
	public function unsetValue($key, $index = null)
	{
		if($index === null) $index = $this->getIndex();

		if($index !== null){
			if(isset($this->_item->{$this->_id}[$index])){
				$values = $this->_item->{$this->_id};
				unset($values[$index][$key]);
				$this->_item->{$this->_id} = $values;
			}
		}else{
            $value = $this->_item->{$this->_id};
            unset($value[$key]);
            $this->_item->{$this->_id} = $value;
		}

		return $this;
	}


	/**
	 * Unsets values stored in the value object.
	 * @param null|true $index - or true to delete all indexes
	 * @return ComElementsViewElement
	 */
	public function unsetValues($index = null)
	{
		if($index === null) $index = $this->getIndex();

		$values = $this->getValues();

		if($this->isRepeatable()){
			if($index === true) $values = array();
			else unset($values[$index]);
		}else{
			if($this->isMultifield()) $values = array();
			else $values = null;
		}

		$this->_item->{$this->_id} = $values;

		return $this;
	}


	/**
	 * @param null $property
	 * @param null $default
	 * @param string $group
	 * @return mixed
	 */
	public function getConfig($property = null, $default = null, $group = 'edit')
	{
        $config = $this->_config->get($group);
        if(!$config) return $default;

		return $property ? $config->get($property, $default) : $config;
	}


	/**
	 * @param $config
	 * @param $group
	 * @return ComElementsViewElement
	 */
	public function setConfig($key, $value, $group = 'edit')
	{
        if(is_array($key))
        {
            $group = $value;
            if(!$this->_config->$group) $this->_config->$group = array();
            foreach($key AS $k => $value) $this->_config->$group->$k = $value;
        }else{
            if(!$this->_config->$group) $this->_config->$group = array();
            $this->_config->$group->$key = $value;
        }
		return $this;
	}


	/**
	 * @return null
	 */
	public function getIndex()
	{
		if($this->isRepeatable()) return $this->_index;
		else return null;
	}

	/**
	 * @return mixed
	 */
	public function getElementTypes()
	{
		return $this->getService('com://site/elements.model.elements')->package($this->getpackage())->getList();
	}

	/**
	 * @return array
	 */
	public function getGroups()
	{
		return $this->_structure ? $this->_structure->getGroups() : array();
	}


	/**
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->_item;
	}

	public function getStructure()
	{
		$item = $this->getItem();
		$item = $item instanceof KDatabaseRowAbstract ? $item : null;
		return $this->_structure ? $this->_structure : ($item ? $item->getStructure() : null);
	}

	/**
	 * @param KObjectArray $item
	 * @return ComElementsViewElement
	 */
	public function setItem(KObjectArray $item)
	{
		$this->_item = $item;
		return $this;
	}


	public function getSearchData()
	{
		$data = array();
		$multifield = $this->isMultifield();

		if($this->isRepeatable())
		{
			$indexes = array_keys($this->getValues());
		}else{
			$indexes = array(null);
		}

		foreach($indexes AS $index)
		{
			$index = (int) $index;
			if($multifield && count($this->_indexable)){
				foreach($this->_indexable AS $key){
					$data[] = new KConfig(array('name' => $key, 'value' => $this->getValue($key, $index), 'index' => $index));
				}
			}else{
				$data[] = new KConfig(array('name' => $multifield ? 'value' : null, 'value' => $this->getValue('value', $index), 'index' => $index));
			}
		}

		return $data;
	}


	/**
	 * Return the views output
	 *
	 * @return string 	The output of the view
	 */
	public function display()
	{
		$hash = md5(serialize($this->_config).$this->_index);
		if(!isset($this->_outputs[$hash]))
		{
			$identifier = clone $this->getIdentifier($this->_layout);
			if($item = $this->getItem()) $identifier->package = $item->getIdentifier()->package;

            $this->getTemplate()->setView($this);
			$data = $this->_data;
			$data['element'] = $this;

			//Try loading a package override template
			try{
                $layout = clone $identifier;
				$this->_outputs[$hash] = $this->getTemplate()
					->loadIdentifier($layout, $data)
					->render();
			}
			catch(KException $e)
			{
				if($e instanceof KTemplateException){
                    //Fallback to com_elements templates
	                $layout = clone $identifier;
	                $layout->package = 'elements';

					$this->_outputs[$hash] = $this->getTemplate()
						->loadIdentifier($layout, $data)
						->render();
				}else{
					$class = get_class($e);
					throw new $class($e->getMessage(), $e->getCode(), $e->getPrevious());
				}
			}
		}

		//Assign the cached output to the public output property used by parent
		$this->output = $this->_outputs[$hash];

		return parent::display();
	}


	public function getTemplate()
	{
		static $templates = array();
		if(!isset($templates[$this->_package])){
			$templates[$this->_package] = parent::getTemplate();
		}

		$templates[$this->_package]->setView($this);
		//$this->_template = $templates[$this->_package];

		return $templates[$this->_package];
	}


	/**
	 * Method to set a template object attached to the view
	 *
	 * @param   mixed   An object that implements KObjectServiceable, an object that
	 *                  implements KServiceIdentifierInterface or valid identifier string
	 * @throws  KDatabaseRowsetException    If the identifier is not a table identifier
	 * @return  KViewAbstract
	 */
	public function setTemplate($template)
	{
		if(!($template instanceof KTemplateAbstract))
		{
			if(is_string($template) && strpos($template, '.') === false )
			{
				$identifier = clone $this->getIdentifier();
				$identifier->package = $this->_package;
				$identifier->path = array('template');
				$identifier->name = $template;
			}
			else $identifier = $this->getIdentifier($template);

			if($identifier->path[0] != 'template') {
				throw new KViewException('Identifier: '.$identifier.' is not a template identifier');
			}

			$template = $identifier;
		}

		$this->_template = $template;

		return $this;
	}

	/**
	 * @param string $style
	 * @return null|string
	 */
	public function render()
	{
		if($this->isRepeatable()){
			$output = array();
			$values = $this->getValues();
			$show = $this->getConfig('show','all','render');
			$index = 0;
			foreach($values AS $value)
			{
				if($show == 'not_first' && $index == 0) continue;

				$this->_index = $index;
				$output[]= $this->_render();

				if($show == 'first' && $index == 0) break;
				$index++;
			}

			$this->_index = null; //Reset index
			$return = $this->addSeparators($output, $this->getConfig('separated_by', null, 'render'));
		}else{
			$return = $this->_render();
		}

		return $return;
	}

	/**
	 * @return null|string
	 */
	protected function _render()
	{
        $value = $this->getValue();
        $this->value = is_array($value) ? new KConfig($value) : $value;
        $this->id = $this->getControlId();
		$this->item = $this->getItem();

		$this->setLayout('render');
		try{
			return $this->display();
		}catch(Exception $e){
			return $this->getValue();
		}
	}


	/**
	 * @return null|string
	 */
	public function edit()
	{
		$this->setLayout('edit');

		if($this->isRepeatable()){

			$values = $this->getValues();
			if($values instanceof KConfig) $values = $values->toArray();
			$elements = array();
			foreach($values AS $index => $value)
			{
				$this->_index = $index;
				$elements[] = $this->_edit();
			}

			$this->_index = '*new*';
			$elements['*new*'] = $this->_edit();

			return $this->getTemplate()->loadIdentifier('com://site/elements.view.elements.repeatable.edit', array('elements' => $elements, 'element_id' => $this->getControlId(null, false, false)))->render();
		}else{
			return $this->_edit();
		}

		return $return;
	}

	/**
	 * @return null|string
	 */
	protected function _edit()
	{
        $value = $this->getValue();
        $this->value = is_array($value) ? new KConfig($value) : $value;
        $this->control_name = $this->getControlName();
        $this->control_id = $this->id = $this->getControlId();

		try{
			$content = $this->display();
		}catch(Exception $e){
			$content = $this->getConfigForm(array(), 'submission');
		}

		return $this->getTemplate()->loadIdentifier('com://site/elements.view.elements.element.edit', array('content' => $content))->render();
	}

	/**
	 * Gets the form control name
	 * @param string $key
	 * @param string $group
	 * @param null $index
	 * @return string
	 */
	public function getControlName($key = null, $group = null, $index = null)
	{
		$name = array();
		$id = $this->getHandle();
		$index = $index !== null ? $index : $this->getIndex();
		$group = $group ? $group : $this->_input_group;

		//Add group
		if($group !== null && $group !== false) $name[] = $group;

		//Add name
		$name[] = $id;

		//Handle repeatable
		if($index !== null && $index !== false)	$name[] = $index;

		//Add key
		if($key !== null) $name[] = $key;

		return array_shift($name).(count($name) ? '['.implode('][',$name).']' : '');
	}


	/**
	 * Gets the ID for a field
	 * @param string $key
	 * @param string $group
	 * @param null $index
	 * @return mixed
	 */
	public function getControlId($key = null, $group = null, $index = null){
		$name = $this->getControlName($key, $group, $index);

		return 'element_'.preg_replace('#[^a-zA-Z0-9_]+#','_', preg_replace('#\]\[$#','_',rtrim($name,']')));
	}


	/**
	 * @return string
	 */
	public function config()
	{
		$this->setLayout('config');
		try{
			return $this->display();
		}catch(Exception $e){
			return $this->getConfigForm();
		}
	}

	/**
	 * @param string $position
	 * @return string
	 */
	public function renderConfig($position = null)
	{
		$this->setLayout('render_config');
		try{
			$this->position = $position;
			return $this->display();
		}catch(Exception $e){
			$position = $position ? (array) $position : $position;
			array_unshift($position, 'elements');
			return $this->getConfigForm($position, 'render');
		}
	}

	/**
	 * @param array $form_name
	 * @param string $config_group
	 * @return string
	 */
	public function getConfigForm($form_name = array('elements'), $config_group = 'edit')
	{
		$package = $this->getPackage();

		//Check for config override
		$path = JPATH_ROOT.'/components/com_'.$package.'/views/elements/'.$this->getType().'/config.xml';
		if(!$package || !file_exists($path)) $path = dirname(__FILE__).'/elements/'.$this->getType().'/config.xml';
		$default_path = dirname(__FILE__).'/elements/default.xml';

		$parameters = $this->getService('com://admin/elements.helper.parameter', array('path' => $default_path));

		//Load and merge the xml files
		$default_xml = JFactory::getXMLParser('Simple');
		$element_xml = JFactory::getXMLParser('Simple');
		$default_xml->loadFile($default_path);
		$element_xml->loadFile($path);

		if ($default_xml && $element_xml)
		{
			$default_params =& $default_xml->document->params;
			$element_params =& $element_xml->document->params;
			if ($default_params && $element_params)
			{
				foreach ($element_params as &$element_param)
				{
					$match = false;
					foreach ($default_params AS &$default_param)
					{
						if($element_param->attributes('group') == $default_param->attributes('group')){
							$match = true;

							foreach($element_param->children() AS $child)
							{
								$new = $default_param->addChild('param', $child->attributes());
								foreach($child->children() AS $subchild){
									$new->addChild($subchild->name(), $subchild->attributes())->setData($subchild->data());
								}
							}
						}
					}
					if(!$match)
					{
						$new_param = $default_xml->document->addChild('params', $element_param->attributes());
						foreach($element_param->children() AS $child)
						{
							$new = $new_param->addChild('param', $child->attributes());
							foreach($child->children() AS $subchild){
								$new->addChild($subchild->name(), $subchild->attributes())->setData($subchild->data());
							}
						}
					}
				}
			}

			$group = $config_group == 'edit' ? '' : $config_group;

			foreach($default_params AS $params)
			{
				if($params->attributes('group') == $group){

					foreach($params->children() AS $param)
					{
						$name = $param->attributes('name');

						//Create the type drop down
						if($name == 'type'){
							$types = $this->getElementTypes();
							$package = null;
							foreach($types AS $type){
								if(!$package || $type->getPackage() != $package){
									$package = $type->getPackage();
									$param->addChild('option', array('value' => null))->setData('** '.ucfirst($package == 'elements' ? 'default' : $package).' **');
								}

								$param->addChild('option', array('value' => $type->type))->setData($type->name);
							}
						}

						//Set id to hidden if not editable
						if($name == 'id' && $this->isFixed()){
							$param->addAttribute('description','hidden');
							$param->addAttribute('type','hidden');
						}

						//If the field is the group field, add the groups from the type
						if($name == 'group' && !$this->isFixed())
						{
							$groups = $this->getGroups();
							foreach($groups AS $group)
							{
								$param->addChild('option', array('value' => $group))->setData(ucfirst($group));
							}
							$param->addChild('option', array('value' => '*custom*'))->setData(JText::_('Custom'));
						}

						//Remove repeatable & group option if element is fixed
						if( ($name == 'repeatable' && $this->isFixed()) ||
							($name == 'group' && $this->isFixed()) ||
							($name == 'group_custom' && $this->isFixed()) ||
							($name == 'separated_by' && !$this->isRepeatable()))
						{
							$params->removeChild($param);
						}
					}
				}
				$parameters->setXML($params);
			}
		}

		//Re-map the custom group variable
		$config = clone $this->getConfig(null, new KConfig(), $config_group);
		$config->id = $this->getId();
		$config->type = $this->getType();
		if($config->group_custom)
		{
			$config->group_custom = $config->group;
			$config->group = '*custom*';
		}

		//Load the config data into params object
		$parameters->loadArray($config->toArray());

		//Add handle to name array
		$form_name[] = $this->getHandle();
		$form_name = array_shift($form_name).'['.implode('][', $form_name).']';

		//Set the group name if edit
		$group = $config_group == 'edit' ? '_default' : $config_group;
		$html = $parameters->render($form_name, $group);

		return $html;
	}

	/**
	 * Add separation HTML to content
	 *
	 * @param $content
	 * @param $rule
	 * @return string
	 */
	public function addSeparators($content, $rule)
	{
		// Ensure rule is an instance of KConfig
		$rule = $rule instanceof KConfig ? $rule : new KConfig((array) (is_string($rule) ? json_decode($rule, true) : $rule));

		// Retrieve seperator type from rule
		$separator = $rule->separator;

		// If seperator type is an instance of KConfig, reformat into HTML
		if($separator instanceof KConfig && $separator->tag)
		{
			$separator = '</'.$separator->tag.'><'.$separator->tag.'/>';
		}

		// Retrieve tag values from rule object
		$tag            = $rule->tag;
		$enclosing_tag  = $rule->enclosing_tag;

		// Check tag has a value
		if($tag)
		{
			// Add separator tags to content
			foreach( $content AS &$c){
				$c = '<'.$tag.'>'.trim($c).'</'.$tag.'>';
			}
		}

		// Return reformatted HTML
		return ($enclosing_tag ? '<'.$enclosing_tag.'>' : '').implode($separator, $content).($enclosing_tag ? '</'.$enclosing_tag.'>' : '');

	}


	/**
	 * Validate
	 *
	 * @notes Use this to implement custom element validation rules
	 * @return bool
	 */
	public function validate()
	{
		return true;
	}


	/**
	 * Overridable save method. Must return true or an array of errors
	 * @return bool | array()
	 */
	public function save(){
		if($this->isRepeatable()){
			if($values = $this->getValues()){
				foreach($values AS $index => $value)
				{
					$this->_index = $index;
					if(!$this->_save()) return false;
				}
			}
		}else{
			if(!$this->_save()) return false;
		}

		return true;
	}


	protected function _save()
	{
		return true;
	}


	/**
	 * Overrideable delete method to be invoked when the item is deleted
	 * @return bool
	 */
	public function delete()
	{
		if($this->isRepeatable()){
			if($values = $this->getValues()){
				foreach($values AS $index => $value)
				{
					$this->_index = $index;
					if(!$this->_delete()) return false;
				}
			}
		}else{
			if(!$this->_delete()) return false;
		}

		return true;
	}


	protected function _delete()
	{
		return true;
	}


	/**
	 * Gets the values to be stored in the attached item on save
	 *
	 * @notes Use this to implement custom actions when running setValues during the element save process.
	 * @return bool
	 */
	public function getSubmissionValue()
	{
		$value = $this->getValues();
		if($this->isRepeatable()){
			$values = new KConfig();
			$i = 0;
			foreach($value AS $index => $v){
				$this->_index = $index;
				if($this->_hasValue(null, $index)) $values->$i = $v;
				$i++;
			}
			$value = $values;
		}

		// Ensure data is returned as an array or scalar value
		return $value instanceof KConfig ? KConfig::unbox($value) : $value;
	}


	/**
     * Get a route based on a full or partial query string
     *
     * option, view and layout can be ommitted. The following variations
     * will all result in the same route
     *
     * - foo=bar
     * - option=com_mycomp&view=myview&foo=bar
     *
     * In templates, use @route()
     *
     * @param	string	The query string used to create the route
     * @param 	boolean	If TRUE create a fully qualified route. Default TRUE.
     * @return 	string 	The route
     */
    public function getRoute( $route = '', $fqr = true)
    {
        //Parse route
        $parts = array();
        parse_str(trim($route), $parts);

        //Check to see if there is package information in the route if not add it
        if(!isset($parts['option'])) {
            $parts['option'] = 'com_'.$this->getPackage();
        }

        //Add the view information to the route if it's not set
        if(!isset($parts['view']))
        {
            $parts['view'] = $this->getItem()->getIdentifier()->name;
        }

        //Add the model state only for routes to the same view
        if($parts['view'] == $this->getName())
        {
            $state = $this->getModel()->getState()->toArray();
            $parts = array_merge($state, $parts);
        }

	    //Add Itemid for frontend routes
	    if(!isset($parts['Itemid']) && JFactory::getApplication()->isSite()){
		    $parts['Itemid']  = KRequest::get('request.Itemid','int');
	    }

	    //Create the route
        $route = KService::get('koowa:http.url', array('url' => JRoute::_('index.php?'.http_build_query($parts))));

        //Add the host and the schema
        if($fqr)
        {
            $route->scheme = $this->getBaseUrl()->scheme;
            $route->host   = $this->getBaseUrl()->host;
        }

        return $route;
    }
}