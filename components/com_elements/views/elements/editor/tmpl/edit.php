<?php
/**
 * User: Oli Griffiths
 * Date: 23/06/2012
 * Time: 13:29
 */

$element = $this->getView();
if(!$element->hasJs):
?>
<script>
	;(function($){
		$(function(){
			$('#element_<?= $element->id ?>').bind('repeat', function(e, row){
				row = $(row)
				var editor = row.find('textarea')[0]
                tinyMCE.execCommand('mceAddControl',false,editor.id);
                tinyMCE.execCommand('mceFocus', false, editor.id);
			})
		})
	})(window.jQuery)
</script>
<?php
$element->hasJs = true;
endif;

echo $this->renderHelper('com://admin/default.template.helper.editor.display', array('name' => $control_name, $control_name => $value));
