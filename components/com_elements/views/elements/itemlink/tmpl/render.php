<?php
/**
* Com Catalogue Element Cart Control Renderer
*
* @author      Kyle Waters <development@organic-development.com>
* @category    Cart Control Element
* @package     Catalogue
* @uses        Com_Elements
*/

$package = $element->getPackage();

$class = $element->getConfig('link_class','','render');
?>
<a href="<?= @route('option=com_'.$package.'&view='.$item->getIdentifier()->name.'&id='.$item->id) ?>" <?= $class ? 'class="'.$class.'"' : '' ?>>
<?= @text($element->getConfig('text','read more','render')) ?>
</a>