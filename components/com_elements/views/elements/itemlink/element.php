<?php
/**
 * Created By: Oli Griffiths
 * Date: 16/10/2012
 * Time: 21:15
 */
defined('KOOWA') or die('Protected resource');

class ComElementsViewElementsItemlinkElement extends ComElementsViewElement
{
	/**
	 * Always return true
	 */
	protected function _hasValue($value = null, $index = null)
	{
		return true;
	}

	/**
	 * There is no edit screen
	 * @return bool
	 */
	public function edit()
	{
		return false;
	}
}