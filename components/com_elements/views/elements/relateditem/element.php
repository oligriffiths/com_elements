<?php
/**
 * Com ......
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_***
 * @package     ******
 * @uses        Com_
 */
class ComElementsViewElementsRelateditemElement extends ComElementsViewElement
{
	protected $_related_items = array();

	public function reset()
	{
		$this->_model = null;
		return parent::reset();
	}


	public function setModel()
	{
		$identifier = clone $this->getIdentifier();
		$identifier->package = $this->getConfig('package') ?: $this->getItem()->package;
		$identifier->path = array('model');
		$identifier->name = KInflector::pluralize($this->getConfig('model'));

		return parent::setModel($identifier);
	}


	protected function _hasValue($key, $index = null)
	{
		if(!parent::_hasValue($key, $index))
		{
			return false;
		}

		$item = $this->getRelatedItem($this->getValue($key, $index));

		return $item && !$item->isNew();
	}


    public function getRelatedItem($id)
	{
		if(!isset($this->_related_item[$id])){
			try
			{
				$this->_related_items[$id] = $this->getModel()->set('id', $id)->getItem();
            }
			catch(Exception $e)
			{
				$this->_related_items[$id] = false;
			}
		}

		return $this->_related_items[$id];
	}


	protected function _render()
	{
		try
		{
			if($item = $this->getRelatedItem($this->getValue())){
				if(!$item->isElementable()){
					return 'Related item does not implement the elementable behavior';
				}

				$layout = $this->getConfig('layout',null, 'render');
				if(!$layout)
				{
					return 'No layout defined in config';
				}

				return $item->render($layout);
			}
		}
		catch(Exception $e)
		{
			return $e->getMessage();
		}
	}

	public function render()
	{
		$this->content = parent::render();

		$this->setLayout('render');
		return $this->display();
	}
}