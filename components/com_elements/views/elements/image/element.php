<?php
/**
 * Created By: Oli Griffiths
 * Date: 03/09/2012
 * Time: 10:03
 */
defined('KOOWA') or die('Protected resource');

class ComElementsViewElementsImageElement extends ComElementsViewElement
{
	protected $_placeholder = 'components/com_elements/views/elements/image/assets/images/placeholder.png';

	//Root path
	protected $_root;

    public function __construct(KConfig $config)
    {
        parent::__construct($config);

	    $this->_root = JPATH_ROOT.'/';
    }

    /**
	 * Initialises the object
	 *
	 * @param object  An optional KConfig object with configuration options
	 */
	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'multifield' => true
		));
		parent::_initialize($config);
	}

	protected function _hasValue($key = null, $index = null)
	{
        if ($this->getConfig('use_placeholder', 0)) {
            return true;
        } else {
    		return (bool) trim($this->getValue('src', $index)) || trim($this->getValue('src_uploaded', $index));
        }
	}


	protected function loadScripts()
	{
		static $loaded;
		if(!$loaded)
		{
			JHTML::script('components/com_elements/views/elements/image/assets/js/foresight.min.js');

			$doc =& JFactory::getDocument();
			$doc->addStyleDeclaration(".fs-img {
			font-family: 'image-set( url({src}) 1x low-bandwidth, url({protocol}://{host}{directory}{filename}-hd.{ext}) 2x high-bandwidth )';
			display:none;
		}");
			$loaded = true;
		}
	}

	protected function _edit()
	{
		$value          = new KConfig($this->getValue());
		$this->images   = $this->listImages();
		$this->source   = preg_replace('#^/#','',$value->src);

		//If no image set, use placeholder image
		if (!$this->source || !file_exists($this->_root.$this->source)) {
			$this->source = $this->_placeholder;
		}

		return parent::_edit();
	}



	public function listImages()
	{

        //if this is an avatar listing, then we need to show only avatars available uploaded by this user if available

		$path = $this->_root.$this->getUploadPath();
		$images = glob($path.'*.{jpg,jpeg,gif,bmp,tiff,png}', GLOB_BRACE);

		$return = array();
		foreach($images AS $image){
			$path = preg_replace('#^'.preg_quote($this->_root).'#','', $image);
			$return[] = (object) array('value' => $path, 'text' => basename($path), 'group' => false);
		}

		return $return;
	}


	public function _render()
	{
		$value = new KConfig($this->getValue());

		$this->source       = preg_replace('#^/#','',$value->src);
		$this->source_hd    = null;
		$this->zoom_source  = null;
		$hd                 = (bool) $this->getConfig( 'hd', null, 'render' );
		$this->max_width    = (float) $this->getConfig( 'max_width', null, 'render' );
		$this->max_height   = (float) $this->getConfig( 'max_height', null, 'render' );
		$this->zoom_width   = (float) $this->getConfig( 'zoom_width', null, 'render' );
		$this->zoom_height  = (float) $this->getConfig( 'zoom_height', null, 'render' );
		$display_width      = $this->getConfig( 'display_width', null, 'render' );
		$display_height     = $this->getConfig( 'display_height', null, 'render' );
		$usePlaceHolder     = $this->getConfig('use_placeholder', 1, 'render');

		$resize_width = (float) ($this->max_width ?: ($this->_isPercentage($display_width) ? $display_width : 0));
		$resize_height = (float) ($this->max_height ?: ($this->_isPercentage($display_height) ? $display_height : 0));

		//If no image set, use placeholder image
		if ((!$this->source || !file_exists($this->_root.$this->source)) && $usePlaceHolder) {
			$this->source = $this->_placeholder;
		}

		//If src is set, try to resize
		if ($this->source){

			//If the image is not a URL and exists on the filesystem, attempt to resize
			if(!preg_match('#^[a-z]+://#',$this->source) &&
				file_exists($this->_root.$this->source)){

				if($resize_width || $resize_height){
					$path = $this->_root.$this->source;

					$cachepath = array('cache');

					//Construct the cachepath
					if($this->source == $this->_placeholder)
					{
						$cachepath[] = 'com_elements';
						$cachepath[] = 'image';
					}else{
						$package = $this->getPackage();
						$cachepath[] = 'com_'.($package ? $package : 'elements');

						$item = $this->getItem();
						$cachepath[] = $item->getIdentifier()->name;
						$cachepath[] = 'images';

						$pks = array();
						foreach($item->getTable()->getColumns() AS $column_id => $column) if($column->primary) $pks[] = $item->get($column_id);
						if(count($pks)) $cachepath[] = implode('.',$pks);
					}

					//Setup options
					$options = array(
						'file' => $path,
						'width' => $resize_width,
						'height' => $resize_height,
						'cachepath' => $this->getConfig('cachepath', implode('/', $cachepath))
					);

					//Do standard resize
					try{
						if($info = $this->getService('com://site/codebox.helper.image')->resize($options)){

							$file_path  = $info['path'];
							$width      = $info['width'];
							$height     = $info['height'];

							$ratio = $height / $width;
							if((!$display_width && $display_height && !$this->_isPercentage($display_height))) $display_width = $display_height / $ratio;
							if((!$display_height && $display_width && !preg_match('#%$#', $display_width))) $display_height = $display_width * $ratio;

							$this->source = preg_replace('#^'.preg_quote(JPATH_ROOT).'/#','', $file_path);

							//If the display width and height are not set, use the resized size
							if(!$display_width && !$display_height) {
								$display_width = $width;
								$display_height = $height;
							}
						}
					}catch(KTemplateHelperException $e)
					{
						return $e->getMessage();
					}

					//If hd is on, attempt to create HD image
					if($hd)
					{
						try{
							$options['hd'] = true;
							$file_path_hd = @helper('image.resize', $options);
							$this->source_hd = preg_replace('#^'.preg_quote(JPATH_ROOT).'/#','', $file_path_hd);

						}catch(KTemplateHelperException $e)
						{
							return $e->getMessage();
						}
					}
				}

                if($this->zoom_width || $this->zoom_height)
                {
                    $options['width'] = $this->zoom_width;
                    $options['height'] = $this->zoom_height;

	                try{
	                    if($info = $this->getService('com://site/codebox.helper.image')->resize($options)){

	                        $file_path  = $info['path'];
	                        $this->zoom_source = preg_replace('#^'.preg_quote(JPATH_ROOT).'/#','', $file_path);
	                        $this->zoom_width = $info['width'];
	                        $this->zoom_height = $info['height'];
	                    }
	                }catch(KException $e){
		                return $e->getMessage();
	                }
                }
			}

			//If either display width or height are set, use their values, else default to max
			$display_width  = $display_width || $display_height ? $display_width : $resize_width;
			$display_height  = $display_width || $display_height ? $display_height : $resize_height;

			//Add units
			$display_width = preg_match('#^[0-9]*$#', $display_width) && $display_width ? $display_width.'px' : $display_width;
			$display_height = preg_match('#^[0-9]*$#', $display_height) && $display_height ? $display_height.'px' : $display_height;
		}

		//Set the width & height
		$this->width = $display_width;
		$this->height = $display_height;

		//Setup item link
		if($this->getConfig('link_to_item', null, 'render')){
			$item = $this->getItem();
			$item_identifier = $item->getIdentifier();
			$this->link = $this->getRoute('option=com_'. $item_identifier->package .'&view='. $item_identifier->name .'&id='. $item->id );
		}else{
			$this->link = null;
		}

		return parent::_render();
	}


	protected function _isPercentage($value)
	{
		return preg_match('#%$#', $value);
	}


	protected function getFile()
	{
		$file = new KConfig(KRequest::get('files.'.$this->getControlName(null, false, false),'raw'));
		if(!$file->count()) return null;

		$return = new KConfig();
		$repeatable = $this->isRepeatable();
		foreach(array('error','name','type','tmp_name','size') AS $property){

			if(!$prop = $file->$property) continue;

			if($this->_input_group) $prop = $prop->{$this->_input_group};

			if($repeatable){
				$prop = new KConfig(array_shift($prop->toArray()));
			}

			$return->$property = $prop->upload;
		}

		if($return->error) return null;
		if(!$return->name) return null;
		if(!$return->type) return null;
		if(!$return->tmp_name) return null;
		if(!$return->size) return null;

		return $return;
	}

	public function upload(KCommandContext $context)
	{
		$file = $this->getFile();

		if(empty($file)) return false;

		$file = new KConfig($file);
		if(!$file->tmp_name)
		{
			//throw error
			$context->setError(new KException('Error uploading file, no filename found'));
			return false;
		}

		if(!$file->size)
		{
			//throw error
			$context->setError(new KException('Error uploading file, no file size found'));
			return false;
		}


		//Validate the image is an image
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime = finfo_file($finfo, $file->tmp_name);
		if (preg_match('#^image/#',$mime)) {

			$tmp_path = JFactory::getApplication()->getCfg('tmp_path');
			$tmp_path = $tmp_path ? $tmp_path : JPATH_ROOT.'/tmp';

			if(!is_dir($tmp_path) && !@mkdir($tmp_path))
			{
				$context->setError(new KException('The temp directory '.$tmp_path.' does not exist'));
				return false;
			}

			if(!is_writable($tmp_path))
			{
				$context->setError(new KException('The temp directory '.$tmp_path.' is not writeable'));
				return false;
			}

			if(!move_uploaded_file($file->tmp_name, $tmp_path.'/'.$file->name))
			{
				//throw error
				$context->setError(new KException('Error uploading file, unable to move uploaded file'));
				return false;
			}

			//@TODO: Apply some resizing

			$url = KRequest::root().preg_replace('#'.preg_quote(JPATH_ROOT).'#','',$tmp_path);

			return array('result' => true, 'url' => $url.'/'.$file->name);

		}else{

			//throw exception
			$context->setError(new KException('Error uploading file, file type not allowed'));
			return false;
		}
	}

	public function validate()
	{
		if($this->getValue('src') && file_exists($this->_root.$this->getValue('src'))){

			$destination = $this->getUploadPath();
			if(!$destination) return false;

			$destination_path = JPATH_ROOT.$destination;

			if(!is_writable($destination_path))
			{
				if(!$this->createUploadPath()){
					throw new KException('Upload path '.$destination_path.' is not writeable');
				}
			}
		}

		return true;
	}

	protected function _save()
	{
		$src        = ltrim($this->src_uploaded ?: $this->src, '/');
		if($this->src_uploaded) $this->unsetValue('src_uploaded');
		$delete     = $this->delete;

		if($delete){
			if(!$this->_delete()){
				return array('Unable to delete the image '.$this->src);
			}
		}else if($src){
			if(!file_exists($this->_root.$src)){
				$this->setValue(null,'src');
			}else{

				$file       = basename($src);
				$tmp_path   = JFactory::getApplication()->getCfg('tmp_path').'/'.ltrim($file,'/');
				$path       = $this->_root.ltrim($src,'/');

				//Check if image is in the tmp path, if so move it
				if($path == $tmp_path)
				{
					if(file_exists($path)){
						$destination = $this->getUploadPath();
						$destination_path = $this->_root.$destination;
						if(!$destination) return false;

						//Create the upload path if not exists
						$this->createUploadPath();

						//Ensure destination is writeable
						if(is_writable($destination_path))
						{
							$destination_path .= $file;

							//Move the file
							if(rename($path, $destination_path)){

								//Set the value to the new path
								$this->setValue($destination.$file, 'src');
							}
						}
					}else{
						return false;
					}
				}
			}
		}

		return true;
	}


	protected function createUploadPath()
	{
		$destination = $this->getUploadPath();
		$destination_path = $destination;

		//Create the directory path
		if(!is_dir($destination_path))
		{
			$parts = explode('/',$destination);
			$tmp_path = JPATH_ROOT;

			foreach($parts AS $part)
			{
				if(!is_dir($tmp_path))
				{
					if(!mkdir($tmp_path)){
						return false;
					}
				}

				$tmp_path .= '/'.$part;
			}
		}

		return true;
	}


	protected function getUploadPath()
	{
		$destination = $this->getConfig('destination', 'images');

		//Extract vars from the path
		preg_match_all('#\$([^\/\$\-]*)#', $destination, $matches);

		$vars = $matches[1];
		$item = $this->getItem();

		foreach($vars AS $var){
			if(!isset($item->$var)) {
				throw new KException('Upload path contains a variable $'.$var.' that doesn\'t exist');
			}
			if($item->$var && !is_scalar($item->$var)){
				throw new KException('Upload path contains a variable $'.$var.' is a non scalar value');
			}

			if(!$item->$var) return false;
			$destination = str_replace('$'.$var, $item->$var, $destination);
		}

		return trim($destination,'/').'/';
	}

	protected function _delete()
	{
		$src = $this->src;
		if($src && file_exists($this->_root.$src))
		{
			if(unlink($this->_root.$src)){
				$this->unsetValues();
			}else{
				return false;
			}
		}else{
			$this->unsetValues();
		}

		return true;
	}
}