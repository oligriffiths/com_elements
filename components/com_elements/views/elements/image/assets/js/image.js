
var fileUploadInit;

(function($) {

    $(function(){

        fileUploadInit = function(){
            var $this = $(this),
                $dropzone = $this

            //Add change event to image selector
            $this.find('.src select').change(function(){
                $this.find('.image img').attr('src', $this.data('root')+'/'+$(this).val())
            })

            $this.fileupload({
                autoUpload: true,
                dropZone: $dropzone,
                formData: {action : 'callback', callback: 'upload', '_token': $this.data('token')},
                maxFileSize: 5000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                process: [
                    {
                        action: 'load',
                        fileTypes: /^image\/(gif|jpeg|png)$/,
                        maxFileSize: 20000000 // 20MB
                    },
                    {
                        action: 'resize',
                        maxWidth: 1440,
                        maxHeight: 900
                    },
                    {
                        action: 'save'
                    }
                ],
                uploadTemplate: function(data){
                    var that = $this.data('fileupload')

                    if(that.options.filesContainer) that.options.filesContainer.empty()

                    var tmpl = $('<div />');
                    var $row = $('<div><span class="name"></span></div>')
                    $.each(data.files, function(){
                        var file = this;

                        file.error = that._hasError(file);
                        if (file.error) {
                            tmpl.append($('<div />',{'class':'alert alert-error','text': file.name+' : '+file.error}))
                        }else{
                            tmpl.append($('<div />',{'class':'alert alert-info','text': 'Uploading: '+file.name}))
                        }
                    })

                    return tmpl;
                },
                drop: function(e, data){
                    if(data.files.length > 1 && $this.data('repeatable') == 1){
                        var $repeatable = $this.parent().parent().parent();

                        var files = data.files

                        //Ensure only the first file gets uploaded by the primary image
                        data.files = [data.files[0]]

                        $.each(files, function(index){
                            var file = this;
                            if(index > 0){
                                var $new_row = $repeatable.repeatable('repeat').find('li:last-child .element-image')
                                $new_row.fileupload('add', {files: [file]});
                            }
                        })
                    }
                },
                done: function(e, data){

                    if(data.filesContainer) data.filesContainer.empty()

                    var result = data.result

                    if(!result){
                        alert('No response received from server, upload failed')

                    }else if(result.result == true){

                        $.each(data.files, function(){
                            var file = this;
                            var $row = $('<div />',{'class':'alert alert-success','text': 'Uploaded: '+file.name});
                            $row.append('<a data-dismiss="alert" class="close" href="#">×</a>');
                            data.filesContainer.append($row)
                        })

                        $this.find('.image').removeClass('dropzone_empty')
                        $this.find('.image img').prop('src', result.url)

                        var $src = $this.find('.src_uploaded');
                        $src.prop('value', result.url)
                        $this.find('.src').remove()

                    }else{

                        if(data.filesContainer) data.filesContainer.empty()
                        $.each(result.errors, function(){

                            var $row = $('<div />',{'class':'alert alert-error','text': this.message});
                            $row.append('<a data-dismiss="alert" class="close" href="#">×</a>');
                            data.filesContainer.append($row)
                        })
                    }
                }
            });
        }

        $('[data-upload]').each(fileUploadInit)
    })
})(window.jQuery);