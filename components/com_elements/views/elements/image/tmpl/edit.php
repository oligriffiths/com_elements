<?php
/**
 * Com Elements Simple Image Element
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Image Element
 * @package     Com_Elements
 */

echo @helper('bootstrap.alert');

$element = $this->getView();
$structure = $element->getStructure();

$width  = $element->getConfig('width', '200px');
$height = $element->getConfig('height', '150px');
$post_url = @route('option=com_elements&view=element&format=json&type=image&id='.$element->id.'&package='.$element->getPackage().'&structure='.($structure ? $structure->id : ''))->setPath('/index');

global $image_element_js_loaded;
if(!$image_element_js_loaded): ?>
<style src="/components/com_elements/views/elements/image/assets/css/image.css" />
<style src="/components/com_elements/views/elements/image/assets/css/jquery.fileupload-ui.css" />

<script src="/components/com_elements/views/elements/image/assets/js/jquery.ui.widget.js" />
<script src="/components/com_elements/views/elements/image/assets/js/jquery.fileupload.js" />
<script src="/components/com_elements/views/elements/image/assets/js/jquery.fileupload-fp.js" />
<script src="/components/com_elements/views/elements/image/assets/js/jquery.fileupload-ui.js" />
<script src="/components/com_elements/views/elements/image/assets/js/image.js" />

<script>
    ;(function($){
        $(function(){
            $('#element_<?= $element->id ?>').bind('repeat', function(e, row){
	            var element = $(row).find('.element-image');
                fileUploadInit.apply($(element))
            })
        })
    })(window.jQuery)
</script>
<?php
	$image_element_js_loaded = true;
endif;
?>

<div class="element-image dropzone" data-upload="<?= $element->id ?>" data-url="<?= $post_url ?>" data-controlname="<?= $control_name ?>" data-token="'<?= JUtility::getToken() ?>'" data-repeatable="<?= (int) $element->isRepeatable() ?>" data-root="<?= KRequest::root() ?>">

    <input type="hidden" class="src_uploaded" name="<?= $control_name ?>[src_uploaded]" />

	<div class="image <?= !$value->src ? 'dropzone_empty' : '' ?>">
		<img src="<?= KRequest::root().'/'.$source ?>" />
	</div>

	<div class="details">

		<div class="file form-inline">
			<div class="src">
				<label><?= @text('Select: ') ?></label>
				<?= @helper('select.optionlist', array('name' => $control_name.'[src]', 'options' => $images, 'selected' => $source, 'user_id' => 5)) ?>
				<?= @text('- or -') ?>
            </div>
			<div class="btn btn-file">
				<span><?= @text('Upload') ?></span>
	            <input name="<?= $control_name ?>[upload]" class="upload-button" type="file" size="1" accept="image/*" />
	        </div>
        </div>

        <div class="fileupload-progress fade">
            <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                <div class="bar" style="width:0%;"></div>
            </div>
        </div>

		<div class="files">

		</div>

		<div class="fields">
	        <input type="text" name="<?= $control_name ?>[alt]" value="<?= $value->alt ?>" placeholder="Image title" />

			<br />
            <label for="<?= $control_id ?>-delete" class="checkbox inline">
                <input type="checkbox" name="<?= $control_name ?>[delete]" id="<?= $control_id ?>-delete" value="1" />
				<?= @text('Delete?') ?>
            </label>
        </div>

	</div>

</div>
