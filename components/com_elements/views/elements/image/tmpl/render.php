<?php
/**
 * Com Elements Image Element Renderer
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_Elements
 * @package     Image
 * @uses        Com_Elements
 * @uses        Com_Organic
 */

if ($source) :
?>
    <?php if($link): ?>
    <a href="<?= $link ?>">
	<?php endif ?>
    <img src="<?= KRequest::root() . '/' . $source ?>" data-original="<?= $value->src ? (KRequest::root() . '/' . $value->src) : '' ?>" data-zoom="<?= $zoom_source ? (KRequest::root() . '/' . $zoom_source) : '' ?>" style="<?= $width ? 'width:'.$width.';' : '' ?> <?= $height ? 'height:'.$height.';' : '' ?> <?= $max_width ? 'max-width:'.$max_width.'px;' : '' ?> <?= $max_height ? 'max-height:'.$max_height.'px;' : '' ?>" class="element-image" alt="<?= $value->alt ?>" data-zoom-width="<?= $zoom_width ?>" data-zoom-height="<?= $zoom_height ?>"/>
	<?php if($link): ?>
    </a>
	<?php endif ?>
<?php else :
    echo @text('no image found');
endif;
?>