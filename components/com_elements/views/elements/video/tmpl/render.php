<?php
/**
 * Com Elements Simple Image Element
 *
 * @author      Mark Head <mark@organic-development.com>
 * @category    Image Element
 * @package     Com_Elements
 */

// Replace the width and height in the embed code
$embed_code = $value->embed_code;
$embed_code = preg_replace( '/width="[^"]+"/', 'width="' . $value->width . '"', $embed_code );
$embed_code = preg_replace( '/height="[^"]+"/', 'height="' . $value->height . '"', $embed_code );
?>

<div class="video-embed">

    <?= $embed_code; ?>

</div>



