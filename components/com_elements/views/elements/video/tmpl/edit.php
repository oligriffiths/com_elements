<?php
/**
 * User: Mark Head <mark@organic-development.com>
 * Date: 23/06/2012
 * Time: 13:29
 */

$element = $this->getView();

// Element stylesheet
JHtml::stylesheet('components/com_elements/views/elements/video/assets/css/video.css');
?>

<ul class="video-form">

    <li>
        <label for="<?= $id . '_embed_code'; ?>"><?= @text( 'Embed Code' ); ?></label>
        <?=
            // Video type
            @helper( 'textarea.textarea', array(
                'type'      => 'text',
                'name'      => $element->getControlName( 'embed_code' ),
                'value'     => $value->embed_code,
                'attribs'   => array(
                    'id'            => $id . '_embed_code',
                    'placeholder'   => @text( 'Embed Code' )
                )
            ) ) ;
        ?>
    </li>

    <li>
        <label for="<?= $id . '_width'; ?>"><?= @text( 'Width (px)' ); ?></label>
        <?=
            // Video width
            @helper( 'form.input', array(
                'type'      => 'text',
                'name'      => $element->getControlName( 'width' ),
                'value'     => $value->width,
                'attribs'   => array(
                    'id'            => $id . '_width',
                    'placeholder'   => @text( 'Width (px)' )
                )
            ) );
        ?>
    </li>

    <li>
        <label for="<?= $id . '_height'; ?>"><?= @text( 'Height (px)' ); ?></label>
        <?=
            // Video height
            @helper( 'form.input', array(
                'type'      => 'text',
                'name'      => $element->getControlName( 'height' ),
                'value'     => $value->height,
                'attribs'   => array(
                    'id'            => $id . '_height',
                    'placeholder'   => @text( 'Height (px)' )
                )
            ) );
        ?>
    </li>

</ul>
