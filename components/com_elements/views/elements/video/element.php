<?php
/**
 * User: Mark Head <mark@organic-development.com>
 * Date: 18/09/2012
 * Time: 08:44
 */

class ComElementsViewElementsVideoElement extends ComElementsViewElement {

    /**
     * Initialises the object
     *
     * @param object  An optional KConfig object with configuration options
     */
    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'multifield' => true
        ));
        parent::_initialize($config);
    }

    protected function _hasValue()
    {
        return (bool) trim($this->getValue('embed_code'));
    }

}
