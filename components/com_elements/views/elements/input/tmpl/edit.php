<?php
/**
 * User: Oli Griffiths
 * Date: 23/06/2012
 * Time: 13:29
 */

$attribs = $element->getConfig('attribs');
$x = new SimpleXMLElement("<element $attribs/>");
$attribs = (array) $x->attributes();
$attribs = isset($attribs['@attributes']) ? $attribs['@attributes'] : array();
$attribs['id'] = $id;


if($pattern = $element->getConfig('pattern')){
	$attribs['pattern'] = $pattern;
}

if($element->getConfig('required')){
	$attribs['required'] = 'required';
}

if($placeholder = $element->getConfig('placeholder')){
	$attribs['placeholder'] = $placeholder;
}else{
	$attribs['placeholder'] = $element->getTitle();
}

$type = $element->getConfig('input_type','text');
switch($type){
	case 'float':
		$type = 'number';
		$attribs['step'] = 'any';
		break;
	case 'email':
		if(!isset($attribs['pattern'])) $attribs['pattern'] = "/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/";
		break;
}

echo @helper('form.input', array('type' => $type, 'name' => $control_name, 'value' => $value, 'size' => $element->getConfig('size'), 'attribs' => $attribs));