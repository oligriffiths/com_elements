<?php

$element = $this->getView();

$tag = $element->getConfig('tag', 'h1', 'render');
?>
<?= $tag ? '<'.$tag.' class="title">' : '' ?>
	<?php


	if($element->getConfig('link_to_item', null, 'render')) :
        $item_identifier = clone $element->getItem()->getIdentifier();
        $link = @route('option=com_'. $item_identifier->package .'&view='. $item_identifier->name .'&id='. $element->getItem()->id );
	?>
		<a href="<?= $link ?>" ><?= $value ?></a>
	<?php
	else :
	    echo $value;
	endif;
	?>
<?= $tag ? '</'.$tag.'>' : '' ?>