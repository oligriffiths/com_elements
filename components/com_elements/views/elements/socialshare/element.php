<?php
/**
 * Com ......
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_***
 * @package     ******
 * @uses        Com_
 */

/*
 * TODO: Linked in needs to be connected but Renshaw isn't using this at the moment...
 */
class ComElementsViewElementsSocialshareElement extends ComElementsViewElement
{
    protected function _hasValue($value = null, $index = null)
    {
        $has_value = false;

        // Check if any of the social buttons are turned on
        if($this->getConfig('facebook', false, 'render') ||
            $this->getConfig('twitter', false, 'render') ||
            $this->getConfig('google', false, 'render') ||
            $this->getConfig('pinterest', false, 'render') ||
            $this->getConfig('linkedin', false, 'render'))
        {
            $has_value = true;
        }

        return $has_value;
    }

    public function _render()
    {
        //useful for data attribute strings
        $this->sanitizedName = str_replace(array('"', "'"), '', $this->getItem()->name);

        //see if we're using bit.ly?
        //TODO: work this into the cache somehow

        if (!$this->shortUrl) {
            $this->shortUrl = $this->getShortUrl();
        }

        $this->sanitzedName = str_replace(array('"', "'"), '', $this->getItem()->name);

        $this->structure = $this->_structure;

        return parent::_render();
    }

    /*
     *  This gets the shortened url from google's shortener service, otherwise it returns the full url.
     *  It checks to see if the apc is turned on, and tries to get the url from apc, otherwise stores it in apc
     */
    protected function getShortUrl()
    {
        $item = $this->getItem();
        $identifier = $this->getItem()->getIdentifier();

        if (extension_loaded('apc')) {
            $ids = array();
            foreach($item->getTable()->getPrimaryKey() AS $column_id => $column) $ids[] = $item->get($column_id);

            $cache_id = 'com_elements.element.socialshare.url.' . $identifier.'.'.implode('.', $ids);
            if ($shortenedUrl = apc_fetch($cache_id)) {
                return $shortenedUrl;
            }
        }

        //this will not work until the base fix is applied in nooku
        $url = KRequest::url();
        $url->query = $this->getRoute('option=com_' . $identifier->package . '&view='. $identifier->name .'&id=' . $this->getItem()->id)->query;
        $shortenedUrl = $route = (string) $url;

        if ($this->getConfig('shortenurls', false, 'render') !== false) {
            //shorten the url
            $ch = curl_init('https://www.googleapis.com/urlshortener/v1/url');


            $postFields = array("longUrl" => $route);
            $postData = stripslashes(json_encode($postFields));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($postData))
            );
            $res = curl_exec($ch);
            $e = curl_error($ch);
            curl_close($ch);

            if ($e == 0) {
                //curl has not errored
                $res = json_decode($res);
                if (isset($res->id)) {
                    //the id is the shortened url
                    $shortenedUrl = $res->id;
                }
            }
        }


        if (extension_loaded('apc')) {
            apc_store($cache_id, $shortenedUrl);
        }

        return $shortenedUrl;

    }

    /*
     *  Get the count of shares on google plus via ajax post request
     *  This can not be cached as it needs to be up to date
     */
    public function googlePlusCount(KCommandContext $context) {
        $res = false;
        if ($context && $context->data->url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://clients6.google.com/rpc");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . urldecode($context->data->url) . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
            $res = curl_exec ($ch);
            $e = curl_error($ch);
            curl_close ($ch);

            if ($e === null || $res == '') {
                $res = false;
            } else {
                $res = json_decode($res);
                if (isset($res[0]->result->metadata->globalCounts->count)) {
                    $res = '{count:' . $res[0]->result->metadata->globalCounts->count . ', ind: "gp"}';
                } else {
                    $res = false;
                }
            }
        }

        if ($res === false) {
            $res = '{count:0, ind:"gp"}';
        }

        return 'sb.postCallBack(' . $res . ')';
    }

    /*
     *  Get the count of shares on twitter via ajax post request
     *  This can not be cached as it needs to be up to date
     */
    public function twitterCount(KCommandContext $context) {
        //now we need to post off to twitter to get the social count :)
        $res = false;
        if (($context && $context->data->url)) {

            $ch = curl_init('http://cdn.api.twitter.com/1/urls/count.json?url=' . $context->data->url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res = curl_exec($ch);
            $e = curl_errno($ch);
            curl_close($ch);

            if ($e == 0) {
                $res = json_decode($res);
                if (isset($res->count)) {
                    $res = '{count:' . $res->count . ', ind:"tw"}';
                } else {
                    $res = false;
                }
            } else {
                $res = false;
            }
        }

        if ($res === false) {
            $res = '{count:0, ind:"tw"}';
        }

        return 'sb.postCallBack(' . $res . ')';
    }

    /*
     *  Get the count of shares on facbook via ajax post request
     *  This can not be cached as it needs to be up to date
     */
    public function facebookCount(KCommandContext $context) {
        //now we need to post off to twitter to get the social count :)
        $res = false;
        if (($context && $context->data->url)) {

            $ch = curl_init('http://graph.facebook.com/?id=' . $context->data->url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $res = curl_exec($ch);
            $e = curl_errno($ch);
            curl_close($ch);

            if ($e == 0) {
                $res = json_decode($res);
                if (isset($res->shares)) {
                    $res = '{count:' . $res->shares . ', ind:"fb"}';
                } else {
                    $res = false;
                }
            } else {
                $res = false;
            }
        }

        if ($res === false) {
            $res = '{count:0, ind:"fb"}';
        }

        return 'sb.postCallBack(' . $res . ')';
    }

    /*
     *  Get the count of shares on pinterest via ajax post request
     *  This can not be cached as it needs to be up to date
     */
    public function pinterestCount(KCommandContext $context) {
        $res = false;
        if (($context && $context->data->url)) {
            $ch = curl_init('http://api.pinterest.com/v1/urls/count.json?callback=&url=' . $context->data->url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $res = curl_exec($ch);
            $e = curl_errno($ch);

            curl_close($ch);

            if ($e === 0)  {
                if (substr($res, 0, 1) == '(' && substr($res, -1) == ')') {
                    $res = substr($res, 1, -1);
                }
                $res = json_decode($res);
                if (isset($res->count)) {
                    $res = '{count:' . $res->count . ', ind:"pin"}';
                } else {
                    $res = false;
                }
            } else {
                $res = false;
            }

        }

        if ($res === false) {
            $res = '{count: 0, ind:"pin"}';
        }

        return 'sb.postCallBack(' . $res . ')';
    }

}