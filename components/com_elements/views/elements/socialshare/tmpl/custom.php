<?php
/**
 * Com ......
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_***
 * @package     ******
 * @uses        Com_
 */

// TODO: This wont work for overrides!
JHtml::stylesheet('components/com_elements/views/elements/socialshare/assets/css/'.$element->getConfig('size', 'medium', 'render').'.css');
JHtml::stylesheet('components/com_elements/views/elements/socialshare/assets/css/socialshare.css');


$url = JURI::base().'/index.php?option=com_catalogue&view=product&id='. $element->getItem()->id .'&t='. $element->getItem()->name;
?>
<div class="element-socialshare">

    <?php if($element->getConfig('facebook', false, 'render')) : ?>

        <script src="https://connect.facebook.net/en_GB/all.js" />
        <script type="text/javascript">
        (function($) {

            FB.init({
                appId : '366201670122852'
            });

            $(function() {
                $('#fbsharebutton').click(function(e){
                    e.preventDefault();
                    FB.ui({
                        method: 'feed',
                        name: '<?= $element->getItem()->name ?>',
                        link: '<?= $url ?>',
                        source: '<?= $url ?>',
                        picture: '<?= JURI::base() .'/images/logo.png' ?>',
                        caption: '<?= $element->getItem()->name ?>',
                        description: '<?= $element->getItem()->excerpt ?>',
                        message: ''
                    });
                });
            });
        })(jQuery);
        </script>

        <a  id="fbsharebutton" class="facebook" title="Share <?= $element->getItem()->name ?> on Facebook"><?= @text('Share '. $element->getItem()->name .' on Facebook') ?></a>

    <?php endif; ?>

    <?php if($element->getConfig('twitter', false, 'render')) : ?>

    <?php

        $twitter_share = "&text=". $element->getItem()->name .
                         "&url=$url";

    ?>

    <a class="twitter"
       href="javascript:(function(){window.twttr=window.twttr||{};var D=550,A=450,C=screen.height,B=screen.width,H=Math.round((B/2)-(D/2)),G=0,F=document,E;if(C>A){G=Math.round((C/2)-(A/2))}window.twttr.shareWin=window.open('http://twitter.com/share','','left='+H+',top='+G+',width='+D+',height='+A+',personalbar=0,toolbar=0,scrollbars=1,resizable=1');E=F.createElement('script');E.src='http://platform.twitter.com/bookmarklets/share.js?v=1<?= $twitter_share ?>';F.getElementsByTagName('head')[0].appendChild(E)}());"><?= @text('Tweet '. $element->getItem()->name .' on Twitter') ?></a>

<!--    <a href="" class="twitter" title="Tweet --><?//= $element->getItem()->name ?><!-- on Twitter">--><?//= @text('Tweet '. $element->getItem()->name .' on Twitter') ?><!--</a>-->
    <?php endif; ?>

    <?php if($element->getConfig('google', false, 'render')) : ?>
    <a href="https://plusone.google.com/_/+1/confirm?hl=en-US&url=<?= $url ?>" target="_blank" class="google" title="+1 <?= $element->getItem()->name ?> on Google"><?= @text('+1 '. $element->getItem()->name .' on Google') ?></a>
    <?php endif; ?>

    <?php if($element->getConfig('pinterest', false, 'render')) : ?>
    <a href="" class="pinterest" title="Pin <?= $element->getItem()->name ?> on pinterest"><?= @text('Pin '. $element->getItem()->name .' on pinterest') ?></a>
    <?php endif; ?>

    <?php if($element->getConfig('linkedin', false, 'render')) : ?>
    <a href="" class="linkedin" title="Share <?= $element->getItem()->name ?> on Linkedin"><?= @text('Share '. $element->getItem()->name .' on Linkedin') ?></a>
    <?php endif; ?>

</div>