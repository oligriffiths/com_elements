<?php
    /**
     * Com ......
     *
     * @author      Kyle Waters <development@organic-development.com>
     * @category    Com_***
     * @package     ******
     * @uses        Com_
     */

    $element = $this->getView();

    //TODO : LinkedIn Needs to be coded in to hook into its api for counts - renshaw doesn't use linkedIn at the moment.

    // TODO: This wont work for overrides!
    JHtml::stylesheet('components/com_elements/views/elements/socialshare/assets/css/' . $element->getConfig('size', 'medium', 'render') . '.css');
    JHtml::stylesheet('components/com_elements/views/elements/socialshare/assets/css/socialshare.css');


    $spans = 0;
    if ( $element->getConfig('facebook', false, 'render') ) $spans++;
    if ( $element->getConfig('twitter', false, 'render') ) $spans++;
    if ( $element->getConfig('google', false, 'render') ) $spans++;
    if ( $element->getConfig('pinterest', false, 'render') ) $spans++;
    if ( $element->getConfig('linkedin', false, 'render') ) $spans++;

    $spans = floor(12 / $spans);

?>
<script type="text/javascript">
jQuery(document).ready(function () {

    //need to set watchers on teh window close event because onclose is only applicable to body or iframe!
    //window.onclose doesn't catch the close either :(

    var sb;
    var socialButtons = function () {

        this.counts = {};
        this.counts.fb = 0;
        this.counts.tw = 0;
        this.counts.pin = 0;
        this.counts.gp = 0;

        this.socialUrls = [];

        this.windows = [];
        this.watcher = [];
        this.buttons = [];

        this.init = function () {

            if ( document.getElementById('sb_tw_tweets') ) {
                this.buttons['tw'] = document.getElementById('sb_tw_tweets');
                this.buttons['tw'].onclick = sb.popupTwitter;
                this.twitterText = this.buttons['tw'].getAttribute('data-text');
            }

            if ( document.getElementById('sb_fb_likes') ) {
                this.buttons['fb'] = document.getElementById('sb_fb_likes');
                this.buttons['fb'].onclick = sb.popupFacebook;
                this.facebookText = this.buttons['fb'].getAttribute('data-text');
            }


            if ( document.getElementById('sb_pin_pins') ) {
                this.buttons['pin'] = document.getElementById('sb_pin_pins');
                this.buttons['pin'].onclick = sb.popupPinterest;
                this.pinText = this.buttons['pin'].getAttribute('data-text');
                this.pinImage = this.buttons['pin'].getAttribute('data-image');
            }

            if ( document.getElementById('sb_gp_gplus') ) {
                this.buttons['gp'] = document.getElementById('sb_gp_gplus');
                this.buttons['gp'].onclick = sb.popupGPlus;
                this.gPlusText = this.buttons['gp'].getAttribute('data-text');
            }

            this.url = '<?= $shortUrl ?>';
            this.encodedUrl = '<?= urlencode($shortUrl) ?>';
            this.postUrl = '<?= @route('option=com_elements&view=element&format=raw&type=socialshare&id=' . $element->id . '&package=' . $element->getPackage() . '&structure=' . ($structure ? $structure->id : '')) ?>';

            //facebook settings
            this.socialUrls.fb = {};
            this.socialUrls.fb.type = 'post';
            this.socialUrls.fb.args = {action: 'callback', callback: 'facebookCount', url: this.encodedUrl};
            //this.socialUrls.fb.type = 'get';
            //this.socialUrls.fb.url = "http://graph.facebook.com/?id=" + this.encodedUrl + "&callback=?";

            //twitter call settings
            this.socialUrls.tw = {};
            this.socialUrls.tw.type = 'post';
            this.socialUrls.tw.args = {action: 'callback', callback: 'twitterCount', url: this.encodedUrl};

            //google plus settings
            this.socialUrls.gp = {};
            this.socialUrls.gp.type = 'post';
            this.socialUrls.gp.args = {action: 'callback', callback: 'googlePlusCount', url: this.encodedUrl};

            //pinterest settings
            this.socialUrls.pin = {};
            this.socialUrls.pin.type = 'post';
            this.socialUrls.pin.args = {action: 'callback', callback: 'pinterestCount', url: this.encodedUrl};

            this.setUpDom();


        };

        this.setUpDom = function () {
            var c1 = null;
            var c2 = null;
            for ( var x in this.buttons ) {
                c1 = document.createElement('div');
                c1.innerHTML = '0';
                c1.setAttribute('class', 'count-bubble');
                c2 = document.createElement('div');
                c2.setAttribute('class', 'sb_btn');

                this.buttons[x].appendChild(c1, c2);
                this.countFetch(x);
            }


        };

        this.countFetch = function ( ind ) {

            if ( ind !== undefined && sb.socialUrls[ind] !== undefined ) {
                if ( sb.socialUrls[ind]['type'] == 'get' ) {

                    jQuery.getJSON(sb.socialUrls[ind]['url'], function ( data ) {
                        eval(data);
                    });
                } else {
                    jQuery.post(
                        sb.postUrl,
                        sb.socialUrls[ind]['args'],
                        function ( response ) {
                            eval(response);
                        }
                    );
                }
            }
        }

        //popups

        this.popupTwitter = function () {
            sb.windows['tw'] = window.open("https://twitter.com/intent/tweet?text=" + encodeURIComponent(sb.twitterText) + "&url=" + encodeURIComponent(sb.url), "", "toolbar=0, status=0, width=650, height=360");
            var self = sb;
            sb.windows['tw'].ind = 'tw';
            sb.windows['tw'].onclose = sb.overSeerWatcher;
            sb.watcher['tw'] = setInterval(function () {
                self.overSeerWatcher("tw");
            }, 500);
        };

        this.popupGPlus = function () {
            sb.windows['gp'] = window.open("https://plus.google.com/share?hl=en&url=" + encodeURIComponent(sb.url), "", "toolbar=0, status=0, width=900, height=500");
            var self = sb;
            sb.windows['gp'].ind = 'gp';
            sb.windows['gp'].onclose = sb.overSeerWatcher;
            sb.watcher['gp'] = setInterval(function () {
                self.overSeerWatcher("gp");
            }, 500);
        };

        this.popupFacebook = function () {
            sb.windows['fb'] = window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent(sb.url) + "&t=" + encodeURIComponent(sb.fbText) + "", "", "toolbar=0, status=0, width=900, height=500");
            var self = sb;
            sb.windows['fb'].ind = 'fb';
            sb.windows['fb'].onclose = sb.overSeerWatcher;
            sb.watcher['fb'] = setInterval(function () {
                self.overSeerWatcher("fb");
            }, 500);
        };


        this.popupLinkedin = function () {
            sb.windows['li'] = window.open('https://www.linkedin.com/cws/share?url=' + encodeURIComponent(sb.url) + '&token=&isFramed=true', 'linkedin', 'toolbar=no,width=550,height=550');
            var self = sb;
            sb.windows['li'].ind = 'li';
            //sb.windows['li'].onclose = sb.overSeerWatcher;
            sb.watcher['li'] = setInterval(function () {
                self.overSeerWatcher("li");
            }, 500);
        };

        this.popupPinterest = function () {
            // load pinmarklet script
            jQuery.ajax({
                dataType: 'script',
                url: 'http://assets.pinterest.com/js/pinmarklet.js',
                success: function ( data ) {
                    // set up a timer to look for updated pin count
                    sb.watcher['pin'] = setInterval(function () {
                        self.overSeerWatcher("pin");
                    }, 500);
                }
            });
        };


        //dom manipulation
        this.fbCountDOM = function ( json ) {
            //console.log(json);

            if ( json.shares ) {
                this.buttons['fb'].childNodes[0].innerHTML = json.shares;
                this.counts['fb'] += json.count * 1;
            } else {
                this.buttons['fb'].childNodes[0].innerHTML = this.counts['fb'];
            }
        }

        this.postCallBack = function ( json ) {
            //console.log(json);

            if ( json.count ) {
                this.buttons[json.ind].childNodes[0].innerHTML = json.count;
                this.counts[json.ind] += json.count * 1;
            } else {
                this.buttons[json.ind].childNodes[0].innerHTML = this.counts[json.ind];
            }
        }

        //watchers for window unload events
        this.overSeerWatcher = function ( ind ) {
            if ( sb.windows[ind].closed === true ) {
                clearInterval(sb.watcher[ind]);
                sb.windows[ind] = null;
                sb.countFetch(ind);
            }
        }
    };
    sb = new socialButtons();
    sb.init();
});

</script>


<div class="element-socialshare">


    <?php if ( $text = $element->getConfig('text', false, 'render') ): ?>
        <p><?= @text($text) ?></p>
    <?php endif ?>

    <div class="row-fluid buttons">


        <?php if ( $element->getConfig('facebook', false, 'render') ) : ?>

            <div id="sb_fb_likes" class="socialshare-icon fb-icon span<?= $spans ?>" data-text="<?= $element->getConfig('facebook_text', 'Check out ' . $sanitzedName, 'render') . ' ' . $shortUrl; ?>" data-title=""></div>

        <?php endif; ?>


        <?php if ( $element->getConfig('twitter', false, 'render') ) : ?>
            <div id="sb_tw_tweets" class="socialshare-icon tw-icon span<?= $spans ?>" data-text="<?= $element->getConfig('twitter_text', 'Check out ' . $sanitzedName, 'render') . ' '; ?>" data-title="share"></div>
        <?php endif; ?>


        <?php if ( $element->getConfig('google', false, 'render') ) : ?>
            <div id="sb_gp_gplus" class="socialshare-icon gp-icon span<?= $spans ?>" data-text="<?= $element->getConfig('gplus_text', 'Check out ' . $sanitzedName, 'render') . ' ' . $shortUrl; ?>" data-title="share"></div>
        <?php endif; ?>


        <?php

            $main_image = $element->getItem()->get('image');

            // Get the first image.
            if ( isset($main_image) && !isset($main_image['src']) && isset($main_image[0]['src']) ) {
                $main_image = $main_image[0];
            }

            if ( $element->getConfig('pinterest', false, 'render') && $main_image && isset($main_image['src']) & !empty($main_image['src']) ) :

                //determine if the main image is set and exists

                $mainImageRelPath = preg_replace('#^/#', '', $main_image['src']);
                if ( $mainImageRelPath != '' && file_exists(JPATH_BASE . '/' . $mainImageRelPath) ) : ?>
                    <script>
                        //    jQuery(function(){
                        //    jQuery("body").append("<script type=\"text/javascript\" src=\"//assets.pinterest.com/js/pinit.js\"><\/script>");
                        //  });
                    </script>

                    <div class="socialshare-icon pin-icon  span<?= $spans ?>" id="sb_pin_pins" data-text="<?= $element->getConfig('pin_text', 'Check out ' . $sanitzedName, 'render') . ' ' . $shortUrl; ?>" data-image="<?= JURI::base() . $mainImageRelPath ?>" data-title="share"></div>

                <?php endif;
            endif;
        ?>


        <?php if ( $element->getConfig('linkedin', false, 'render') ) : ?>
            <div class="socialshare-icon linkedin-icon  span<?= $spans ?>" id="sb_li_linkedin" data-text="<?= $element->getConfig('linkedin_text', 'Check out ' . $sanitzedName, 'render') . ' ' . $shortUrl; ?>" data-title="share"></div>
        <?php endif; ?>

    </div>

</div>
