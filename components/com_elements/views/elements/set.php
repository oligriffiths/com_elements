<?php
/**
 * User: Oli Griffiths
 * Date: 22/06/2012
 * Time: 20:13
 */

class ComElementsViewElementsSet extends KObjectSet
{
	/**
	 * @var mixed
	 */
	protected $_structure;

	/**
	 * @var mixed
	 */
	protected $_package;

	/**
	 * Holds a copy of each element type so new instances can be cloned
	 * @var array
	 */
	protected static $_element_types = array();


	/**
	 * @param KConfig $config
	 */
	public function __construct(KConfig $config)
	{
		$config->append(array(
			'structure' => null,
			'item' => null,
			'package' => null
		));
		parent::__construct($config);

		$this->_structure   = $config->structure;
		$this->_item        = $config->item;
		$this->_package     = $config->structure ? $config->structure->package : $config->package;
	}


	/**
	 * Sets the structure in the layout
	 * @param $structure
	 * @return ComElementsViewElementsSet
	 */
	public function setStructure(&$structure)
	{
		$this->_structure = $structure;
		if($structure->getItem()) $this->setItem($structure->getItem());
		return $this;
	}


	/**
	 * @param $property
	 * @return mixed
	 */
	public function __get($property)
	{
		if($this->_object_set->offsetExists($property)){
			return $this->_object_set->offsetGet($property);
		}
	}


	/**
	 * @param $property
	 * @param $value
	 */
	public function __set($property, $value)
	{
		if($value instanceof ComElementsElementDefault){
			$this->_object_set->offsetSet($property, $value);
		}else{
			$this->$property = $value;
		}
	}


	/**
	 * Clear the set
	 */
	public function erase()
	{
		$this->_object_set = new ArrayObject();
		return $this;
	}


	/**
	 * @param $id
	 * @return mixed
	 */
	public function getElement($id)
	{
		return $this->$id;
	}


	/**
	 * @param $config
	 * @return ComElementsViewElement
	 * @throws Exception
	 */
	public function setElement($config)
	{
		static $element_types = array();

		if(!$config instanceof ComElementsViewElement){
			$config = is_string($config) ? new KConfig(array('type' => $config)) : new KConfig($config);

			$package = $this->_package ?: $this->_structure->package;

			$options = $config->toArray();
			if(array_key_exists('item', $options) && !$options['item']) unset($options['item']);
			if(!array_key_exists('item', $options) && isset($this->_item)) $options['item'] = $this->_item;
			$options['structure'] = $this->_structure;
			$options['package'] = $this->_package;
			$options['set'] = $this;

			$type = $config->get('type','default') ?: 'default';

			$identifier             = clone $this->getIdentifier();
			$identifier->package    = $package;
			$identifier->path       = array('view','elements', $type);
			$identifier->name       = 'element';

			//Check if element has an override / exists within target package
			if(is_dir(dirname($identifier->filepath)))
			{
				//Check if the element has an element class file
				if(!file_exists($identifier->filepath)) $identifier->package = 'elements';

			}else{
				$identifier->package = 'elements';
				$options['package'] = 'elements';
			}

			//Check if this element type has been instantiated before
			if(!isset($element_types[$package.'.'.$type])){

	            //Get an instance of the element
	            $element = $this->getService($identifier, $options);

				if(!$element instanceof ComElementsViewElement)
                {
                    throw new Exception('Element does not extend ComElementsViewElement. '.$identifier);
                }

				//Store the element type for recloning later
				$element_types[$package.'.'.$type] = clone $element;
				$element_types[$package.'.'.$type]->reset();
			}else{

				//Restore from cache
				$element = clone $element_types[$package.'.'.$type];
				$element->setup($options);
			}
		}

		//Add element to the set
		$this->insert($element);
		return $element;
	}


	/**
	 * @param $id
	 * @return ComElementsViewElementsSet
	 */
	public function removeElement($id)
	{
		if($this->_object_set->offsetExists($id)) {
			$this->_object_set->offsetUnset($id);
		}
		return $this;
	}


	/**
	 * @param $data
	 * @param int $mode - mode for the data - 1 = render - 0 = edit
	 */
	public function setElements( $data, $mode = 0 )
	{
		//Get current object set, use new data to set ordering
		$elements = clone $this->_object_set;
		$this->_object_set = new ArrayObject();

		//Loop new data and insert
		foreach($data AS $id => $element)
		{
			if(!$element instanceof KConfig) $element = new KConfig($element);

			//If element already exists, merge in config info
			if($elements->offsetExists($id)){
				$existing = $elements->offsetGet($id);
				$element->append($existing->getConfig(null, null, $mode ? 'render' : 'edit'));
			}

			unset($element->id);
			$this->setElement($element);
		}

		//Re-insert existing data
		foreach($elements AS $id => $object)
		{
			//Only insert new object or overwrite existing objects that have no type set
			//@TODO: refactor this, we need to distinguish between columns set to skip and ones that havent been set
			if(!$this->_object_set->offsetExists($id) || !$this->getElement($id)->getType()) $this->insert($object);
		}
	}


	/**
	 * @param $item
	 * @return ComElementsViewElementsSet
	 */
	public function setItem($item)
	{
       // $this->_item = $item;
		foreach($this AS $element) $element->setItem($item);

		return $this;
	}
}