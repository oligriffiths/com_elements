<?php
/**
 * Created by JetBrains PhpStorm.
 * User: leeparsons
 * Date: 25/10/2012
 * Time: 09:33
 * To change this template use File | Settings | File Templates.
 */
class ComElementsViewElementsFacebookcommentsElement Extends ComElementsViewElement
{
    public static $js_loaded = false;

	/**
	 * Enable by default if not explicity disabled
	 * @param null $value
	 * @param null $index
	 * @return bool
	 */
	protected function _hasValue($value = null, $index = null)
	{
		$value = $this->getValue($value, $index);
		return $value === null ? true : $value;
	}


	public function _render()
    {

        $item_identifier = clone $this->getItem()->getIdentifier();
/*         $this->link = (string) KRequest::base() . (string) $this->getRoute('option=com_'. $item_identifier->package .'&view='. $item_identifier->name .'&id='. $this->getItem()->id ); */
		$this->link = (string) KRequest::url();
        $this->show_count = (bool) $this->getConfig('show_count', false, 'render');
        return parent::_render();
    }
}
