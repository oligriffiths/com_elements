<?php

$element = $this->getView();

//see if we are displaying either the comments list with form or count only
if ($show_count) : ?>
<span class="fb-comment-count" data-href="<?= $link; ?>">0</span>
    <?php if (!$element::$js_loaded): ?>
        <script>
            //get the count of comments for each comment speech bubble:
            //cycle through all of them and get all of them in one request
            ;(function($) {
                $(document).ready(function() {
                    if ($('.fb-comment-count').length > 0) {
                        var urlBase = window.location.protocol + '//' + window.location.host;
                        var urlStr = '';
                        $('.fb-comment-count').each(function(k) {
                            $(this).addClass('fb-ind-' + k);
                            if ($(this).attr('data-href').indexOf(urlBase) == -1) {
                                urlStr = urlBase + $(this).attr('data-href');
                            } else {
                                urlStr = $(this).attr('data-href');
                            }

                            $.getJSON('https://graph.facebook.com/?ids=' + encodeURIComponent(urlStr), function(data) {
                                for (var x in data) {
                                    if (data[x].shares) {
                                        //the return should contain the right data!
                                        $('span.fb-ind-' + k).html(data[x].shares);
                                    } else {
                                        $('span.fb-ind-' + k).html(0);
                                    }
                                }
                            });
                        });
                    }
                });
            })(jQuery);
        </script>
    <?php
		$element::$js_loaded = true;
		endif;
	?>
<?php else: ?>
    <div id="fb-root"></div>
    <script>/*<!CDATA[*/(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));/*]]>*/</script>
    <div class="fb-comments" data-href="<?= $link ?>" data-num-posts="<?= $element->getConfig('limit', 5, 'render') ?>" data-width="<?= $element->getConfig('width', '', 'render') ?>"></div>
<?php endif; ?>