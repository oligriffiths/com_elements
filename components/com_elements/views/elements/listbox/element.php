<?php
/**
 * Created By: Oli Griffiths
 * Date: 30/10/2012
 * Time: 10:56
 */
defined('KOOWA') or die('Protected resource');

class ComElementsViewElementsListboxElement extends ComElementsViewElement
{
	protected function _hasValue($key, $index = null)
	{
		$value = $this->getValue($key, $index);
		return $value ? true : false;
	}


	protected function _edit()
	{
		try{

			$package = $this->getConfig('package') ?: $this->getPackage();
			$identifier = clone $this->getIdentifier();
			$identifier->package = $package;
			$identifier->path = 'model';
			$identifier->name = $this->getConfig('model');


			$control_name = $this->getControlName();
			$deselect = true;
			$attribs = array();
			if($this->getConfig('multiselect', false)){
				$attribs['multiple'] = 'multiple';
				$attribs['size'] = '10';
				$deselect = false;
				$control_name = $this->getControlName('');
			}

            $selected = $this->getValue();
            if($selected instanceof KDatabaseRowsetAbstract) $selected = array_keys($selected->getData());

            parse_str($this->getConfig('state'), $state);

            $config = new KConfig();
			$config->append(array(
				'identifier' => $identifier,
				'value' => $this->getConfig('key'),
				'text' => $this->getConfig('text'),
				'selected' => $selected,
				'attribs' => $attribs,
				'deselect' => $deselect,
                'filter' => $state
			));

			return $this->getTemplate()->renderHelper('listbox.'.$control_name, $config->toArray());

		}catch(KException $e){
			return $e->getMessage();
		}
	}
}