<?php
/**
 * Com ......
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_***
 * @package     ******
 * @uses        Com_
 */

$element = $this->getView();

try{
	$package = $element->getConfig('package') ?: $element->getPackage();
	$identifier = clone $this->getIdentifier();
	$identifier->application = JFactory::getApplication()->isAdmin() ? 'admin' : 'site';
	$identifier->package = $package;
	$identifier->path = 'model';
	$identifier->name = $element->getConfig('model');

	$state_query = trim($element->getConfig('state'));
	$state = array();
	if($state_query){
		parse_str($state_query, $state);
	}else{
		$state = array();
	}

	$deselect = $element->getConfig('deselect', true);
	$multiselect = $element->getConfig('multiselect');
	$attribs = array(
		'id' => $control_id
	);
	if($multiselect){
		$attribs['multiple'] = 'multiple';
		$attribs['size'] = '10';
		$control_name .= '[]';
		$deselect = false;
	}

	$config = new KConfig();
	$config->append(array(
		'name' => $control_name,
		'identifier' => (string) $identifier,
		'value' => $element->getConfig('key') ?: 'id',
		'text' => $element->getConfig('text') ?: 'name',
		'prompt' => sprintf(@text('- Select %s -'),ucfirst(KInflector::singularize($element->getConfig('model')))),
		'selected' => $value,
		'filter' => $state,
		'attribs' => $attribs,
		'deselect' => $deselect,
	));

	echo @helper('listbox.'.$control_name, $config->toArray());

}catch(KException $e){
	echo $e->getMessage();
}