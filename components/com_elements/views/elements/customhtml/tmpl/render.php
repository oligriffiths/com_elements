<?php

$element = $this->getView(); ?>

<?php if ($html = $element->getConfig('html', false, 'render')): ?>
<div class="element element_customhtml">
	<?= $html; ?>
</div>
<?php endif; ?>
