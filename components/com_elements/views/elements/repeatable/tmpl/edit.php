<?php
/**
 * User: Oli Griffiths
 * Date: 03/11/2012
 * Time: 14:46
 */

static $repeated;

if(!$repeated):
?>
<script src="<?= KRequest::root() ?>/components/com_elements/assets/js/jquery-ui-1.9.2.sortable.min.js" />
<script src="<?= KRequest::root() ?>/components/com_elements/assets/js/repeatable.js" />
<style src="<?= KRequest::root() ?>/components/com_elements/assets/css/repeatable.css" />
<?php
$repeated = true;
endif ?>

<ul class="unstyled repeatable" id="element_<?= $element_id ?>_repeatable" data-repeat="<?= $element_id ?>">
<?php foreach($elements AS $index => $element): ?>
	<li class="index_<?= $index ?>">
        <div class="buttons">
            <div class="icon icon-remove" style="<?= count($elements) < 3 ?'display: none' : '' ?>"></div>
            <div class="icon icon-move" style="<?= count($elements) < 3 ?'display: none' : '' ?>"></div>
        </div>

		<div class="element">
			<?= $element ?>
		</div>
	</li>
<?php endforeach ?>
</ul>
<a class="repeatable-add btn btn-small" data-repeater="element_<?= $element_id ?>_repeatable">
	<i class="icon icon-plus"></i>
	<?= @text('add another') ?>
</a>