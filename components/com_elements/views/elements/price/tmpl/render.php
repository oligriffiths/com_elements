<?php
/**
 * Com ......
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_***
 * @package     ******
 * @uses        Com_
 */

//** MOVE TO COM_CATALOGUE **//
$element = $this->getView();

// Get product tax rate
$tax = $this->getService('com://site/catalogue.model.tax_rate')->id($element->getItem()->tax_group_id)->getItem();

// Set locale
$locale = JFactory::getLanguage()->getLocale();
setlocale(LC_MONETARY, $locale[1]);

// Output locale formatted money amount
if($element->getConfig('apply_tax')){
	echo money_format('%.2n', $value * $tax->rate);
}else{
	echo money_format('%.2n', $value);
}