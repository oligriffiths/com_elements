<?php
/**
 * User: Oli Griffiths
 * Date: 23/06/2012
 * Time: 13:29
 */

$size = '';
switch($element->getConfig('size'))
{
	case 'xs': $size = 'input-mini'; break;
	case 's': $size = 'input-small'; break;
	case 'm': $size = 'input-medium'; break;
	case 'l': $size = 'input-xlarge'; break;
	case 'xl': $size = 'input-xxlarge'; break;
}

?>
<textarea id="<?= $id ?>" name="<?= $control_name ?>" <?= $size ? 'class="'.$size.'"' : '' ?> rows="<?= $element->getConfig('rows', 5) ?>" cols="<?= $element->getConfig('cols', 40) ?>"><?= $value ?></textarea>