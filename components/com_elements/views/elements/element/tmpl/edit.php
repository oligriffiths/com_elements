<?php
/**
 * Created By: Oli Griffiths
 * Date: 03/12/2012
 * Time: 13:01
 */
defined('KOOWA') or die('Protected resource');

static $element_style;

if(!$element_style):

echo @helper('bootstrap.popover');
?>
<style src="<?= KRequest::root() ?>/components/com_elements/assets/css/elements.css" />
<script src="<?= KRequest::root() ?>/components/com_elements/assets/js/elements.js" />
<?php
$element_style = true;
endif ?>

<?= $content ?>