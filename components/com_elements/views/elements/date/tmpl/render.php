<?php
/**
 * User: Oli Griffiths
 * Date: 22/06/2012
 * Time: 20:07
 */

$element = $this->getView();

?>
<?php if(preg_match('#[\d]{4}-[\d]{2}-[\d]{2}#', $value) || preg_match('#[\d]{4}-[\d]{2}-[\d]{2}\s[\d]{2}:[\d]{2}:[\d]{2}#', $value)): ?>
    <?php $format = $element->getConfig('format') == 'custom' ? $element->getConfig('format_custom') : $element->getConfig('format','%a %e%o, %B %Y'); ?>
	<?php if(strpos($format,'%o') != false): ?>
	    <?php $format = str_replace('%o', date('S', strtotime($value)), $format); ?>
    <?php endif; ?>
    <time datetime="<?= $value ?>"><?= @helper('date.format', array('date' => $value, 'format' => $format)) ?></time>
<?php endif; ?>