<?php
/**
 * User: Oli Griffiths
 * Date: 23/06/2012
 * Time: 13:29
 */

$element->setConfig('class', $element->getConfig('class', '', 'render').' controls-calendar', 'render');

echo @helper( 'behavior.calendar', array( 'date' => $value, 'name' => $control_name ) ) ?>
