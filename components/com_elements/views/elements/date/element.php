<?php
/**
 * Created by JetBrains PhpStorm.
 * User: leeparsons
 * Date: 24/10/2012
 * Time: 09:40
 * To change this template use File | Settings | File Templates.
 */

class ComElementsViewElementsDateElement extends ComElementsViewElement
{
    protected function _hasValue($value = null, $index = null)
    {
        if ($this->getConfig('id', '', 'render') == 'publish_up') {
            $publish_up = $this->getValue();

            if ($publish_up === null && $this->getItem()->created_on !== null) {
                $this->setValue($this->getItem()->created_on, 'publish_up');
            } else {
                return $publish_up !== null;
            }

        }

        return true;
    }

}