<?php
/**
 * Created By: Oli Griffiths
 * Date: 03/12/2012
 * Time: 16:56
 */
defined('KOOWA') or die('Protected resource');

$type = 'text';
$attribs = array();
if($element->getConfig('tag') == 'description'){
	$attribs['rows'] = '10';
	$attribs['cols'] = '60';

	echo @helper('form.textarea', array('name' => $control_name, 'value' => $value, 'size' => 'xl', 'attribs' => $attribs));
}else{
	echo @helper('form.input', array('type' => $type, 'name' => $control_name, 'value' => $value, 'size' => 'l', 'attribs' => $attribs));
}