<?php
/**
 * Created By: Oli Griffiths
 * Date: 03/12/2012
 * Time: 17:00
 */
defined('KOOWA') or die('Protected resource');

$type = $element->getConfig('tag');

?>
<meta name="<?= $type ?>" content="<?= $value ?>" />