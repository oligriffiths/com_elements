<?php
/**
 * Com Scrapbook Element Render Layout
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Element
 * @package     Scrapbook
 * @uses        Com_Element
 */

$template_helper = $this->getService('com://site/scrapbook.template.helper.add');
echo $template_helper->addButton(array(
    'package'        => $item->getIdentifier()->package,
    'type'           => $item->getIdentifier()->name,
    'row_id'         => $item->id,
    'button_text'    => $element->getConfig('button_text', null, 'render'),
    'list'           => $element->getConfig('list', null, 'render'),
    'title'          => $item->name
));
?>