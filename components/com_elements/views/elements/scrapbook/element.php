<?php
/**
 * Com Scrapbook Scrapbook Element
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Element
 * @package     Scrapbook
 * @uses        Com_Elements
 */
class ComElementsViewElementsScrapbookElement extends ComElementsViewElement {

    protected function _hasValue($value = null, $index = null)
    {
        return true;
    }

}