<?php
/**
 * Com ......
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    Com_***
 * @package     ******
 * @uses        Com_
 */

class ComElementsViewLayoutHtml extends ComElementsViewHtml
{
    public function display()
    {
        $this->getModel()->getItem()->setItem($this->item);

        return $this->getTemplate()->render();
    }
}