<?php
/**
 * Created By: Oli Griffiths
 * Date: 25/09/2012
 * Time: 10:52
 */
defined('KOOWA') or die('Protected resource');

class ComElementsInitialize extends KObject implements KServiceInstantiatable
{
	/**
	 * Initializes the options for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param   KConfig $object An optional KConfig object with configuration options
	 * @return  void
	 */
	protected function _initialize(KConfig $config)
	{
		parent::_initialize($config);

		//Register media locator and kloader
		KServiceIdentifier::addLocator(KService::get('com://site/elements.service.locator.media'));

		KService::setAlias('com://site/elements.model.layouts','com://admin/elements.model.layouts');
		KService::setAlias('com://site/elements.model.structures','com://admin/elements.model.structures');
	}

	/**
	 * Force creation of a singleton
	 *
	 * @param 	object 	An optional KConfig object with configuration options
	 * @param 	object	A KServiceInterface object
	 * @return KDatabaseTableDefault
	 */
	public static function getInstance(KConfigInterface $config, KServiceInterface $container)
	{
		// Check if an instance with this identifier already exists or not
		if (!$container->has($config->service_identifier))
		{
			//Create the singleton
			$classname = $config->service_identifier->classname;
			$instance  = new $classname($config);
			$container->set($config->service_identifier, $instance);
		}

		return $container->get($config->service_identifier);
	}
}