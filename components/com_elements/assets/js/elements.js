;(function($){
    $(function(){
        if ($('[rel=tooltip]').length > 0) {
            $('[rel=tooltip]').tooltip();
        }
        if ($('[rel=popover]').length > 0) {
            $('[rel=popover]').popover();
        }
    })
})(window.jQuery)