/* =========================================================
 * bootstrap-repeatable.js v2.1.1
 * http://twitter.github.com/bootstrap/javascript.html#repeatables
 * =========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */


!function ($) {

	"use strict"; // jshint ;_;


	/* MODAL CLASS DEFINITION
	 * ====================== */

	var Repeatable = function (element, options) {
		this.options = options
		this.element = $('#'+options.repeat)
		this.repeatable = $(element)

		this.row = this.repeatable.find('li:last-child').remove()
		this.index = this.repeatable.children().length-1

		var self = this;
		this.repeatable.on('click', '.icon-remove', function ( e ) {
			self.remove($(this).parent().parent())
		})

		this.repeatable.sortable({ handle: ".icon-move", axis: 'y' });
	}

	Repeatable.prototype = {

		constructor: Repeatable,

		updateControls: function(){

			this.repeatable.find('.icon-remove').css('display', this.repeatable.children().length > 1 ? '' : 'none')
			this.repeatable.find('.icon-move').css('display', this.repeatable.children().length > 1 ? '' : 'none')
		},

		remove: function(row){
			if(confirm('Are you sure you wish to delete this element?')){
				row.remove()
				this.updateControls()
			}
		},

		repeat: function(){
			var row = this.row.clone(true, true)
			this.repeatable.append(row)
			this.index++;
			var self = this;

            row.prop('class', 'index_'+this.index)
			row.find('input,textarea,select').each(function(){
				var $el = $(this)
				$el.prop('name', $el.prop('name').replace(/\[\*new\*\]/, '['+self.index+']'))
				$el.prop('id', $el.prop('id').replace(/\*new\*/, '_'+self.index))
			})
			this.updateControls()

			this.element.trigger('repeat', row)
		}
	}


	/* REPEATABLE PLUGIN DEFINITION
	 * ======================= */
	$.fn.repeatable = function (option) {
		return this.each(function () {
			var $this = $(this)
				, data = $this.data('repeatable')
				, options = $.extend({}, $.fn.repeatable.defaults, $this.data(), typeof option == 'object' && option)

			if (!data) $this.data('repeatable', (data = new Repeatable(this, options)))

			if(typeof(option) == 'string') data[option]()
		})
	}

	$.fn.repeatable.defaults = {
		
	}

	$.fn.repeatable.Constructor = Repeatable


	/* REPEATABLE DATA-API
	 * ============== */
	$(function () {

		$('[data-repeat]').each(function(){
			var $this = $(this)
			$this.repeatable()
		})

		$('body').on('click.repeat.data-api', '[data-repeater]', function ( e ) {
			var $this = $(this)
			e.preventDefault()
			$('#'+$this.data('repeater')).repeatable('repeat')
		})
	})

}(window.jQuery);