<?php
/**
 * Created By: Oli Griffiths
 * Date: 25/09/2012
 * Time: 10:55
 */
defined('KOOWA') or die('Protected resource');

class ComElementsServiceLocatorMedia extends KServiceLocatorAbstract
{
	/**
	 * The type
	 *
	 * @var string
	 */
	protected $_type = 'media';

	/**
	 * Get the classname based on an identifier
	 *
	 * @param     object             An identifier object - [application::]type.package.[.path].name
	 * @return     string|false     Returns the class on success, returns FALSE on failure
	 */
	public function findClass(KServiceIdentifier $identifier)
	{
		$word      = preg_replace('/[^a-zA-Z0-9\s_]/', ' ', implode('/', $identifier->path));
		$path      = str_replace(' ', '', ucwords(strtolower(str_replace('/', ' ', $word))));

		$classname = 'Media'.ucfirst($identifier->package).$path.ucfirst($identifier->name);

		//Manually load the class to set the basepath
		if (!$this->getService('koowa:loader')->loadClass($classname, $identifier->basepath))
		{
			$classpath = $identifier->path;
			$classtype = !empty($classpath) ? array_shift($classpath) : '';

			//Create the fallback path and make an exception for views
			$path = ucfirst($classtype).KInflector::camelize(implode('_', $classpath));

			$package = KInflector::camelize($identifier->package);
			if(class_exists('Media'.$package.$path.ucfirst($identifier->name))) {
				$classname = 'Media'.$package.$path.ucfirst($identifier->name);
			}else if(class_exists($package.$path.ucfirst($identifier->name))) {
				$classname = $package.$path.ucfirst($identifier->name);
			} elseif(class_exists($package.$path.'Default')) {
				$classname = $package.$path.'Default';
			} elseif(class_exists('ComDefault'.$path.ucfirst($identifier->name))) {
				$classname = 'ComDefault'.$path.ucfirst($identifier->name);
			} elseif(class_exists('ComDefault'.$path.'Default')) {
				$classname = 'ComDefault'.$path.'Default';
			} elseif(class_exists( 'K'.$path.ucfirst($identifier->name))) {
				$classname = 'K'.$path.ucfirst($identifier->name);
			} elseif(class_exists('K'.$path.'Default')) {
				$classname = 'K'.$path.'Default';
			} else {
				$classname = false;
			}
		}

		return $classname;
	}

	/**
	 * Get the path based on an identifier
	 *
	 * @param  object   An identifier object - [application::]type.package.[.path].name
	 * @return string    Returns the path
	 */
	public function findPath(KServiceIdentifier $identifier)
	{
		$path = array(JPATH_ROOT);
		$path[] = 'media';
		$path[] = $identifier->package;
		$path = array_merge($path, $identifier->path);

		$path = implode('/', $path);
		$path .= '/'.$identifier->name.'.php';

		return $path;
	}
}